@extends('layout.default')

@section('content')
<section class="content-header">
	<h1>Daftar Riwayat Peminjaman Aset</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('/') }}"><i class="ion-speedometer"></i> Dasbor</a></li>
		<li class="active">Log Peminjaman Aset</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<table id="table" class="table table-striped dataTable no-footer" data-tables="true" width="100%">
						<thead>
							<tr>
								<th width="10">No</th>
								<th>NUP</th>
								<th>Nama Aset</th>
								<th>Kategori</th>
								<th>Tgl. Pinjam</th>
								<th>Tgl. Kembali</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script type="text/javascript">
	$(document).ready(function () {
		var table = $("#table").DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ route('user.logdata')}}",
			columns: [
				{ data: "rownum", name: "rownum", class: "text-right" },
				{ data: "nuk", name: "asset_item.nup"},
				{ data: "asset_name", name: "asset_item.name" },
				{ data: "category", name: "asset_type.name"},
				{ data: "started_at", name: "lending.started_at" },
				{ data: "ended_at", name: "lending.ended_at"},
				{ data: "status", name: "lending.status" }
			],
			language: {
				url: "{{ asset('lang/Indonesian.json') }}"
			},
			responsive: true,
			initComplete: function() {
				$("#table").wrap("<div class='table-responsive'></div>");
				$("select[name='table_length']").select2({
					minimumResultsForSearch: Infinity
				});
			}
		});
	});
</script>
@endsection
