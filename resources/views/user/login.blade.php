@extends('layout.default')

@section('styles')
<style type="text/css">
	.title {
		font-size: 42px;
	}
</style>
@stop

@section('content')
<div class="flex-center position-ref full-height">
	<div class="content">
		<div class="title m-b-md">
			Aplikasi Layanan
		</div>

		<div class="container">
			<form method="POST" action="{{ url('/dasbor') }}" accept-charset="UTF-8" role="form">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4 form-group">
						<input type="text" name="username" class="form-control" placeholder="Nama pengguna">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4 form-group">
						<input type="password" name="password" class="form-control" placeholder="Kata sandi">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4 form-group">
						<button type="submit" class="btn btn-block btn-primary">Login</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@stop