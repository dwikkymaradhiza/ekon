@extends('layout.default')

@section('content')
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="container">
				<div class="col-md-12 text-center">
					<h3>PROSES PERMINTAAN {{ $name }}</h3>
					<p>
						Proses permintaan peminjaman telah kami terima.<br>
						Setelah permintaan disetujui Anda dapat mengklik tombol tiket untuk<br>
						mendapatkan tiket pengambilan aset.
					</p>
					<a class="btn btn-large btn-primary" href="{{ url('/dasbor') }}">KEMBALI</a>
				</div>
			</div>
		</div>
	</div>
</section>
@stop
