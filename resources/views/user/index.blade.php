@extends('layout.default')

@section('styles-pre')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
<style>
	.select2-container--default .select2-results__option[aria-disabled=true] {
		display: none;
	}
</style>
@endsection

@section('content')
<section class="content-header">
	<h1>Menajemen Pengguna</h1>
	<h2>Daftar Pengguna</h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('/') }}"><i class="ion-speedometer"></i> Dasbor</a></li>
		<li>Manajemen Pengguna</li>
		<li class="active">Daftar Pengguna</li>
	</ol>
	@if (app('access')['create'] == '1')
		<button type="button" class="btn btn-primary" id="indexAdd">Tambah Pengguna</button>
	@endif
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<table id="table" class="table table-striped table-responsive dataTable no-footer" data-tables="true" width="100%">
						<thead>
							<tr>
								<th class="text-right" width="10">No</th>
								<th>Nama</th>
								<th class="text-nowrap">Alamat Surel</th>
								<th>NIP</th>
								<th>Jabatan</th>
								<th>Peran</th>
								<th width="10">&nbsp;</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('modal')
<div class="modal fade stick-up" id="modaldetail" role="dialog" aria-hidden="false">
	<div class="modal-dialog">
		<div class="modal-content-wrapper">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Detil Pengguna</h4>
				</div>

				@if (app('access')['update'] == '1')
					<form method="POST" id="formupdate" action="">
						{{ method_field('PUT') }}
						{{ csrf_field() }}
				@endif

				<div class="modal-body box box-primary box-full">
					<div class="box-body">
						<div class="form-group inputan">
							<label>Nama</label>
							<div id="name"></div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group inputan">
									<label>Alamat Surel</label>
									<div id="email"></div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group inputan">
									<label>NIP</label>
									<div id="nip"></div>
								</div>
							</div>
						</div>

						<div class="form-group inputan">
							<label>Jabatan</label>
							<div id="position"></div>
						</div>

						<div class="form-group inputan">
							<label>Unit</label>
							<div id="unit"></div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								@php
									$error = false;
									if (app('access')['update'] == '1' AND $errors->first('role_name') != ''):
										$error = true;
									endif;
								@endphp
								<div class="form-group req {{ $error ? 'has-error' : '' }} {{ app('access')['update'] == '0' ? 'inputan' : '' }}">
									<label {{ (app('access')['update'] == '1' ? 'for="role_name"' : '') }}>Peran</label>
									@if (app('access')['update'] == '0')
										<div id="role"></div>
									@else
										<select name="role_name" id="role_name" class="form-control select2" style="width: 100%;">
											<option value="">&#8212; Pilih peran &#8212;</option>
											@foreach ($roles as $k => $v)
												<option value="{{ $v->id }}">{{ $v->role }}</option>
											@endforeach
										</select>
										@if ($error)
											<span class="help-block">{{ $errors->first('role_name') }}</span>
										@endif
									@endif
								</div>
							</div>
						</div>

						@if (app('access')['update'] == '1')
							<div class="forminfo">
								<i class="ion ion-asterisk text-red"></i> Wajib diisi
							</div>
						@endif
					</div>
				</div>

				<div class="modal-footer">
					@if (app('access')['update'] == '1')
						<button type="submit" class="btn btn-primary">Simpan</button>
					@endif
					<button type="button" class="btn btn-default {{ app('access')['update'] == '1' ? 'pull-right' : '' }}" data-dismiss="modal">Tutup</button>
				</div>

				@if (app('access')['update'] == '1')
					</form>
				@endif
			</div>
		</div>
	</div>
</div>

@if (app('access')['create'] == '1' OR app('access')['update'] == '1')
	<div class="modal fade stick-up" id="modalform" role="dialog" aria-labelledby="modalProc" tabindex="-1" aria-hidden="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title" id="modalProc"><span></span> Pengguna</h4>
					</div>

					<form method="POST" id="formuser" action="">
						{{ method_field('POST') }}
						{{ csrf_field() }}

						<div class="modal-body box box-primary box-full">
							<div class="box-body">
								<div class="form-group req {{ $errors->first('name') != '' ? 'has-error' : '' }}">
									<label for="name">Nama</label>
									<input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
									@if ($errors->first('name') != '')
										<span class="help-block">{{ $errors->first('name') }}</span>
									@endif
								</div>

								<div class="form-group req {{ $errors->first('email') != '' ? 'has-error' : '' }}">
									<label for="email">Alamat Surel</label>
									<input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}">
									@if ($errors->first('email') != '')
										<span class="help-block">{{ $errors->first('email') }}</span>
									@endif
								</div>

								<div class="form-group ispass {{ $errors->first('password') != '' ? 'has-error' : '' }}">
									<label for="password">Kata Sandi</label>
									<input type="text" name="password" id="password" class="form-control" value="{{ old('password') }}">
									@if ($errors->first('password') != '')
										<span class="help-block">{{ $errors->first('password') }}</span>
									@endif
								</div>

								<div class="form-group req {{ $errors->first('roles_id') != '' ? 'has-error' : '' }}">
									<label for="password">Peran</label>
									<select name="roles_id" id="roles_id" class="form-control select2" style="width: 100%;">
										<option value="">&#8212; Pilih peran &#8212;</option>
										@foreach ($roles as $k => $v)
											@if ($v->id == 3 OR $v->id == 5 OR $v->id == 6)
												<option value="{{ $v->id }}">{{ $v->role }}</option>
											@endif
										@endforeach
									</select>
									@if ($errors->first('roles_id') != '')
										<span class="help-block">{{ $errors->first('roles_id') }}</span>
									@endif
								</div>

								<br>
								<div class="forminfo">
									<i class="ion ion-asterisk text-red"></i> Wajib diisi
								</div>
								<div class="forminfo isupdate">
									<i class="ion ion-information text-blue"></i> Kosongkan bila tidak ingin mengganti
								</div>
							</div>
						</div>

						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Simpan</button>
							<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Tutup</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endif

@if (app('access')['delete'] == '1')
	<div class="modal fade stick-up" id="modaldel" role="dialog" aria-labelledby="modalProc" tabindex="-1" aria-hidden="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title" id="modalProc">Hapus Data Pengguna</h4>
					</div>

					<form method="POST" id="formhapus" action="">
						{{ method_field('DELETE') }}
						{{ csrf_field() }}

						<div class="modal-body box box-danger">
							<div class="box-body">
								<div class="row">
									<div class="col-sm-12">
										Apakah Anda yakin ingin menghapus data <b class="user-name"></b>?
									</div>
								</div>
							</div>
						</div>

						<div class="modal-footer">
							<button type="submit" class="btn btn-danger">Hapus</button>
							<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Tutup</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endif
@endsection

@section('scripts')
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var table = $("#table").DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ url('pengguna/data-pengguna')}}",
			columns: [
				{ data: "rownum", name: "rownum", class: "text-right", searchable: false },
				{ data: "name", name: "name" },
				{ data: "email", name: "email", render: function(data) {
					return "<a href='mailto:" + data + "'>" + data + "</a>";
				}},
				{ data: "nip", name: "nip" },
				{ data: "position", name: "p.position" },
				{ data: "role", name: "r.role", render: function(data) {
					var warna = "primary";
					if (data == "Teknisi") {
						warna = "warning";
					} else if (data == "Pengguna") {
						warna = "success";
					}
					return "<span class='label label-" + warna + "'>" + data + "</span>";
				}},
				{ data: "action", class: "text-nowrap", orderable: false, searchable: false }
			],
			language: {
				url: "{{ asset('lang/Indonesian.json') }}"
			},
			responsive: true,
			initComplete: function() {
				$("#table").wrap("<div class='table-responsive'></div>");
				$("select[name='table_length']").select2({
					minimumResultsForSearch: Infinity
				});
			}
		});

		$(".select2").select2({
			minimumResultsForSearch: Infinity
		});
	});

	@if (session('success') != '')
		Lobibox.notify("success", {
			icon: "ion ion-android-checkmark-circle",
			title: "Berhasil",
			msg: "Data pengguna '<b>{{ session('success') }}</b>' telah berhasil {{ session()->has('erase') ? 'dihapus' : 'disimpan' }}.",
			sound: false
		});
	@endif

	@if (app('access')['create'] == '1')
		$("#indexAdd").on("click", function() {
			tambah();
		});

		@if (count($errors) > 0 AND session()->has('errornew'))
			tambah();
		@endif

		function tambah() {
			$("#formuser").attr("action", "{{ url('pengguna/tambah-pengguna') }}");
			$("#formuser input[name='_method']").val("POST");
			$(".modal-title span").text("Tambah");
			$(".ispass").addClass("req");
			$(".isupdate").hide();
			$("#modalform").modal("show");
		}
	@endif

	function detil(userid, old) {
		$.ajax({
			method: "GET",
			url: "{{ url('pengguna/data-detil') }}/" + userid,
			dataType: "json",
			fail: function() {
				//
			},
			error: function(jqXHR, textStatus, errorThrown) {
				// console.log(textStatus);
				// console.log(errorThrown);
			},
			success: function(data) {
				@if (app('access')['update'] == '0')
					$("#role").text(data.role);
				@else
					$("#formupdate").attr("action", "{{ url('pengguna') }}/" + userid);
					if (old == "") {
						if (data.simpeg_id == 0) {
							$("#role_name option[value='1'], #role_name option[value='2'], #role_name option[value='4'], #role_name option[value='7']").prop("disabled", true);
							if (userid == 1) {
								$("#role_name").prop("disabled", true);
							} else {
								$("#role_name").prop("disabled", false);
							}
						} else {
							$("#role_name option").prop("disabled", false);
						}
						$("#role_name").val(data.roles_id);
						$("#role_name").trigger("change");
					};
				@endif
				$("#name").text(data.name);
				$("#email").html("<a href='mailto:" + data.email + "'>" + data.email + "</a>");
				$("#nip").text(data.nip);
				var pos = data.position;
				if (data.position_short != "" && data.position_short != data.position) {
					pos = data.position_short + "<br><small><i>" + pos + "</i></small>";
				}
				$("#position").html(pos);
				var unit = data.unit;
				if (data.unit_short != "" && data.unit_short != data.unit) {
					unit = data.unit_short + "<br><small><i>" + unit + "</i></small>";
				}
				$("#unit").html(unit);
				$("#modaldetail").modal("show");
			}
		});
	}

	$("#modaldetail").on("hidden.bs.modal", function() {
		@if (app('access')['update'] == '0')
			$("#role_name").text("");
		@else
			$(".form-group").removeClass("has-error");
			$(".help-block").remove();
			$("#formupdate").attr("action", "");
			$("#role_name").val("");
			$("#role_name").trigger("change");
		@endif
		$("#role_name").prop("disabled", false);
		$("#role_name option").prop("disabled", false);
		$("#role_name").select2("val", "");
		$("#role_name").select2({
			minimumResultsForSearch: Infinity
		});
		$("#name, #email, #simpeg, #nip, #position").text("");
	});

	@if (count($errors) > 0 AND session()->has('userid'))
		detil({{ session('userid') }}, "old");
	@endif

	@if (app('access')['update'] == '1')
		function update(userid, old) {
			$.ajax({
				method: "GET",
				url: "{{ url('pengguna/data-detil') }}/" + userid,
				dataType: "json",
				fail: function() {
					//
				},
				error: function(jqXHR, textStatus, errorThrown) {
					// console.log(textStatus);
					// console.log(errorThrown);
				},
				success: function(data) {
					$("#formuser").attr("action", "{{ url('pengguna/sunting-pengguna') }}/" + userid);
					$("#formuser input[name='_method']").val("PUT");
					$(".modal-title span").text("Sunting Data");
					$(".ispass").addClass("info");
					$(".isupdate").show();
					// $("#formupdate").show();
					$("#modalform").modal("show");

					if (old == "") {
						$("#formuser #roles_id").val(data.roles_id);
						$("#formuser #roles_id").trigger("change");
					};
					$("#formuser #name").val(data.name);
					$("#formuser #email").val(data.email);
					$("#modalform").modal("show");
				}
			});
		}

		@if (count($errors) > 0 AND session()->has('errorupdate'))
			update({{ session('errorupdate') }}, "");
		@endif
	@endif

	@if (app('access')['create'] == '1' OR app('access')['update'] == '1')
		$("#modalform").on("hidden.bs.modal", function() {
			$(".ispass").removeClass("req").removeClass("info");
			$(".form-group").removeClass("has-error");
			$(".help-block").remove();
			$("#formuser").attr("action", "");
			$("#roles_id").val("");
			$("#roles_id").trigger("change");
			$("#formuser #name, #formuser #email, #formuser #password, #formuser input[name='_method']").val("");
		});
	@endif

	@if (app('access')['delete'] == '1')
		function hapus(userid, nama) {
			$("#formhapus").attr("action", "{{ url('pengguna/hapus-pengguna') }}/" + userid);
			$("b.user-name").text(nama);
			$("#modaldel").modal("show");
		}
		$("#modaldel").on("hidden.bs.modal", function() {
			$("#formhapus").attr("action", "");
		});
	@endif
</script>
@endsection
