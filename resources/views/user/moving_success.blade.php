@extends('layout.default')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="flex-center position-ref">
                <div class="container">
                    <div class="col-md-12 text-center">
                        <h3>PROSES TIKET PEMINDAHAAN {{ $name }}</h3>
                        <p>
                            Proses permintaan tiket pemindahaan telah kami terima.
                        </p>
                        <a class="btn btn-large btn-primary" href="{{ url('/dasbor') }}">KEMBALI</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop