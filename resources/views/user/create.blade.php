@extends('layout.default')

@section('styles-pre')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<section class="content-header">
	<h1>Menajemen Pengguna</h1>
	<h2>Tambah Pengguna</h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('/') }}"><i class="ion-speedometer"></i> Dasbor</a></li>
		<li>Manajemen Pengguna</li>
		<li class="active">Tambah Pengguna</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@if (session('success') != '')
				<!-- <div class="alert alert-info">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="ion ion-android-checkmark-circle" style="vertical-align: text-bottom;"></i>&nbsp; Berhasil</h4>
					Data pengguna '<b>{{ session('success') }}</b>' telah berhasil disimpan.
				</div> -->
			@endif

			<form method="POST" id="formcreate" action="">
				{{ csrf_field() }}
			</form>
		</div>
	</div>
</section>
@endsection
