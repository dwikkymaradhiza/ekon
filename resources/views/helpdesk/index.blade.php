@extends('layout.default')

@section('styles-pre')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<section class="content-header">
	<h1>Manajemen Pemeliharaan</h1>
	<h2>Daftar Permintaan Pemeliharaan</h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('/') }}"><i class="ion-speedometer"></i> Dasbor</a></li>
		<li>Manajemen Pemeliharaan</li>
		<li class="active">Daftar Permintaan</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<table id="table" class="table table-striped table-responsive dataTable no-footer" data-tables="true" width="100%">
						<thead>
							<tr>
								<th width="10">No</th>
								<th>Tiket</th>
								<th>NUP</th>
								<th>Nama Aset</th>
								<th>Pemohon</th>
								<th>Tgl. Pengajuan</th>
								<th width="30">Status</th>
								<th width="10">&nbsp;</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('modal')
<div class="modal fade stick-up" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrapper">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Detil Permintaan</h4>
				</div>

				<div class="modal-body box-wrapper box-primary">
					<div class="box-wrapper-body">
						<h4 id="ticket"></h4>

						<div class="row">
							<div class="col-sm-6">
								<div class="box box-success collapsed-box">
									<div class="box-header with-border">
										<h3 class="box-title">Aset</h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>
									<div class="box-body">
										<div class="form-group">
											<label>Nama Aset</label>
											<div id="asset_name"></div>
										</div>

										<div class="form-group">
											<label>NUP</label>
											<div id="nup"></div>
										</div>

										<div class="form-group">
											<label>Kategori</label>
											<div id="type"></div>
										</div>

										<div class="form-group">
											<label>Lokasi</label>
											<div id="location"></div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="box box-success collapsed-box">
									<div class="box-header with-border">
										<h3 class="box-title">Pemohon</h3>

										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>

									<div class="box-body">
										<div class="form-group">
											<label>Nama</label>
											<div id="user_name"></div>
										</div>

										<div class="form-group">
											<label>NIP</label>
											<div id="nip"></div>
										</div>

										<div class="form-group">
											<label>Posisi</label>
											<div id="position">IT</div>
										</div>

										<div class="form-group">
											<label>Tanggal Pengajuan</label>
											<div id="created_at"></div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="box box-primary">
									<div class="box-header with-border">
										<h3 class="box-title">Keluhan</h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>

									<div class="box-body" id="keluhanBox">
										<div id="complaint"></div>
									</div>
								</div>

								<!-- Laporan Awal kalo udah dibuat -->
								<div class="box box-primary hidden" id="laporanAwal">
									<div class="box-header with-border">
										<h3 class="box-title">Laporan Awal</h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>

									<div class="box-body">
										<div class="form-group">
											<label>Pengecekan oleh</label>
											<div id="namaTehnisi"></div>
										</div>
										<div class="form-group">
											<label>Laporan Awal</label>
											<div id="laporanInit"></div>
										</div>
										<div class="form-group">
											<label>Perkiraan Biaya</label>
											<div id="costInit"></div>
										</div>
									</div>
								</div>
								<!-- Laporan Awal kalo udah dibuat -->
							</div>

							<div class="col-sm-6">
								<div class="box box-primary">
									<div class="box-header with-border">
										<h3 class="box-title">{{ (Auth::user()->roles_id < 3 OR Auth::user()->roles_id == 7) ? 'Penanganan' : 'Laporan Awal' }}</h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>

									<div class="box-body">
										<!-- Pilih Teknisi -->
										<p id="teknisi-txt"></p>
										<select class="form-control select2" id="teknisi" name="technician" style="width: 100%" {{ (app('access')['update'] == '0' ? 'disabled' : '') }}>
											<option value="">&#8211; Pilih teknisi &#8211;</option>
											@foreach($users as $row)
												<option value="{{ $row->id }}">{{ $row->name }}</option>
											@endforeach
										</select>
										<!-- Pilih Teknisi -->

										<!-- Laporan Teknisi -->
										<div class="form-group" id="infoGroup">
											<label for="information">Laporan Pengecekan</label>
											<textarea name="information" id="information" class="form-control init_info" rows="3" {{ (app('access')['update'] == '0' ? 'disabled' : '') }}></textarea>
										</div>
										<div class="form-group" id="costGroup">
											<label for="cost">Perkiraan Biaya</label>
											<div class="input-group">
												<div class="input-group-addon">Rp.</div>
												<input type="text" name="cost" class="form-control init_cost" placeholder="0" id="cost" {{ (app('access')['update'] == '0' ? 'disabled' : '') }}>
											</div>
										</div>
										<!-- Laporan Teknisi -->

										<!-- Final -->
										<div class="form-group" id="notesGroup">
											<label for="notes">Penyelesaian</label>
											<textarea name="notes" id="notes" class="form-control" rows="3" {{ (app('access')['update'] == '0' ? 'disabled' : '') }}></textarea>
										</div>

										<div class="form-group" id="cost_finalGroup">
											<label for="cost_final">Biaya</label>
											<div class="input-group">
												<div class="input-group-addon">Rp.</div>
												<input type="text" name="cost_final" class="form-control" placeholder="0" id="cost_final" {{ (app('access')['update'] == '0' ? 'disabled' : '') }}>
											</div>
										</div>
										<!-- Final -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					@if (app('access')['update'] == '1')
						<button type="button" id="laporan" class="btn btn-primary id">Proses</button>
						<button type="button" id="setuju" value="" class="btn btn-primary">Proses</button>
						<button type="button" id="prosesPengecekan" value="" class="btn btn-primary id">Proses</button>
						<button type="button" id="penggantian" value="" class="btn btn-info">Penggantian</button>
						<button type="button" id="tolak" value="" class="btn btn-danger id">Tolak</button>
						<button type="button" id="tolakPersetujuan" value="" class="btn btn-danger id">Tolak</button>
					@endif
					<button type="button" id="batalkan" class="btn btn-default pull-right" data-dismiss="modal">Batal</button>
					<!-- 0 = Waiting Approval, 1 = Field Checking, 2 = Initial Report, 3 = Done, 4 = Rejected -->
				</div>

				<div class="box box-default collapsed-box1">
					<div class="box-header with-border">
						<h3 class="box-title">Log Pemeliharaan Aset</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="ion-minus"></i>
								<i class="ion-plus"></i>
							</button>
						</div>
					</div>

					<div class="box-body">
						<table id="detail" class="table table-striped table-responsive dataTable no-footer" data-tables="true" width="100%">
							<thead>
								<tr>
									<th width="10">No</th>
									<th>Masalah</th>
									<th>Tgl. Pengajuan</th>
									<th>Penyelesaian</th>
									<th>Tgl. Penyelesaian</th>
									<th>Biaya</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/inputmask/inputmask.js') }}"></script>
<script src="{{ asset('plugins/inputmask/inputmask.extensions.js') }}"></script>
<script src="{{ asset('plugins/inputmask/inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('plugins/inputmask/jquery.inputmask.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#teknisi").select2({ dropdownParent: $("#modal_edit") });

	$("#modal_edit").on("show.bs.modal", function(e) {
		var Id = $(e.relatedTarget).data("id");
		var ticket = $(e.relatedTarget).data("ticket");
		var nup = $(e.relatedTarget).data("nup");
		var created_at = $(e.relatedTarget).data("created_at");
		var asset_name = $(e.relatedTarget).data("asset_name");
		var asset_item_id = $(e.relatedTarget).data("asset_item_id");
		var complaint = $(e.relatedTarget).data("complaint");
		var user_name = $(e.relatedTarget).data("user_name");
		var nip = $(e.relatedTarget).data("nip");
		var position = $(e.relatedTarget).data("position");
		var type = $(e.relatedTarget).data("type");
		var location = $(e.relatedTarget).data("location");
		var user_id = $(e.relatedTarget).data("user_id");
		var status = $(e.relatedTarget).data("status");
		var tolak = $(e.relatedTarget).data("tolak");
		var technician = $(e.relatedTarget).data("technician");
		var tehName = $(e.relatedTarget).data("tehname");
		var room = $(e.relatedTarget).data("room");
		var laporanInit = $(e.relatedTarget).data("init_info");
		var costInit = $(e.relatedTarget).data("init_cost");

		$("#ticket").html("Nomor Tiket: " + ticket);
		$("#nup").html(nup);
		$("#asset_name").html(asset_name);
		$("#type").html(type);
		$("#complaint").html(complaint);
		$("#user_name").html(user_name);
		$("#nip").html(nip);
		$("#position").html(position);
		$("#created_at").html(created_at);
		$("#type").html(type);
		$("#location").html(location);
		$("#setuju").val(Id);
		$("#tolak").val(Id);
		$("#user_id").val(user_id);
		$("#laporanInit").html(laporanInit);
		$("#costInit").html(costInit);
		$(".id").val(Id);
		$(".laporan").val(Id);
		$(".init_info").val(laporanInit);
		$(".init_cost").val(costInit);
		$("#teknisi").val(technician).trigger("change");

		if (status == 0) {
			$("#keluhanBox").css("min-height", "95px");

			$("#setuju").show();
			$("#prosesPengecekan").hide();
			$("#tolak").hide();
			$("#penggantian").hide();
			$("#laporan").hide();
			$("#teknisi").prop('disabled', false);

			$("#notesGroup").hide();
			$("#cost_finalGroup").hide();

			$("#teknisi-txt").text("Pilihlah teknisi untuk melakukan pengecekan lapangan");
			$("#teknisi").show();
			$("#teknisi").next(".select2").show();

			$("#infoGroup").hide();
			$("#costGroup").hide();

			$("#tolakPersetujuan").show();
		}
		if (status == 1) {
			$("#setuju, #laporan, #tolak, #notesGroup, #cost_finalGroup, #penggantian").hide();

			@if (Auth::user()->roles_id < 3 OR Auth::user()->roles_id == 7)
				$("#keluhanBox").css("min-height", "82px");
				$("#teknisi-txt").html("Sedang dalam pengecekan lapangan oleh:<br/><b>"+tehName+"</b>");
				$("#teknisi, #prosesPengecekan, #infoGroup, #costGroup, #batalkan").hide();
				$("#teknisi").next(".select2").hide();
			@elseif (Auth::user()->roles_id == 3)
				$("#keluhanBox").css("min-height", "200px");
				$("#teknisi-txt, #teknisi").hide();
				$("#teknisi").next(".select2").hide();
				$("#infoGroup, #costGroup, #prosesPengecekan").show();
			@endif
			$("#tolakPersetujuan").hide();
		}
		if (status == 2) {
			@if (Auth::user()->roles_id < 3 OR Auth::user()->roles_id == 7)
				$("#laporanAwal").removeClass("hidden");
				$("#setuju").hide();
				$("#laporan, #tolak, #notesGroup, #cost_finalGroup").show();
				$("#penggantian, #prosesPengecekan, #infoGroup, #costGroup, #teknisi").hide();
				$("#teknisi").next(".select2").hide();
				$("#namaTehnisi").html(tehName);
				$("#notes").css("min-height", "158px");
			@elseif (Auth::user()->roles_id == 3)
				$("#keluhanBox").css("min-height", "200px");

				$("#setuju, #laporan, #tolak, #penggantian, #prosesPengecekan, #batalkan, #teknisi, #notesGroup, #cost_finalGroup").hide();
				$("#teknisi").next(".select2").hide();

				$("#infoGroup, #costGroup").show();
				$("#information, #cost").attr("disabled", true);
			@endif

			$("#tolakPersetujuan, #teknisi-txt").hide();
		}

		var urlDetail = "pemeliharaan/data-detil/" + asset_item_id;
		var detail = $("#detail").DataTable({
			processing: true,
			serverSide: true,
			bFilter: false,
			bInfo: false,
			destroy: true,
			searching: false,
			paging: false,
			ajax: urlDetail,
			columns: [
				{ data: "rownum", name: "rownum", class: "text-right" },
				{ data: "complaint", name: "complaint" },
				{ data: "created_at", name: "created_at", class: "text-nowrap" },
				{ data: "info", name: "info" },
				{ data: "done_at", name: "done_at", class: "text-nowrap" },
				{ data: "cost", name: "cost", class: "text-nowrap" }
			],
			language: {
				url: "{{ asset('lang/Indonesian.json') }}"
			},
			responsive: true,
			initComplete: function() {
				$("#detail").wrap("<div class='table-responsive'></div>");
				$("select[name='table_length']").select2({
					minimumResultsForSearch: Infinity
				});
			}
			// order: [ [3, "desc"] ]
		});
	});
	$("#modal_edit").on("hidden.bs.modal", function(e) {
		$("#ticket, #nup, #asset_name, #type, #complaint, #user_name, #nip, #position, #created_at, #type, #location, #laporanInit, #costInit, #teknisi-txt, #namaTehnisi").html("");
		$("#setuju, #tolak, #user_id, .id, .laporan, .init_info, .init_cost").val("");
		$("#teknisi").val("").trigger("change");
		$("#keluhanBox, #notes").removeAttr("style");
		$("#teknisi, #information, #cost").prop('disabled', false);
	});

	var table = $("#table").DataTable({
		processing: true,
		serverSide: true,
		ajax: "{{ url('pemeliharaan/data-daftar')}}",
		columns: [
			{ data: "rownum", name: "rownum", class: "text-right" },
			{ data: "ticket", name: "s.ticket" },
			{ data: "nup", name: "a.nup" },
			{ data: "asset_name", name: "a.name" },
			{ data: "user_name", name: "u.name" },
			{ data: "created_at", name: "s.created_at", class: "text-nowrap" },
			{ data: "status", orderable: false, searchable: false, render: function (data) {
				if (data == "0") {
					return "<span class='label label-info'>Menunggu Persetujuan</span>";
				}
				if (data == "1") {
					return "<span class='label label-warning'>Pengecekan Keluhan</span>";
				}
				if (data == "2") {
					return "<span class='label label-success'>Laporan Awal</span>";
				}
			}},
			{ data: "action", orderable: false, searchable: false}
		],
		language: {
			url: "{{ asset('lang/Indonesian.json') }}"
		},
		responsive: true,
		initComplete: function() {
				$("#table").wrap("<div class='table-responsive'></div>");
				$("select[name='table_length']").select2({
					minimumResultsForSearch: Infinity
				});
			}
		// order: [ [3, "desc"] ]
	});

	$("#form_edit_checking").validate({
		rules: {
			information: {
				required: true
			},
			cost: {
				required: true,
				number:true
			}
		},
		messages: {
			information: {
				required: "information is required"
			},
			 cost: {
				required: "cost is required",
				number:"Hanya Angka"
			},
		}
	});

	$("#form_edit_report").validate({
		rules: {
			notes: {
				required: true
			}
		},
		messages: {
			notes: {
				required: "notes is required"
			}
		}
	});

	var idUser = $(".user_id").val();

	// button Approve
	$("#setuju").click("button", function (e) {
		e.preventDefault();
		var teknisi = document.getElementById("teknisi").value;
		if (teknisi == "") {
			Lobibox.notify("error", {
				icon: "ion ion-android-close",
				msg: "Pilih salah satu teknisi!",
				sound: false
			});
		} else {
			var id = $(this).val();
			var data = "id=" + id + "&status=1&technician=" + teknisi;

			$.ajax({
				headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
				url: "{{ url('pemeliharaan/update')}}",
				data: data,
				type: "post",
				dataType: "json",
				success: function (response) {
					if (response.status === true) {
						Lobibox.notify("success", {
							icon: "ion ion-android-checkmark-circle",
							title: "Berhasil",
							msg: "Data telah berhasil diproses.",
							sound: false
						});
						$("#modal_edit").modal("hide");
						table.ajax.reload();
					} else {
						Lobibox.notify("error", {
							icon: "ion ion-android-close",
							msg: "Pemrosesan data gagal!",
							sound: false
						});
					}
				}
			});
		}
	});

	// button reject
	$("#tolak").click("button", function (e) {
		e.preventDefault();
		var notes = $("#notes").val();
		if (notes == "") {
			Lobibox.notify("error", {
				icon: "ion ion-android-close",
				msg: "Laporan tidak boleh kosong!",
				sound: false
			});
		} else {
			var id = $(this).val();
			var data = "id=" + id + "&status=4&notes=" + notes;

			$.ajax({
				headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
				url: "{{ url('pemeliharaan/update')}}",
				data: data,
				type: "post",
				dataType: "json",
				success: function (response) {
					if (response.status === true) {
						Lobibox.notify("success", {
							icon: "ion ion-android-checkmark-circle",
							title: "Berhasil",
							msg: "Data telah berhasil diproses.",
							sound: false
						});
						 $("#modal_edit").modal("hide");
							table.ajax.reload();

					} else {
						Lobibox.notify("error", {
							icon: "ion ion-android-close",
							msg: "Pemrosesan data gagal!",
							sound: false
						});
					}
				}
			});
		}
	});

	$("#tolakPersetujuan").click("button", function (e) {
		e.preventDefault();

		var id = $(this).val();
		var data = "id=" + id + "&status=4";

		$.ajax({
			headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
			url: "{{ url('pemeliharaan/update')}}",
			data: data,
			type: "post",
			dataType: "json",
			success: function (response) {
				if (response.status === true) {
					Lobibox.notify("success", {
						icon: "ion ion-android-checkmark-circle",
						title: "Berhasil",
						msg: "Data telah berhasil diproses.",
						sound: false
					});
					 $("#modal_edit").modal("hide");
						table.ajax.reload();

				} else {
					Lobibox.notify("error", {
						icon: "ion ion-android-close",
						msg: "Pemrosesan data gagal!",
						sound: false
					});
				}
			}
		});
	});

	$("#checking").click("button", function (e) {
		e.preventDefault();
		var id = $(this).val();
		var data = "id=" + id + "&status=2";

		$.ajax({
		headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
		url: "{{ url('pemeliharaan/update')}}",
		data: data,
		type: "post",
		dataType: "json",
		success: function (response) {
			if (response.status === true) {
				Lobibox.notify("success", {
					icon: "ion ion-android-checkmark-circle",
					title: "Berhasil",
					msg: "Data telah berhasil diproses.",
					sound: false
				});
				 $("#modal_edit").modal("hide");
					table.ajax.reload();

			} else {
				Lobibox.notify("error", {
					icon: "ion ion-android-close",
					msg: "Pemrosesan data gagal!",
					sound: false
				});
			 }
		  }
		});
	});

	$("#laporan").click("button", function (e) {
		e.preventDefault();
		var notes = $("#notes").val();
		var cost_final = $("#cost_final").val();
		if (notes == "" || cost_final == "") {
			Lobibox.notify("error", {
				msg: "Laporan dan biaya tidak boleh kosong!",
				sound: false
			});
		} else {
			var id = $(this).val();
			var data = "id=" + id + "&status=3&notes=" + notes + "&cost_final=" + cost_final;
			$.ajax({
			headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
			url: "{{ url('pemeliharaan/update')}}",
			data: data,
			type: "post",
			dataType: "json",
			success: function (response) {
				if (response.status === true) {
					Lobibox.notify("success", {
						icon: "ion ion-android-checkmark-circle",
						title: "Berhasil",
						msg: "Data telah berhasil diproses.",
						sound: false
					});
					$("#modal_edit").modal("hide");
						table.ajax.reload();

				} else {
					Lobibox.notify("error", {
						icon: "ion ion-android-close",
						msg: "Pemrosesan data gagal!",
						sound: false
					});
				 }
			  }
			});
		}
	});

	$("#prosesPengecekan").click("button", function (e) {
		e.preventDefault();
		var information = $("#information").val();
		var cost = $("#cost").val();
		if (information == "" || cost == "") {
			Lobibox.notify("error", {
				icon: "ion ion-android-close",
				msg: "Laporan dan biaya tidak boleh kosong!",
				sound: false
			});
		} else {
			var id = $(this).val();
			var data = "id=" + id + "&status=2&information=" + information + "&cost=" + cost;
			$.ajax({
			headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
			url: "{{ url('pemeliharaan/update')}}",
			data: data,
			type: "post",
			dataType: "json",
			success: function (response) {
				if (response.status === true) {
					Lobibox.notify("success", {
						icon: "ion ion-android-checkmark-circle",
						title: "Berhasil",
						msg: "Data telah berhasil diproses.",
						sound: false
					});
					 $("#modal_edit").modal("hide");
						table.ajax.reload();

				} else {
					Lobibox.notify("error", {
						icon: "ion ion-android-close",
						msg: "Pemrosesan data gagal!",
						sound: false
					});
				 }
			  }
			});
		}
	});

	$("#cost, #cost_final").inputmask("numeric", {
		radixPoint: ",",
		groupSeparator: ".",
		digits: 0,
		autoGroup: true,
		prefix: "",
		rightAlign: false,
	});

	$("#modal_edit").on("hidden.bs.modal", function() {
		$("#teknisi").val("");
		$("#teknisi").trigger("change");
	});
 });
</script>
@endsection
