<form id="form_edit_report" action="#" method="post">
	<div class="row">
		<div class="col-sm-4">
			<div class="form-group required">
				<label for="information">Notes</label>
				<input type="text" name="notes" class="form-control" placeholder="example: ... ">
				<input type="hidden" name="status" class="form-control" value="3">
				<input type="hidden" name="id" class="form-control id">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<button type="submit" class="btn btn-primary"><span class="bold">Submit</span></button>
		</div>
	</div>
</form>
