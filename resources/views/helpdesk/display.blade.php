@extends('layout.display')

@section('styles-pre')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<section class="content-header">
	<h2>Daftar Permintaan Pemeliharaan</h2>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<table id="table" class="table table-striped table-responsive dataTable no-footer" data-tables="true" width="100%">
						<thead>
							<tr>
								<th width="10">No</th>
								<th>Tiket</th>
								<th>NUP</th>
								<th>Nama Aset</th>
								<th>Pemohon</th>
								<th>Tgl. Pengajuan</th>
								<th width="30">Status</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('scripts')
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/inputmask/inputmask.js') }}"></script>
<script src="{{ asset('plugins/inputmask/inputmask.extensions.js') }}"></script>
<script src="{{ asset('plugins/inputmask/inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('plugins/inputmask/jquery.inputmask.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
	var table = $("#table").DataTable({
				searching: false,
				paging: false,
				ordering: false,
				bInfo : false,
		processing: false,
		serverSide: true,
		ajax: "{{ url('pemeliharaan/data-daftar-tampilan')}}",
		columns: [
			{ data: "rownum", name: "rownum", class: "text-right" },
			{ data: "ticket", name: "s.ticket" },
			{ data: "nup", name: "a.nup" },
			{ data: "asset_name", name: "a.name" },
			{ data: "user_name", name: "u.name" },
			{ data: "created_at", name: "s.created_at", class: "text-nowrap" },
			{ data: "status", orderable: false, searchable: false, render: function (data) {
				if (data == "0") {
					return "<span class='label label-info'>Menunggu Persetujuan</span>";
				}
				if (data == "1") {
					return "<span class='label label-warning'>Pengecekan Keluhan</span>";
				}
				if (data == "2") {
					return "<span class='label label-success'>Laporan Awal</span>";
				}
			}}
		],
		language: {
			url: "{{ asset('lang/Indonesian.json') }}"
		},
		responsive: true,
		initComplete: function() {
						$("#table").wrap("<div class='table-responsive'></div>");
						$("select[name='table_length']").select2({
								minimumResultsForSearch: Infinity
						});
				}
		// order: [ [3, "desc"] ]
	});

		setInterval(function () {
			table.ajax.reload();
		}, 3600000);
 });
</script>
@endsection
