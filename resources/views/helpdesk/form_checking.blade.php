	<form id="form_edit_checking" method="post" action="#">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group required">
					<label for="information">Information</label>
					<input type="text" name="information" class="form-control" placeholder="example: ... ">
					<input type="hidden" name="status" class="form-control" value="2">
					<input type="hidden" name="id" class="form-control id">
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group required">
					<label for="cost">Cost</label>
					<input type="text" name="cost" class="form-control" placeholder="example: ...">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<button type="submit" class="btn btn-primary"><span class="bold">Submit</span></button>
			</div>
		</div>
	</form>
