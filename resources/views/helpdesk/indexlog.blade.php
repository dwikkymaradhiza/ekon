@extends('layout.default')

@section('content')
<section class="content-header">
	<h1>Manajemen Pemeliharaan</h1>
	<h2>Daftar Log Pemeliharaan</h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('/') }}"><i class="ion-speedometer"></i> Dasbor</a></li>
		<li>Manajemen Pemeliharaan</li>
		<li class="active">Log Pemeliharaan</li>
	</ol>
</section>


<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<table id="table" class="table table-striped dataTable no-footer" data-tables="true" width="100%">
						<thead>
							<tr>
								<th width="10">No</th>
								<th>Tiket</th>
								<th>NUP</th>
								<th>Nama Aset</th>
								<th>Pemohon</th>
								<th>Tgl. Pengajuan</th>
								<th>Penyelesaian</th>
								<th>Tgl. Penyelesaian</th>
								<th>Biaya</th>
								<th>Status</th>
								<th width="10">&nbsp;</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<br>
@endsection

@section('modal')
<div class="modal fade stick-up" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrapper">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Detil Permintaan</h4>
				</div>

				<div class="modal-body box-wrapper box-primary">
					<div class="box-wrapper-body">
						<h4 id="ticket"></h4>

						<div class="row">
							<div class="col-sm-6">
								<div class="box box-success">
									<div class="box-header with-border">
										<h3 class="box-title">Aset</h3>

										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>

									<div class="box-body">
										<div class="form-group">
											<label>Nama Aset</label>
											<div id="asset_name"></div>
										</div>

										<div class="form-group">
											<label>NUP</label>
											<div id="nup"></div>
										</div>

										<div class="form-group">
											<label>Kategori</label>
											<div id="type"></div>
										</div>

										<div class="form-group">
											<label>Lokasi</label>
											<div id="location"></div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="box box-success">
									<div class="box-header with-border">
										<h3 class="box-title">Pemohon</h3>

										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>

									<div class="box-body">
										<div class="form-group">
											<label>Nama</label>
											<div id="user_name"></div>
										</div>

										<div class="form-group">
											<label>NIP</label>
											<div id="nip"></div>
										</div>

										<div class="form-group">
											<label>Posisi</label>
											<div id="position">IT</div>
										</div>

										<div class="form-group">
											<label>Tanggal Pengajuan</label>
											<div id="created_at"></div>
										</div>
									</div>
								</div>
							</div>

							<!-- <div class="col-sm-12">
								<div class="box box-default">
									<div class="box-header with-border">
										<h3 class="box-title">Log</h3>

										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>

									<div class="box-body">
										<table id="detail" class="table table-striped table-responsive dataTable no-footer" data-tables="true" width="100%">
											<thead>
												<tr>
													<th width="10" class="text-right">No</th>
													<th>Tanggal</th>
													<th>Masalah</th>
													<th>Penyelesaian</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div> -->
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="box box-info">
									<div class="box-header with-border">
										<h3 class="box-title">Keluhan</h3>

										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>

									<div class="box-body">
								     <p id="keluhan"></p>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="box box-info">
									<div class="box-header with-border">
										<h3 class="box-title">Penanganan</h3>

										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>

									<div class="box-body">
										<p id="penanganan"></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
	$("#modal_edit").on("show.bs.modal", function(e) {
		var Id = $(e.relatedTarget).data("id");
		var ticket = $(e.relatedTarget).data("ticket");
		var nup = $(e.relatedTarget).data("nup");
		var created_at = $(e.relatedTarget).data("created_at");
		var asset_name = $(e.relatedTarget).data("asset_name");
		var asset_item_id = $(e.relatedTarget).data("asset_item_id");
		var complaint = $(e.relatedTarget).data("complaint");
		var user_name = $(e.relatedTarget).data("user_name");
		var nip = $(e.relatedTarget).data("nip");
		var position = $(e.relatedTarget).data("position");
		var type = $(e.relatedTarget).data("type");
		var location = $(e.relatedTarget).data("location");
		var user_id = $(e.relatedTarget).data("user_id");
		var status = $(e.relatedTarget).data("status");
		var tolak = $(e.relatedTarget).data("tolak");
		var info = $(e.relatedTarget).data("info");

		$("#ticket").html("Nomor Tiket: "+ticket);
		$("#nup").html(nup);
		$("#asset_name").html(asset_name);
		$("#type").html(type);
		$("#complaint").html(complaint);
		$("#user_name").html(user_name);
		$("#nip").html(nip);
		$("#position").html(position);
		$("#created_at").html(created_at);
		$("#type").html(type);
		$("#location").html(location);
		$("#setuju").val(Id);
		$("#tolak").val(Id);
		$("#user_id").val(user_id);
		$(".id").val(Id);
		$("#keluhan").html(complaint);
		$("#penanganan").html(info);

		// var urlDetail = 'data-detil/'+user_id;
		// var detail = $("#detail").DataTable({
		// 	processing: true,
		// 	serverSide: true,
		// 	bFilter: false,
		// 	bInfo: false,
		// 	destroy: true,
		// 	searching: false,
		// 	paging: false,
		// 	ajax: urlDetail,
		// 	columns: [
		// 		{ data: "rownum", name: "rownum", class: "text-right" },
		// 		{ data: "created_at", name: "ticket" },
		// 		{ data: "complaint", name: "complaint" },
		// 		{ data: "info", name: "info" }
		// 	],
		// 	language: {
		// 		url: "{{ asset('lang/Indonesian.json') }}"
		// 	},
		// 	responsive: true
		// 	// order: [ [3, "desc"] ]
		// });
	});
	$("#modal_edit").on("hidden.bs.modal", function(e) {
		$("#ticket, #nup, #asset_name, #type, #complaint, #user_name, #nip, #position, #created_at, #type, #location, #keluhan, #penanganan").html("");
		$("#setuju, #tolak, #user_id, .id").val("");
	});

	var table = $("#table").DataTable({
		processing: true,
		serverSide: true,
		ajax: "{{ url('pemeliharaan/data-log') }}",
		columns: [
			{ data: "rownum", name: "rownum" },
			{ data: "ticket", name: "s.ticket" },
			{ data: "nup", name: "a.nup" },
			{ data: "asset_name", name: "a.name" },
			{ data: "user_name", name: "u.name" },
			{ data: "created_at", name: "s.created_at", class: "text-nowrap" },
			{ data: "info", name: "s.info" },
			{ data: "done_at", name: "s.done_at", class: "text-nowrap" },
			{ data: "cost", name: "s.cost", class: "text-nowrap" },
			{ data: "status", orderable: false, searchable: false, render: function (data) {
				if (data == "3") {
					return "<span class='label label-success'>Selesai</span>";
				} else {
					return "<span class='label label-danger'>Ditolak</span>";
				}
			}},
			{ data: "action", orderable: false, searchable: false}
		],
		// order: [ [3, 'desc'] ],
		language: {
			url: "{{ asset('lang/Indonesian.json') }}"
		},
		responsive: true,
		initComplete: function() {
			$("#table").wrap("<div class='table-responsive'></div>");
			$("select[name='table_length']").select2({
				minimumResultsForSearch: Infinity
			});
		}
	});
});
</script>
@endsection
