@extends('layout.default')

@section('content')
<section class="content-header">
	<h1>Manajemen Pemindahan</h1>
	<h2>Daftar Permintaan</h2>
	<!-- <ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Tables</a></li>
		<li class="active">Data tables</li>
	</ol> -->
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<table id="table" class="table table-striped dataTable no-footer" data-tables="true" width="100%">
						<thead>
							<tr>
								<th width="10">No</th>
								<th>Nama Asset</th>
								<th>Nama Pemohon</th>
								<th>Lokasi Asal</th>
								<th>Lokasi Tujuan</th>
								<th>Tgl. Permintaan</th>
								<th width="10">&nbsp;</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('modal')
<div class="modal fade stick-up" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrapper">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Detil Pemindahan</h4>
				</div>

				<div class="modal-body box-wrapper box-primary">
					<div class="box-wrapper-body">
						<div class="row">
							<div class="col-sm-6">
								<h4 id="lending_ticket"></h4>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="box box-primary">
									<div class="box-header with-border">
										<h3 class="box-title">Aset</h3>

										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>

									<div class="box-body">
										<div class="form-group">
											<label>NUP</label>
											<div id="nup"></div>
										</div>

										<div class="form-group">
											<label>Nama Asset</label>
											<div id="asset_name"></div>
										</div>

										<div class="form-group">
											<label>Kategori</label>
											<div id="type"></div>
										</div>

										<div class="form-group">
											<label>Lokasi</label>
											<div id="location"></div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="box box-success">
									<div class="box-header with-border">
										<h3 class="box-title">Pemohon</h3>

										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>

									<div class="box-body">
										<div class="form-group">
											<label>Nama</label>
											<div id="user_name"></div>
										</div>

										<div class="form-group">
											<label>NIP</label>
											<div id="nip"></div>
										</div>

										<div class="form-group">
											<label>Posisi</label>
											<div id="position">IT</div>
										</div>

										<div class="form-group">
											<label>Tanggal Pengajuan</label>
											<div id="created_at"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				@if (app('access')['update'] == '1')
					<div class="modal-footer">
						<button type="button" id="setuju" value="" class="btn btn-primary">Proses</button>
						<button type="button" id ="tolak" value="" class="btn btn-danger">Tolak</button><button type="button" id ="batalkan" class="btn btn-default pull-right" data-dismiss="modal">Batal</button>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$("#modal_edit").on("show.bs.modal", function(e) {
			var Id = $(e.relatedTarget).data("id");
			var lending_ticket = $(e.relatedTarget).data("ticket");
			var nup = $(e.relatedTarget).data("nup");
			var created_at = $(e.relatedTarget).data("created_at");
			var asset_name = $(e.relatedTarget).data("asset_name");
			var asset_item_id = $(e.relatedTarget).data("asset_item_id");
			var user_name = $(e.relatedTarget).data("user_name");
			var nip = $(e.relatedTarget).data("nip");
			var position = $(e.relatedTarget).data("position");
			var type = $(e.relatedTarget).data("type") + " (" + $(e.relatedTarget).data("type_code") + ")";
			var location = $(e.relatedTarget).data("room") + " (" + $(e.relatedTarget).data("room_code") + ")";
			var user_id = $(e.relatedTarget).data("user_id");
			var status = $(e.relatedTarget).data("status");
			var tolak = $(e.relatedTarget).data("tolak");

			$("#lending_ticket").html("Nomor Tiket: "+lending_ticket);
			$("#nup").html(nup);
			$("#asset_name").html(asset_name);
			$("#type").html(type);
			$("#location").html(location);
			$("#user_name").html(user_name);
			$("#nip").html(nip);
			$("#position").html(position);
			$("#created_at").html(created_at);
			$("#setuju").val(Id);
			$("#tolak").val(Id);
			$(".user_id").val(user_id);
			$(".id").val(Id);
		});
		$("#modal_edit").on("hidden.bs.modal", function(e) {
			$("#lending_ticket, #nup, #asset_name, #type, #location, #user_name, #nip, #position, #created_at").html("");
			$("#setuju, #tolak, .user_id, .id").val("");
		});

		var table = $("#table").DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ url('pemindahan/data-daftar') }}",
			columns: [
				{ data: "rownum", name: "rownum", class: "text-right" },
				{ data: "asset_name", name:"a.name" },
				{ data: "user_name", name:"u.name" },
				{ data: "init_position" },
				{ data: "new_position" },
				{ data: "created_at", name:"m.created_at" },
				{ data: "action", orderable: false, searchable: false }
			],
			language: {
				url: "{{ asset('lang/Indonesian.json') }}"
			},
			responsive: true,
			initComplete: function() {
				$("#table").wrap("<div class='table-responsive'></div>");
				$("select[name='table_length']").select2({
					minimumResultsForSearch: Infinity
				});
			}
			// order: [ [3, "desc"] ]
		});

		$("#setuju").click("button", function (e) {
			e.preventDefault();
			var id = $(this).val();
			var data = "id=" + id + "&status=1";

			$.ajax({
				headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
				url: "{{ url('pemindahan/update')}}",
				data: data,
				type: "get",
				dataType: "json",
				success: function (response) {
					if (response.status === true) {
						Lobibox.notify("success", {
							icon: "ion ion-android-checkmark-circle",
							title: "Berhasil",
							msg: "Data telah berhasil diproses.",
							sound: false
						});

						 $("#modal_edit").modal("hide");
						 table.ajax.reload();
					} else {
						Lobibox.notify("error", {
							icon: "ion ion-android-close",
							msg: "Pemrosesan data gagal!",
							sound: false
						});
					}
				}
			});
		});

		$("#tolak").click("button", function (e) {
			e.preventDefault();
			var id = $(this).val();
			var data = "id=" + id + "&status=2&return_ticket=tolak";

			$.ajax({
				headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
				url: "{{ url('pemindahan/update')}}",
				data: data,
				type: "get",
				dataType: "json",
				success: function (response) {
					if (response.status === true) {
						Lobibox.notify("success", {
							icon: "ion ion-android-checkmark-circle",
							title: "Berhasil",
							msg: "Data telah berhasil diproses.",
							sound: false
						});

						$("#modal_edit").modal("hide");
							table.ajax.reload();
					} else {
						Lobibox.notify("error", {
							icon: "ion ion-android-close",
							msg: "Pemrosesan data gagal!",
							sound: false
						});
					}
				}
			});
		});
	});
</script>
@endsection
