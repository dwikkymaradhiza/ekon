@extends('layout.default')
@section('styles')
<link href="{{ asset('css/webcame.css') }}" rel="stylesheet">
<style>
	#modal_edit .select2-container {
		display: block;
		width: auto !important;
	}
</style>
@endsection

@section('content')
<section class="content-header">
	<h1>Manajemen Pengembalian</h1>
	<h2>Daftar Permintaan</h2>
	<!-- <ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Tables</a></li>
		<li class="active">Data tables</li>
	</ol> -->
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<table id="table" class="table table-striped dataTable no-footer" data-tables="true" width="100%">
						<thead>
							<tr>
								<th width="10">No</th>
								<th>Tiket</th>
								<th>Aset</th>
								<th>Pemohon</th>
								<th>Dari</th>
								<th>Hingga</th>
								<th>Tgl. Pengajuan</th>
								<th width="10">&nbsp;</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('modal')
<div class="modal fade stick-up" id="modal_detail" tabindex="-1" role="dialog" aria-hidden="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrapper">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Detil Permintaan Pengembalian</h4>
				</div>

				<div class="modal-body box-wrapper box-primary">
					<div class="box-wrapper-body">
						<div class="row">
							<div class="col-sm-12">
								<h4 id="lending_ticket"></h4>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="box box-success">
									<div class="box-header with-border">
										<h3 class="box-title">Aset</h3>
									</div>

									<div class="box-body">
										<div class="form-group">
											<label>NUP</label>
											<div id="nup"></div>
										</div>

										<div class="form-group">
											<label>Nama Asset</label>
											<div id="asset_name"></div>
										</div>

										<div class="form-group">
											<label>Kategori</label>
											<div id="type"></div>
										</div>

										<div class="form-group">
											<label>Lokasi</label>
											<div id="location"></div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="box box-success">
									<div class="box-header with-border">
										<h3 class="box-title">Pemohon</h3>
									</div>

									<div class="box-body">
										<div class="form-group">
											<label>Nama</label>
											<div id="user_name"></div>
										</div>

										<div class="form-group">
											<label>NIP</label>
											<div id="nip"></div>
										</div>

										<div class="form-group">
											<label>Posisi</label>
											<div id="position">IT</div>
										</div>

										<div class="form-group">
											<label>Tanggal Pengajuan</label>
											<div id="created_at"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" id="return" value="" class="btn btn-primary id">Ok</button>
					<button type="button" id ="batalkan" class="btn btn-default pull-right" data-dismiss="modal">Batal</button>

					<!-- 0 = Waiting Approval, 1 = Approved, 2 = Rejected, 3 = Returned -->
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade stick-up" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content-wrapper">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Serah Terima Pengembalian</h4>
				</div>

				<div class="modal-body box-wrapper box-primary">
					<div class="box-wrapper-body">
						<div class="box box-success">
							<div class="box-body">
								<div class="row">
									<div class="col-sm-8">
										<select class="form-control select2" id="camera-select" name="camera_select" tabindex="-1"></select>
									</div>

									<div class="col-sm-4 camera-btn">
										<input id="image-url" type="hidden" class="form-control" placeholder="Image url">
										<button title="Decode Image" class="btn btn-default btn-sm hidden" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
										<button title="Image shoot" class="btn btn-info btn-sm disabled hidden" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>

										<button title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span></button>
										<button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span></button>
										<button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span></button>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div id="canvasWrapper">
									<canvas width="320" height="240" id="webcodecam-canvas"></canvas>
									<div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
									<div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
									<div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
									<div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
								</div>

								<p id="scanned-img"></p>
								<!-- <div class="caption text-center">
									<p id="scanned-QR" class="cekQr"></p>
								</div> -->

								<!-- <p class="text-center">atau</p> -->
								<input type="text" name="information" class="form-control scanned-QR" placeholder="Masukkan Nomor Tiket" id="tiket">
								<br>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" id="setuju" value="" class="btn btn-primary">Cek Tiket</button>
					<button type="button" id ="batalkan" class="btn btn-default pull-right" data-dismiss="modal">Batal</button>

					<!-- 0 = Waiting Approval, 1 = Approved, 2 = Rejected, 3 = Returned -->
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')

 <script type="text/javascript" src="{{ asset('js/filereader.js')}}"></script>
 <script type="text/javascript" src="{{ asset('js/qrcodelib.js')}}"></script>
 <script type="text/javascript" src="{{ asset('js/webcodecamjs.js')}}"></script>
 <script type="text/javascript" src="{{ asset('js/main.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var table = $("#table").DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ url('pengembalian/data-daftar') }}",
			columns: [
				{ data: "rownum", name: "rownum", class: "text-right" },
				{ data: "return_ticket", name: "l.return_ticket" },
				{ data: "asset_name", name: "a.name" },
				{ data: "user_name", name: "u.name" },
				{ data: "started_at", name: "l.started_at" },
				{ data: "ended_at", name: "l.ended_at" },
				{ data: "created_at", name: "l.created_at" },
				{ data: "action", orderable: false, searchable: false }
			],
			language: {
				url: "{{ asset('lang/Indonesian.json') }}"
			},
			responsive: true,
			initComplete: function() {
				$("#table").wrap("<div class='table-responsive'></div>");
				$("select[name='table_length']").select2({
					minimumResultsForSearch: Infinity
				});
			}
			// order: [ [3, "desc"] ]
		});

		$("#modal_edit").on("show.bs.modal", function(e) {
			var barcode = $(e.relatedTarget).data("barcode");
			$(".barcode").html(barcode);
		});
		$("#modal_edit").on("hidden.bs.modal", function(e) {
			$(".barcode").html("");
		});

		$("#setuju").on("click",function() {

			var id = $("#tiket").val();

			$.ajax({
				headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
				url: "{{ url('pengembalian/data-tiket') }}",
				method: "get",
				data : "tiket=" + id,
				dataType: "json",
				success:function(response) {
					if (response.status === true) {
						$("#modal_edit").modal("hide");
						$("#modal_detail").modal("show");
						$("#lending_ticket").html("Nomor Tiket: "+response.message.return_ticket);
						$("#nup").html(response.message.nup);
						$("#asset_name").html(response.message.asset_name);
						$("#type").html(response.type + " (" + response.type_code + ")");
						$("#location").html(response.room + " (" + response.room_code + ")");
						$("#user_name").html(response.message.user_name);
						$("#nip").html(response.message.nip);
						$("#position").html(response.message.position);
						$("#created_at").html(response.created);
						$(".id").val(response.message.id)
					} else {
						Lobibox.notify("error", {
							icon: "ion ion-android-close",
							msg: "Pemrosesan data gagal!",
							sound: "sound4"
						});
					}
				}
			})
		});

		$(".cekQr").on("click",function() {

			var value = document.getElementById('scanned-QR').innerText;

			var fields = value.split(':');

			var street = fields[1];

			var id = street;

			$.ajax({
				headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
				url: "{{ url('pengembalian/data-tiket') }}",
				method: "get",
				data : "tiket=" + id,
				dataType: "json",
				success:function(response) {
					if (response.status === true) {
						$("#modal_edit").modal("hide");
						$("#modal_detail").modal("show");
						$("#lending_ticket").html("Nomor Tiket: "+response.message.return_ticket);
						$("#nup").html(response.message.nup);
						$("#asset_name").html(response.message.asset_name);
						$("#type").html(response.type);
						$("#user_name").html(response.message.user_name);
						$("#nip").html(response.message.nip);
						$("#position").html(response.message.position);
						$("#created_at").html(response.created);
						$(".id").val(response.message.id)
					} else {
						Lobibox.notify("error", {
							icon: "ion ion-android-close",
							msg: "Pemrosesan data gagal!",
							sound: "sound4"
						});
					}
				}
			})
		});

		$("#return").click("button", function (e) {
			e.preventDefault();
			var id = $(this).val();

			var data = "id=" + id + "&status=3";

			$.ajax({
				headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"},
				url: "{{ url('pengembalian/update')}}",
				data: data,
				type: "post",
				dataType: "json",
				success: function (response) {
					if (response.status === true) {
						Lobibox.notify("success", {
							icon: "ion ion-android-checkmark-circle",
							title: "Berhasil",
							msg: "Data telah berhasil diproses.",
							sound: false
						});

						 $("#modal_detail").modal("hide");
						 table.ajax.reload();
					} else {
						Lobibox.notify("error", {
							icon: "ion ion-android-close",
							msg: "Pemrosesan data gagal!",
							sound: false
						});
					}
				}
			});
		});
	});
</script>
@endsection
