@extends('layout.default')

@section('styles-pre')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<section class="content-header">
	<h1>Daftar Aset Saya</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('/') }}"><i class="ion-speedometer"></i> Dasbor</a></li>
		<li class="active">Aset Saya</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="text-center active col-md-6 nopadding"><a href="#aset" aria-controls="aset" role="tab" data-toggle="tab"><h4>Peminjaman</h4></a></li>
					<li role="presentation" class="text-center col-md-6 nopadding"><a href="#aset-tetap" aria-controls="aset-tetap" role="tab" data-toggle="tab"><h4>Aset Tetap</h4></a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="aset">
						<div class="box-body">
							<table id="table" class="table table-striped dataTable no-footer" data-tables="true" width="100%">
								<thead>
									<tr>
										<th width="10">No</th>
										<th>NUP</th>
										<th>Nama Aset</th>
										<th>Jenis</th>
										<th>Tgl. Pinjam</th>
										<th>Tgl. Kembali</th>
										<th width="30">Status</th>
										<th width="10">&nbsp;</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane" id="aset-tetap">
						<div class="box-body">
							<table id="table-asset" class="table table-striped dataTable no-footer" data-tables="true" width="100%">
								<thead>
									<tr>
										<th width="10">No</th>
										<th width="20">NUP</th>
										<th width="30">Nama Aset</th>
										<th width="30">Jenis</th>
										<th width="10">&nbsp;</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<br>
@stop

@section('modal')
<!-- RETURN MODAL SECTION -->
<div id="return-modal" class="modal fade stick-up" role="dialog" aria-labelledby="modalReturn">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="modalReturn"><span></span> Permintaan Tiket Pengembalian</h4>
			</div>
			<div class="modal-content">
				<form role="form" action="{{ route('return.store') }}" method="POST">
					{{ csrf_field() }}
					<div class="modal-body box box-primary">
						<div class="box-body">
							Apakah Anda ingin membuat tiket pengembalian aset<br>untuk item <b><span class="modal-item"></span></b>?
						</div>
					</div>

					<input type="hidden" name="nuk" class="modal-item-value"  />
					<input type="hidden" name="lending_id" class="modal-item-lending"  />

					<div class="modal-footer">
						<input type="submit" name="return-submit" value="Proses" class="btn btn-primary" />
						<button type="button" data-dismiss="modal" class="btn btn-default pull-right">Batal</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- END OF THE RETURN MODAL SECTION -->

<!-- HELPDESK MODAL SECTION -->
<div id="helpdesk-modal" class="modal fade stick-up" role="dialog" aria-labelledby="modalHelp">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="modalHelp"><span></span> Permintaan Tiket Helpdesk</h4>
			</div>
			<div class="modal-content">
				<form role="form" action="{{ route('helpdesk.store') }}" method="POST">
					{{ csrf_field() }}

					<div class="modal-body box box-primary">
						<div class="box-body {{ $errors->first('message') != '' ? 'has-error' : '' }}">
							Tuliskan keluhan Anda untuk aset <b><span class="modal-item"></span></b> pada kotak di bawah ini.<br><br>
							<textarea class="form-control" id="keluhanBox" name="message" rows="3">{{ old('message') }}</textarea>
							@if ($errors->first('message') != '')
								<span class="help-block"><small><i>{{ $errors->first('message') }}</i></small></span>
							@endif
						</div>
					</div>

					<input type="hidden" name="nuk" class="modal-item-value" value="{{ old('nuk') }}" />
					<input type="hidden" name="lending_id" class="modal-item-lending" value="{{ old('lending_id') }}" />

					<div class="modal-footer">
						<input type="submit" name="return-submit" value="Proses" class="btn btn-primary" />
						<button type="button" data-dismiss="modal" class="btn btn-default pull-right" >Batal</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- END OF THE HELPDESK MODAL SECTION -->

<!-- MOVING MODAL SECTION -->
<div id="move-modal" class="modal fade stick-up" role="dialog" aria-labelledby="modalMove">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="modalMove"><span></span> Permintaan Tiket Pemindahan</h4>
			</div>
			<div class="modal-content">
				<form role="form" action="{{ route('moving.store') }}" method="POST">
					{{ csrf_field() }}

					<div class="modal-body box box-primary">
						<div class="box-body">
							Pilih lokasi baru untuk aset <b><span class="modal-item"></span></b>.<br><br>
							<select class="form-control" name="room_id" id="room_id" style="width: 100%;">
								<option value>&#8212; Pilih lokasi &#8212;</option>
								@foreach($rooms as $k => $room)
									<option value="{{ $room->id }}">{{ $room->code }} - {{ (!empty($room->name)) ? $room->name : $room->description }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<input type="hidden" name="nuk" class="modal-item-value" />

					<div class="modal-footer">
						<input type="submit" name="return-submit" value="Proses" class="btn btn-primary" />
						<button type="button" data-dismiss="modal" class="btn btn-default pull-right" >Batal</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- END OF THE MOVING MODAL SECTION -->

<!-- DETAIL LENDING MODAL SECTION -->
<div class="modal fade stick-up" id="taken-modal" tabindex="-1" role="dialog" aria-hidden="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrapper">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Detil Permintaan Peminjaman</h4>
				</div>

				<div class="modal-body box-wrapper box-primary">
					<div class="box-wrapper-body">
						<h4 id="lending_ticket"></h4>

						<div class="row">
							<div class="col-sm-6">
								<div class="box box-success">
									<div class="box-header with-border">
										<h3 class="box-title">Aset</h3>

										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>

									<div class="box-body">
										<div class="form-group">
											<label>Nama Aset</label>
											<div id="asset_name"></div>
										</div>

										<div class="form-group">
											<label>NUP</label>
											<div id="nup"></div>
										</div>

										<div class="form-group">
											<label>Jenis</label>
											<div id="type"></div>
										</div>

										<div class="form-group">
											<label>Lokasi</label>
											<div id="location"></div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="box box-success">
									<div class="box-header with-border">
										<h3 class="box-title">QR Code</h3>

										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="ion-minus"></i>
												<i class="ion-plus"></i>
											</button>
										</div>
									</div>

									<div class="box-body">
										<div id="qrcode" style="margin: -2px;"></div>
										<div id="lendqrcode" style="margin: -2px;"></div>
										<!-- <div class="form-group">
											<label>Nama</label>
											<div id="user_name"></div>
										</div>

										<div class="form-group">
											<label>NIP</label>
											<div id="nip"></div>
										</div>

										<div class="form-group">
											<label>Posisi</label>
											<div id="position"></div>
										</div>

										<div class="form-group">
											<label>Tanggal Pengajuan</label>
											<div id="created_at"></div>
										</div> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id ="tutup" class="btn btn-default" data-dismiss="modal">Tutup</button>
					<a href="#" target="_blank" class="btn-download btn btn-primary" style="margin-bottom: 5px;">Cetak</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- EOF DETAIL LENDING MODAL SECTION -->

<!-- SUCCESS MODAL SECTION -->
@if(Session::has('stored'))
	<div id="success-modal" class="modal fade stick-up" role="dialog" aria-labelledby="modalHelp">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="modalHelp">Proses Pengajuan Berhasil</h4>
				</div>
				<div class="modal-body box box-primary">
					<div class="box-body">
						{!! Session::get('stored') !!}
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn btn-default">Tutup</button>
				</div>
			</div>
		</div>
	</div>
@endif
<!-- END OF THE SUCCESS MODAL SECTION -->
@stop

@section('scripts')
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#room_id").select2({ dropdownParent: $("#move-modal") });
		$("#move-modal").on("show.bs.modal", function(e) {
			var current_room = $(e.relatedTarget).data("room");
			$("#room_id option[value='" + current_room + "']").prop("disabled", true);
		});
		$("#move-modal").on("hidden.bs.modal", function(e) {
			$("#room_id").val("").trigger("change");
			$("#room_id option").prop("disabled", false);
		});

		$("#taken-modal").on("show.bs.modal", function(e) {
			var ticket = $(e.relatedTarget).data("return_ticket") || $(e.relatedTarget).data("lending_ticket");
			var qrcode = $(e.relatedTarget).data("qrcode");
			var lendqrcode = $(e.relatedTarget).data("lendqrcode");
			var nup = $(e.relatedTarget).data("nup");
			var id = $(e.relatedTarget).data("id");
			var created_at = $(e.relatedTarget).data("created_at");
			var asset_name = $(e.relatedTarget).data("asset_name");
			var user_name = $(e.relatedTarget).data("user_name");
			var nip = $(e.relatedTarget).data("nip");
			var position = $(e.relatedTarget).data("position");
			var type = $(e.relatedTarget).data("type");
			var location = $(e.relatedTarget).data("room_name") || $(e.relatedTarget).data("room_desc");

			$("#lending_ticket").html("Nomor Tiket: " + ticket);
			$("#nup").html(nup);
			$("#asset_name").html(asset_name);
			$("#type").html(type);
			$("#user_name").html(user_name);
			$("#nip").html(nip);
			$("#position").html(position || "-");
			$("#created_at").html(created_at);
			$("#type").html(type);
			$("#location").html(location);
			if (qrcode) {
				$("#qrcode").html(qrcode);
				$("#qrcode").show();
				$("#lendqrcode").hide();
			} else {
				$("#lendqrcode").html(lendqrcode);
				$("#lendqrcode").show();
				$("#qrcode").hide();
			}

			$(".btn-download").attr("href", "{{ route('lending.print') }}" + "/" + id);
		});
		$("#taken-modal").on("hidden.bs.modal", function(e) {
			$("#lending_ticket, #nup, #asset_name, #type, #user_name, #nip, #position, #created_at, #type, #location, #qrcode, #lendqrcode").html("");
			$(".btn-download").attr("href", "");
		});

		var table = $("#table").DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ route('user.asset')}}",
			columns: [
				{ data: "rownum", name: "rownum", class: "text-right" },
				{ data: "nuk", name: "a.nup"},
				{ data: "asset_name", name: "a.name" },
				{ data: "category", name: "asset_type.name"},
				{ data: "started_at", name: "lending.started_at" },
				{ data: "ended_at", name: "lending.ended_at"},
				{ data: "status", name: "lending.status" },
				{ data: "action", orderable: false, searchable: false}
			],
			language: {
				url: "{{ asset('lang/Indonesian.json') }}"
			},
			responsive: true,
			initComplete: function() {
				$("#table").wrap("<div class='table-responsive'></div>");
				$("select[name='table_length']").select2({
					minimumResultsForSearch: Infinity
				});
				$("body").on("click", ".return-ticket, .helpdesk-ticket, .moving-ticket", function() {
					$("span.modal-item").html($(this).data("asset_name"));
					$(".modal-item-value").val($(this).attr("nuk"));
					$(".modal-item-lending").val($(this).attr("lending"));
				});
				$(".btn.dropdown-toggle").on("click", function(e) {
					// alert(e.pageX + ' , ' + e.pageY);
					$(".btn-group ul.dropdown-menu").width(180).css("position", "fixed").css("left", (e.pageX - 180)).css("top", e.pageY);
				});
			}
		});

		var table = $("#table-asset").DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ route('user.asset.fixed')}}",
			columns: [
				{ data: "rownum", name: "rownum", class: "text-right" },
				{ data: "nuk", name: "asset_item.nup"},
				{ data: "asset_name", name: "asset_item.name" },
				{ data: "category", name: "asset_type.name" },
				{ data: "action", orderable: false, searchable: false, class: "text-left" }
			],
			language: {
				url: "{{ asset('lang/Indonesian.json') }}"
			},
			responsive: true,
			initComplete: function() {
				$("#table-asset").wrap("<div class='table-responsive'></div>");
				$("select[name='table-asset_length']").select2({
					minimumResultsForSearch: Infinity
				});
				$(".btn.dropdown-toggle").on("click", function(e) {
					// alert(e.pageX + ' , ' + e.pageY);
					$(".btn-group ul.dropdown-menu").width(180).css("position", "fixed").css("left", (e.pageX - 180)).css("top", e.pageY);
				});
			}
		});

		@if (Session::has('stored'))
			$("#success-modal").modal('show');
		@endif

		@if (session('error') != '')
			$("#helpdesk-modal").modal("show");
		@endif
	});
</script>
@endsection
