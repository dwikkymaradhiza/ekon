@extends('layout.default')

@section('content')

<section class="content-header">
	<h1>Dasbor Teknisi</h1>
	<!-- <ol class="breadcrumb">
		<li><a href="#"><i class="ion-speedometer"></i> Home</a></li>
		<li><a href="#">Tables</a></li>
		<li class="active">Data tables</li>
	</ol> -->
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12 hidden">
			<div class="box">
				<div class="box-body">
					<div>
						<span class="text-green">Green</span>
						<span class="text-aqua">Aqua</span>
						<span class="text-light-blue">Light Blue</span>
						<span class="text-red">Red</span>
						<span class="text-yellow">Yellow</span>
						<span class="text-muted">Muted</span>
					</div>
					<hr>

					<div>
						<span class="alert alert-danger">Danger</span>
						<span class="alert alert-info">Info</span>
						<span class="alert alert-warning">Warning</span>
						<span class="alert alert-success">Success</span>
					</div>
					<hr>

					<div>
						<span class="callout callout-danger">Danger</span>
						<span class="callout callout-info">Info</span>
						<span class="callout callout-warning">Warning</span>
						<span class="callout callout-success">Success</span>
					</div>
					<hr>

					<div class="row">
						<div class="col-md-6">
							<form role="form">
								<div class="form-group">
									<label for="formtest1">Label</label>
									<input class="form-control" id="formtest1" placeholder="Placeholder" type="text">
								</div>
								<div class="form-group has-error">
									<label for="formtest2">Label</label>
									<input class="form-control" id="formtest2" placeholder="Placeholder" type="text">
									<span class="help-block">Help block with error</span>
								</div>
							</form>
						</div>

						<div class="col-md-6">
							<form class="form-horizontal">
								<div class="form-group">
									<label for="formtest3" class="col-sm-2 control-label">Label</label>
									<div class="col-sm-10">
										<input class="form-control" id="formtest3" placeholder="Label" type="text">
									</div>
								</div>
								<div class="form-group has-warning">
									<label for="formtest4" class="col-sm-2 control-label">Label</label>
									<div class="col-sm-10">
										<input class="form-control" id="formtest4" placeholder="Label" type="text">
										<span class="help-block">Help block with error</span>
									</div>
								</div>
							</form>
						</div>
					</div>
					<hr>

					<div>
						<button type="button" class="btn btn-default">Default</button>
						<button type="button" class="btn btn-primary">Primary</button>
						<button type="button" class="btn btn-success">Success</button>
						<button type="button" class="btn btn-info">Info</button>
						<button type="button" class="btn btn-danger">Danger</button>
						<button type="button" class="btn btn-warning">Warning</button>
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-4 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-green">
					<i class="ion-wrench"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Total Pemeliharaan</span>
					<span class="info-box-number">{{ number_format($service[0], 0, ',', '.') }}<small>Total {{ number_format($service[0], 0, ',', '.') }} (100%)</small></span>
				</div>
				<div class="info-percent bg-green">
					<span style="width: 100%;"></span>
				</div>
			</div>
		</div>

		<div class="col-sm-4 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-aqua">
					<i class="ion-wrench"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Pemeliharaan Tertangani</span>
					<span class="info-box-number">
                                            {{ number_format($service[1], 0, ',', '.') }}
                                            <small>Total {{ number_format($service[0], 0, ',', '.') }} ({{ $service[0] > 0 ? number_format(($service[1]/$service[0])*100, 2, ',', '.') : '0' }}%)</small>
                                        </span>
				</div>
				<div class="info-percent bg-aqua">
					<span style="width: {{ $service[0] > 0 ? ($service[1]/$service[0])*100 : '0' }}%;"></span>
				</div>
			</div>
		</div>

		<div class="col-sm-4 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-red">
					<i class="ion-wrench"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Pemeliharaan Belum Tertangani</span>
                                        <span class="info-box-number">
                                            {{ number_format($service[2], 0, ',', '.') }}
                                            <small>Total {{ number_format($service[2], 0, ',', '.') }} ({{ $service[0] > 0 ? number_format(($service[2]/$service[0])*100, 2, ',', '.') : '0' }}%)</small>
                                        </span>
				</div>
				<div class="info-percent bg-red">
					<span style="width: {{ $service[0] > 0 ? ($service[2]/$service[0])*100 : '0' }}%;"></span>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="row">
					<div class="col-md-7">
						<div class="box-header with-border">
							<h3 class="box-title">Laporan Pemeliharaan</h3>
						</div>

						<div class="box-body">
							<div class="chart"><canvas id="pemeliharaan-chartjs" style="height: 200px;"></canvas></div>
						</div>
					</div>

					<div class="col-md-5">
						<div class="box-header with-border">
							<h3 class="box-title">Sebaran 5 Terbanyak</h3>
						</div>

						<div class="box-body">
							<div class="chart"><canvas id="sebaran-chartjs" style="height: 200px;"></canvas></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script src="{{ asset('plugins/chartjs/Chart.min.js') }}"></script>
<script type="text/javascript">
	var d = new Date(),
		n = d.getMonth(),
		y = d.getFullYear();
	var month = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];
	var labelnyaMonth = [];
	for (var i = 0; i <= n; i++) {
		labelnyaMonth.push(month[i]+" {{ date('Y') }}");
	}

	Chart.defaults.global.defaultFontFamily				  = "'Raleway', sans-serif";
	Chart.defaults.global.defaultFontColor				  = "#607d8b";
	Chart.defaults.global.legend.position				  = "bottom";
	Chart.defaults.global.legend.lineWidth				  = 1;
	Chart.defaults.global.tooltips.backgroundColor		  = "rgba(0, 0, 0, .64)";
	Chart.defaults.global.tooltips.cornerRadius			  = 4;
	Chart.defaults.global.tooltips.xPadding				  = 15;
	Chart.defaults.global.tooltips.yPadding				  = 15;
	Chart.defaults.global.tooltips.titleSpacing			  = 0;
	Chart.defaults.global.tooltips.titleMarginBottom	  = 10;
	Chart.defaults.global.elements.line.tension			  = .3;
	Chart.defaults.global.elements.line.borderWidth		  = 1;
	Chart.defaults.global.elements.point.radius			  = 4;
	Chart.defaults.global.elements.point.hitRadius		  = 40;
	Chart.defaults.global.elements.point.hoverRadius	  = 6;
	Chart.defaults.global.elements.point.hoverBorderWidth = 2;
	Chart.defaults.global.barThickness					  = 5;
	Chart.defaults.global.maxBarThickness				  = 5;
	var options = {
		scales: {
			yAxes: [{
				ticks	  : { beginAtZero: true },
				gridLines : { color: "rgba(0, 0, 0, .05)" }
			}],
			xAxes: [{
				gridLines : { display: false, color: "rgba(0, 0, 0, .05)" }
			}]
		}
	};

	var helpChartCanvas	= $("#pemeliharaan-chartjs").get(0).getContext("2d");
	var helpChart		= new Chart(helpChartCanvas);
	var helpChartData	= {
		labels: labelnyaMonth,
		datasets: [
			{
				label				  : "Permintaan",
				backgroundColor		  : "rgba(79, 192, 232, .4)",
				borderColor			  : "#4fc0e8",
				pointBackgroundColor  : "#4fc0e8",
				pointBorderColor	  : "#ffffff",
				pointHoverBorderColor : "#ffffff",
				data				  : [{{ implode(',', $rep_service['request']) }}]
			},
			{
				label				  : "Tertangani",
				backgroundColor		  : "rgba(75, 137, 220, .8)",
				borderColor			  : "#498ade",
				pointBackgroundColor  : "#498ade",
				pointBorderColor	  : "#ffffff",
				pointHoverBorderColor : "#ffffff",
				data				  : [{{ implode(',', $rep_service['handled']) }}]
			}
		]
	};
	var myLineChart = new Chart(helpChart, {
		type: "line",
		data: helpChartData,
		options: options
	});

	var labelnya = [],
		requestnya = [],
		handlednya = [];
	@foreach ($most_service['request'] as $k => $v)
		labelnya.push("{{ $v['short_name'] != '' ? $v['short_name'] : $v['name'] }}");
		requestnya.push({{ $v['total'] }});
	@endforeach
	@foreach ($most_service['handled'] as $k => $v)
		handlednya.push({{ $v['total'] }});
	@endforeach
	var spreadChartCanvas	= $("#sebaran-chartjs").get(0).getContext("2d");
	var spreadChart			= new Chart(spreadChartCanvas);
	var spreadChartData		= {
		labels  : labelnya,
		datasets: [
			{
				label				 : "Permintaan",
				backgroundColor		 : "rgba(79, 192, 232, .8)",
				hoverBackgroundColor : "#3baeda",
				borderColor			 : "#3baeda",
				pointBackgroundColor : "#3baeda",
				pointBorderColor	 : "#ffffff",
				data				 : requestnya
			},
			{
				label				 : "Tertangani",
				backgroundColor		 : "rgba(75, 137, 220, .8)",
				hoverBackgroundColor : "#498ade",
				borderColor			 : "#498ade",
				pointBackgroundColor : "#498ade",
				pointBorderColor	 : "#ffffff",
				data				 : handlednya
			}
		]
	};
	var myBarChart = new Chart(spreadChart, {
		type: "bar",
		data: spreadChartData,
		options: options
	});

	Chart.defaults.global.legend.display = false;

	var costHelpChartCanvas	= $("#cost-pemeliharaan-chartjs").get(0).getContext("2d");

	var gradient = costHelpChartCanvas.createLinearGradient(0, 0, 0, 400);
	gradient.addColorStop(0, "rgba(160, 212, 104, .8)");
	gradient.addColorStop(1, "rgba(79, 192, 232, 0)");

	var costHelpChart		= new Chart(costHelpChartCanvas);
	var costHelpChartData	= {
		labels  : labelnyaMonth,
		datasets: [
			{
				label				  : "Biaya",
				backgroundColor		  : gradient,
				borderColor			  : "#8cc152",
				pointBackgroundColor  : "#8cc152",
				pointBorderColor	  : "#ffffff",
				pointHoverBorderColor : "#ffffff",
				data				  : [{{ implode(',', $rep_service['cost']) }}]
			}
		]
	};
	var myCostLineChart = new Chart(costHelpChart, {
		type: "line",
		data: costHelpChartData,
		options: options
	});

	var costSpreadChartCanvas	= $("#cost-sebaran-chartjs").get(0).getContext("2d");

	var gradient = costSpreadChartCanvas.createLinearGradient(0, 0, 0, 400);
	gradient.addColorStop(0, "rgba(160, 212, 104, .8)");
	gradient.addColorStop(1, "rgba(79, 192, 232, 0)");
</script>
@stop
