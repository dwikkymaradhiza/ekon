@extends('layout.full')

@section('styles')
<style type="text/css">
	#logoGaruda {
		display: block;
		margin: 0 auto;
	}
	.login-box {
		left: 50%;
		margin: -244px 0 0 -210px;
		position: absolute;
		top: 50%;
		width: 420px;
	}
	.login-bg {
		background: url("../img/maintenance.jpg") no-repeat fixed center center / cover;
		-webkit-filter: blur(10px);
		-moz-filter: blur(10px);
		-ms-filter: blur(10px);
		-o-filter: blur(10px);
		filter: blur(10px);
		height: 100%;
		left: 0;
		position: absolute;
		top: 0;
		width: 100%;
		z-index: 1;
	}
	.login-box-body {
		background-color: rgba(255, 255, 255, 0.24);
		border-bottom: 1px solid rgba(255, 255, 255, 0.24);
		border-radius: 4px;
		border-top: 1px solid rgba(255, 255, 255, 0.24);
		color: #263238;
		position: relative;
		z-index: 2;
	}
	.login-box-body .icheckbox_square-blue {
		background-color: #ffffff;
		margin-left: -20px;
		margin-top: -3px;
	}
	.login-box-body .btn {
		display: block;
		margin: 0 auto;
		padding: 12px 45px;
	}
	.login-box-body hr {
		border-bottom: 1px solid rgba(255, 255, 255, 0.4);
		border-top: 1px solid rgba(96, 125, 139, 0.2);
		margin: 15px 0 10px 0;
	}
	#lupaPass1 {
		margin: 7px 0px;
		text-align: right;
	}
	#lupaPass2 {
		display: none;
		margin-top: 30px;
	}

	@media (max-width: 479px) {
		.login-box {
			left: 0;
			margin: 0;
			padding: 15px;
			top: 0;
			width: 100%;
		}
		.text-title {
			font-size: 24px;
		}
		form {
			text-align: center;
		}
		.form-control {
			text-align: center;
		}
		#lupaPass1 {
			display: none;
		}
		#lupaPass2 {
			display: block;
		}
	}
</style>
@stop

@section('content')
<div class="login-box">
	<div class="login-bg"></div>
	<div class="login-box-body">
		<img id="logoGaruda" src="{{ asset('img/garuda.png') }}" height="96" width="88">
		<h1 class="text-title text-center">Aplikasi Layanan</h1>

		<form autocomplete="off" class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
			{{ csrf_field() }}
			@if (count($errors) > 0)
				<div class="alert alert-danger" style="margin: 15px -5px;">
					@foreach ($errors->all() as $error)
						<p>{{ $error }}</p>
					@endforeach
				</div>
			@endif
			<div class="form-group{{ ($errors->has('email') OR $errors->has('nip')) ? ' has-error' : '' }}">
				<div class="col-md-12">
					<input type="text" class="typeahead form-control input-lg" name="email" value="{{ old('email') }}" placeholder="Nama atau Alamat Surel" autocomplete="off" required autofocus>
					<input type="hidden" name="nip" id="nip" value="{{ old('nip') }}">

					@if ($errors->has('email'))
						<span class="help-block">
							<strong>{{ $errors->first('email').' '.$errors->first('nip') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
				<div class="col-md-12">
					<input id="password" type="password" class="form-control input-lg" name="password" placeholder="Kata Sandi" autocomplete="off" required>

					@if ($errors->has('password'))
						<span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-4">
					<div class="checkbox">
						<label>
							<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> &nbsp;Ingat Saya&#8230;
						</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-12">
					<button type="submit" class="btn btn-primary btn-lg">
						Login
					</button>
				</div>
			</div>

			<div id="lupaPass2">Lupa kata sandi? <a href="{{ route('password.request') }}">Klik di sini</a></div>
			<hr>

			<p class="text-center"><small>Aplikasi ini menggunakan autentikasi yang sama pada aplikasi SIMPEG. Pastikan data yang Anda masukkan sudah benar.</small></p>
		</form>
	</div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script>
	$(document).ready(function() {
		var path = "{{ route('autocomplete') }}";
		$("input.typeahead").typeahead({
			source: (query, process) => {
				return $.get(path, { query: query }, function (data) {
					return process(data);
				});
			},
			display: "id",
			limit: 5
		});

		$("input.typeahead").change(function() {
			var current = $("input.typeahead").typeahead("getActive");
			if (current) {
				$("input.typeahead").val(current.name);
				if (current.nip == "") {
					// var nameX = current.name.split(" (");
					// nameX = nameX[1].split(")");
					// $("input#nip").val(nameX[0]);
					$("input#nip").val(current.name);
				} else {
					$("input#nip").val(current.nip);
				}
			} else {
				$("input#nip").val($("input[name='email']").val());
			}
		});
	});
</script>
@endsection
