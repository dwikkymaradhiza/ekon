<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta name="apple-mobile-web-app-capable" content="yes" />

		<title>Aplikasi Layanan</title>

		@section('meta_keywords')
		<meta name="keywords" content="Ekon, Layanan" />
		@show
		@section('meta_author')
		<meta name="author" content="Ekon">
		<meta name="generator" content="Ekon">
		@show
		@section('meta_description')
		<meta name="description" content="Aplikasi Layanan" />
		@show

		<!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" /> -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
		<link href="https://fonts.googleapis.com/css?family=Montserrat:500|Raleway:500,500i,700,700i" rel="stylesheet" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" />
		<!-- Font Awesome Icons -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- <link href="{{ asset('bower_components/AdminLTE/dist/css/AdminLTE.min.css') }}" rel="stylesheet" /> -->
		<!-- <link href="{{ asset('bower_components/AdminLTE/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet"> -->
		@yield('styles-pre')
		<link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('css/skins/skin-blue.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('plugins/lobiBox/css/lobibox.min.css')}}" rel="stylesheet" />
		<link href="{{ asset('plugins/iCheck/square/blue.css') }}" rel="stylesheet" />
		<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet" />
		<!-- <link href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" /> -->
		<link href="//cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet" />
		<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
		<style>
			html {
				background: #ffffff;
				background: -moz-linear-gradient(top, #ffffff 0%, #eceff1 100%);
				background: -webkit-linear-gradient(top, #ffffff 0%,#eceff1 100%);
				background: linear-gradient(to bottom, #ffffff 0%,#eceff1 100%);
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#eceff1',GradientType=0 );
			}
			body {
				background-color: transparent;
				min-height: 100vh !important;
			}
			body > .content {
				margin-top: -4.7em;
				padding-top: 4.7em;
				min-height: 99vh;
			}
			.main-header {
				background-color: transparent;
				max-height: none;
				padding: 1.5em 0 0 0;
			}
			.main-header .logo-display {
				font-size: 2em;
			}
			.logo-lg img {
				height: 1.8em;
				margin: -0.2em .5em 0 0;
				width: 1.65em;
			}
			.content-header > h2 {
				font-size: 1.33333em;
				margin: 0;
			}
			table.dataTable {
				font-size: 1.08333em;
			}
			.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td,
			.table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
				padding: .5em;
			}
			.dataTables_wrapper .label {
				margin: -0.18em 0;
				padding: .36em 1em .5em 1em;
			}
		</style>
		@yield('styles')
		<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
	</head>
		<body>
		<header class="main-header">
			<div class="logo-display">
				<center>
					<span class="logo-lg"><img src="{{ asset('img/garuda.png') }}" height="96" width="88">Aplikasi Layanan</span>
				</center>
			</div>
		</header>
		<div class="content">
			@yield('content')
		</div>
		<script src="//code.jquery.com/jquery-3.1.1.min.js"></script>
		<script src="{{ asset('plugins/lobiBox/js/lobibox.min.js') }}"></script>
		<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
		<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
		<!-- <script src="{{ asset('js/app.min.js') }}"></script> -->
		<!--<script src="{{ asset('bower_components/AdminLTE/dist/js/app.min.js') }}"></script>-->
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="{{ asset('js/adminlte.js') }}"></script>
		<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
		<script src="//cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
		<script src="{{ asset('jquery-validation/js/jquery.validate.min.js')}}"></script>
		<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
		<script>
			$(document).ready(function() {
				var font = window.screen.width / 80;
				$("body").css("font-size", font + "px");
			});
		</script>
		@yield('scripts')
	</body>
</html>
