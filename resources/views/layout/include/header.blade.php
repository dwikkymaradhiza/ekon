<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta name="apple-mobile-web-app-capable" content="yes" />

		<title>Aplikasi Layanan</title>

		@section('meta_keywords')
		<meta name="keywords" content="Ekon, Layanan" />
		@show
		@section('meta_author')
		<meta name="author" content="Ekon">
		<meta name="generator" content="Ekon">
		@show
		@section('meta_description')
		<meta name="description" content="Aplikasi Layanan" />
		@show

		<!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" /> -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
		<link href="https://fonts.googleapis.com/css?family=Montserrat:500|Raleway:500,500i,700,700i" rel="stylesheet" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" />
		<!-- Font Awesome Icons -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- <link href="{{ asset('bower_components/AdminLTE/dist/css/AdminLTE.min.css') }}" rel="stylesheet" /> -->
		<!-- <link href="{{ asset('bower_components/AdminLTE/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet"> -->
		@yield('styles-pre')
		<link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('css/skins/skin-blue.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('plugins/lobiBox/css/lobibox.min.css')}}" rel="stylesheet" />
		<link href="{{ asset('plugins/iCheck/square/blue.css') }}" rel="stylesheet" />
		<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet" />
		<!-- <link href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" /> -->
		<link href="//cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet" />
		<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
		@yield('styles')

		<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
	</head>

	<!-- HEADER INC ENDS HERE -->
	@if (app('access')['roles_id'] == 4)
		@include('layout.include.header-user')
	@else
		@include('layout.include.header-admin')
	@endif

	<div class="content-wrapper">
