<body id="user-page" class="skin-blue layout-top-nav">
	<div class="wrapper">
		<header class="main-header">
			<nav class="navbar navbar-static-top">
				<a href="#" class="sidebar-toggle hidden-md hidden-lg" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>

				<div class="navbar-header">
					<a href="{{ url('/') }}" class="navbar-brand logo">
						<span class="logo-lg"><img src="{{ asset('img/garuda.png') }}" height="30" width="28"> &nbsp; Aplikasi Layanan</span>
					</a>
				</div>

				<ul class="nav navbar-nav hidden-sm hidden-xs">
					@foreach (app('menu_public') as $k => $v)
						@if (Auth::user()->simpeg_id > 0)
							<li class="{{ url()->current() == url($v['uri']) ? 'active' : '' }}">
								<a href="{{ url($v['uri']) }}">
									<i class="{{ $v['css_class'] == '' ? 'fa fa-circle-o' : $v['css_class'] }}"></i> <span>{{ $v['name'] }}</span>
								</a>
							</li>
						@else
							@if ($v['uri'] == 'peminjaman/form-pengajuan-rapat' AND Auth::user()->roles_id == 6)
								<li class="{{ url()->current() == url($v['uri']) ? 'active' : '' }}">
									<a href="{{ url($v['uri']) }}">
										<i class="{{ $v['css_class'] == '' ? 'fa fa-circle-o' : $v['css_class'] }}"></i> <span>{{ $v['name'] }}</span>
									</a>
								</li>
							@endif
						@endif
					@endforeach
				</ul>

				<div class="navbar-custom-menu hidden-sm hidden-xs">
					<!-- <form id="search" role="form" method="POST" action="#">
						<div class="input-group">
							<span class="input-group-addon"><i class="ion ion-android-search"></i></span>
							<input class="form-control" type="text" name="search" placeholder="Cari aset&#8230;">
						</div>
					</form> -->
					<ul class="nav navbar-nav">
						<li><a href="#" style="max-width: 240px; overflow: hidden; white-space: nowrap;">Selamat datang, {{ str_limit(Auth::user()->name, 32, '&#8230;') }}</a></li>
						<li>
							<form method="POST" action="{{ route('logout') }}">
								{{ csrf_field() }}
								<button id="logout"><i class="ion ion-log-out"></i></button>
							</form>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<aside class="main-sidebar">
			<section class="sidebar">
				<ul class="sidebar-menu" data-widget="tree">
					<!-- <li>
						<form id="search" role="form" method="POST" action="#">
							<div class="input-group">
								<span class="input-group-addon"><i class="ion ion-android-search"></i></span>
								<input class="form-control" type="text" name="search" placeholder="Cari aset&#8230;">
							</div>
						</form>
					</li> -->
					<li class="header-user"><a href="#">Selamat datang, {{ str_limit(Auth::user()->name, 32, '&#8230;') }}</a></li>
					<li class="header">Menu Pengguna</li>
					@foreach (app('menu_public') as $k => $v)
						@if (Auth::user()->simpeg_id > 0)
							<li class="{{ url()->current() == url($v['uri']) ? 'active' : '' }}">
								<a href="{{ url($v['uri']) }}">
									<i class="{{ $v['css_class'] == '' ? 'fa fa-circle-o' : $v['css_class'] }}"></i> <span>{{ $v['name'] }}</span>
								</a>
							</li>
						@else
							@if ($v['uri'] == 'peminjaman/form-pengajuan-rapat' AND Auth::user()->roles_id == 6)
								<li class="{{ url()->current() == url($v['uri']) ? 'active' : '' }}">
									<a href="{{ url($v['uri']) }}">
										<i class="{{ $v['css_class'] == '' ? 'fa fa-circle-o' : $v['css_class'] }}"></i> <span>{{ $v['name'] }}</span>
									</a>
								</li>
							@endif
						@endif
					@endforeach

					<li class="header">Logout</li>
					<li class="hidden-md hidden-lg">
						<form method="POST" action="{{ route('logout') }}">
							{{ csrf_field() }}
							<button id="logout"><i class="ion ion-log-out"></i> <span>Logout</span></button>
						</form>
					</li>
				</ul>
			</section>
		</aside>
