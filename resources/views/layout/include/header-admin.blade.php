<body class="skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<header class="main-header">
			<a href="{{ url('/') }}" class="logo">
				<span class="logo-mini"><img src="{{ asset('img/garuda.png') }}" height="30" width="28"></span>
				<span class="logo-lg"><img src="{{ asset('img/garuda.png') }}" height="30" width="28"> &nbsp; Aplikasi Layanan</span>
			</a>
			<nav class="navbar navbar-static-top">
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>

				<ul class="nav navbar-nav hidden-sm hidden-xs">
					@foreach (app('menu_public') as $k => $v)
						@if (Auth::user()->simpeg_id > 0)
							<li class="{{ url()->current() == url($v['uri']) ? 'active' : '' }}">
								<a href="{{ url($v['uri']) }}">
									<i class="{{ $v['css_class'] == '' ? 'fa fa-circle-o' : $v['css_class'] }}"></i> <span>{{ $v['name'] }}</span>
								</a>
							</li>
						@else
							@if ($v['uri'] == 'peminjaman/form-pengajuan-rapat' AND Auth::user()->roles_id == 6)
								<li class="{{ url()->current() == url($v['uri']) ? 'active' : '' }}">
									<a href="{{ url($v['uri']) }}">
										<i class="{{ $v['css_class'] == '' ? 'fa fa-circle-o' : $v['css_class'] }}"></i> <span>{{ $v['name'] }}</span>
									</a>
								</li>
							@endif
						@endif
					@endforeach
				</ul>

				<div class="navbar-custom-menu hidden-sm hidden-xs">
					<!-- <form id="search" role="form" method="POST" action="#">
						<div class="input-group">
							<span class="input-group-addon"><i class="ion ion-android-search"></i></span>
							<input class="form-control" type="text" name="search" placeholder="Cari aset&#8230;">
						</div>
					</form> -->
					<ul class="nav navbar-nav">
						<li><a href="#">Selamat datang, {{ str_limit(Auth::user()->name, 32, '&#8230;') }}</a></li>
						<li>
							<form method="POST" action="{{ route('logout') }}">
								{{ csrf_field() }}
								<button id="logout"><i class="ion ion-log-out"></i></button>
							</form>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<aside class="main-sidebar">
			<section class="sidebar">
				<ul class="sidebar-menu" data-widget="tree">
					<!-- <li class="hidden-md hidden-lg">
						<form id="search" role="form" method="POST" action="#">
							<div class="input-group">
								<span class="input-group-addon"><i class="ion ion-android-search"></i></span>
								<input class="form-control" type="text" name="search" placeholder="Cari aset&#8230;">
							</div>
						</form>
					</li> -->
					<li class="header-user hidden-md hidden-lg"><a href="#">Selamat datang, {{ str_limit(Auth::user()->name, 32, '&#8230;') }}</a></li>
					<li class="header hidden-md hidden-lg">Menu Pengguna</li>
					@foreach (app('menu_public') as $k => $v)
						@if (Auth::user()->simpeg_id > 0)
							<li class="hidden-md hidden-lg {{ url()->current() == url($v['uri']) ? 'active' : '' }}">
								<a href="{{ url($v['uri']) }}">
									<i class="{{ $v['css_class'] == '' ? 'fa fa-circle-o' : $v['css_class'] }}"></i> <span>{{ $v['name'] }}</span>
								</a>
							</li>
						@else
							@if ($v['uri'] == 'peminjaman/form-pengajuan-rapat' AND Auth::user()->roles_id == 6)
								<li class="hidden-md hidden-lg {{ url()->current() == url($v['uri']) ? 'active' : '' }}">
									<a href="{{ url($v['uri']) }}">
										<i class="{{ $v['css_class'] == '' ? 'fa fa-circle-o' : $v['css_class'] }}"></i> <span>{{ $v['name'] }}</span>
									</a>
								</li>
							@endif
						@endif
					@endforeach

					<li class="header">Menu {{ app('access')['role'] }}</li>
					@foreach (app('menu') as $k => $v)
						@php
							$c = array();
						@endphp
						@if (count($v['children']) > 0)
							@foreach ($v['children'] as $kk => $vv)
								@php
									array_push($c, url($vv['uri']));
								@endphp
							@endforeach
						@endif
						@if (!(count($v['children']) == 0 AND $v['uri'] == '#'))
							<li class="{{ (count($v['children']) > 0) ? 'treeview' : '' }} {{ (url()->current() == url($v['uri']) OR in_array(url()->current(), $c)) ? 'active' : '' }}">
								<a href="{{ url($v['uri']) }}">
									<i class="{{ $v['css_class'] == '' ? 'fa fa-circle-o' : $v['css_class'] }}"></i> <span>{{ $v['name'] }}</span>
									@if (count($v['children']) > 0)
										<span class="pull-right-container">
											<i class="ion ion-ios-arrow-down pull-right"></i>
											<i class="ion ion-ios-arrow-left pull-right"></i>
										</span>
									@endif
								</a>
								@if (count($v['children']) > 0)
									<ul class="treeview-menu">
										@foreach ($v['children'] as $kk => $vv)
											<li {{ url()->current() == url($vv['uri']) ? 'class=active' : '' }}>
												<a href="{{ url($vv['uri']) }}">
													<i class="{{ $vv['css_class'] == '' ? 'circle-o' : $vv['css_class'] }}"></i> <span>{{ $vv['name'] }}</span>
												</a>
											</li>
										@endforeach
									</ul>
								@endif
							</li>
						@endif
					@endforeach

					<li class="header hidden-md hidden-lg">Logout</li>
					<li class="hidden-md hidden-lg">
						<form method="POST" action="{{ route('logout') }}">
							{{ csrf_field() }}
							<button id="logout"><i class="ion ion-log-out"></i> <span>Logout</span></button>
						</form>
					</li>
				</ul>
			</section>
		</aside>
