		</div>
		<footer class="main-footer">
			<div class="pull-right hidden-xs">Versi 1.0.0</div>
			@php
				$thn = Carbon\Carbon::now()->format('Y');
			@endphp
			Hak Cipta &copy; {{ ($thn > 2017 ? '2017-' : '').$thn }} &#8226; <br class="hidden-md hidden-lg"> Kementerian Koordinator Bidang Perekonomian Republik Indonesia.
		</footer>
	</div>

	@yield('modal')

	<script src="//code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src="{{ asset('plugins/lobiBox/js/lobibox.min.js') }}"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
	<!-- <script src="{{ asset('js/app.min.js') }}"></script> -->
	<!--<script src="{{ asset('bower_components/AdminLTE/dist/js/app.min.js') }}"></script>-->
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="{{ asset('js/adminlte.js') }}"></script>
	<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
	<script src="//cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
	<script src="{{ asset('jquery-validation/js/jquery.validate.min.js')}}"></script>
	<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
	<script type="text/javascript">
		function getWidth() {
			var winW = $(window).width();
			if (winW >= 768) {
				$("body.sidebar-mini").addClass("sidebar-collapse");
			} else {
				$("body.sidebar-mini").removeClass("sidebar-collapse");
			}
		}
		$(document).ready(function() {
			getWidth();
		});
		$(window).on("resize", function() {
			getWidth();
		});
	</script>
	@yield('scripts')
	</body>
</html>
