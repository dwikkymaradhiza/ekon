<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta name="apple-mobile-web-app-capable" content="yes" />

		<title>Aplikasi Layanan</title>

		@section('meta_keywords')
		<meta name="keywords" content="Ekon, Layanan" />
		@show
		@section('meta_author')
		<meta name="author" content="Ekon">
		<meta name="generator" content="Ekon">
		@show
		@section('meta_description')
		<meta name="description" content="Aplikasi Layanan" />
		@show

		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
		<link href="https://fonts.googleapis.com/css?family=Montserrat:500|Raleway:500,500i,700,700i" rel="stylesheet" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" />
		<link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('css/skins/skin-blue.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('plugins/iCheck/square/blue.css') }}" rel="stylesheet" />
		<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
		@yield('styles')

		<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
	</head>
	<body id="fullPage" class="fixed">
		@yield('content')

		<script src="//code.jquery.com/jquery-3.1.1.min.js"></script>
		<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
		<script>
			$(function () {
			$("input").iCheck({
					checkboxClass: "icheckbox_square-blue",
					radioClass: "iradio_square-blue",
					increaseArea: "20%" // optional
				});
			});
		</script>
		@yield('scripts')
	</body>
</html>