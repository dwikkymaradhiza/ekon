<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<title>Berita Acara</title>
		<style>
			.pdf-berita-acara a,.pdf-berita-acara p, .pdf-berita-acara h1, .pdf-berita-acara{
				font-family: 'Elegance';
			}
			.pdf-berita-acara a{
				font-style: italic;
			}
			.pdf-berita-acara h1{
				line-height: 12px;
				font-size: 16px;
			}
			.pdf-berita-acara header{
				text-align: center;
				padding-bottom: 20px;
			}
			.pdf-berita-acara header .header-line{
				border-top: 2px solid #000;
				border-bottom: 1px solid #000;
				height: 2px;
			}
			.pdf-berita-acara .content{
				text-align: justify;
			}
			.ttd{
				float:right;
				text-align: center;
			}
		</style>
	</head>
	<body class="pdf-berita-acara">
		<header>
			<h1>KEMENTRIAN KOORDINATOR BIDANG PEREKONOMIAN REPUBLIK INDONESIA</h1>
			<h1>SEKRETARIAT</h1>
			<h1>BIRO UMUM</h1>
			<p>
				Jalan Lapangan Banteng Timur No.2-4 Jakarta Pusat 10710<br/>
				Telp. (021) 3521974 - Fax. (021) 3521986<br/>
				Website : <a href="http://www.ekon.go.id">www.ekon.go.id</a>
			</p>
			<div class="header-line"></div>
		</header>
		<div class="content">
			<p>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Pada hari ini, <strong>{{ $day }}</strong> tanggal <strong>{{ $date }}</strong> bulan <strong>{{ $month }}</strong>  tahun <strong>{{ $year }}</strong>,
				telah dilaksanakan serah terima 1(satu) unit aset milik KEMENTRIAN KOORDINATOR BIDANG PEREKONOMIAN REPUBLIK INDONESIA, kepada :
			</p>
			<p class="detail">
				<table border="0" cellpadding="3" cellspacing="0">
					<tr>
						<td width="150">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama</td>
						<td>:</td>
						<td>{{ $user_name }}</td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP</td>
						<td>:</td>
						<td>{{ $nip }}</td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jabatan</td>
						<td>:</td>
						<td>{{ $position }}</td>
					</tr>
				</table>
			</p>
			<p>
				Dengan spesifikasi aset sebagai berikut :
			</p>
			<p class="detail">
				<table border="0" cellpadding="3" cellspacing="0">
					<tr>
						<td width="150">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Aset</td>
						<td>:</td>
						<td>{{ $name }}</td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NUP</td>
						<td>:</td>
						<td>{{ $nup }}</td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kategori</td>
						<td>:</td>
						<td>{{ $category }}</td>
					</tr>
				</table>
			</p>
			<p>
				Dengan ketentuan :
			</p>
			<p>
				<ol>
					<li>Aset tersebut digunakan untuk kegiatan operasional Sekretaris Kementerian pada Kementerian Koordinator Bidang Perekonomian.</li>
					<li>Apabila aset tersebut hilang, yang bersangkutan wajib bertanggungjawab dan mengganti sesuai dengan ketentuan pada kontrak yang berlaku.</li>
					<li>Apabila yang bersangkutan tidak lagi menduduki jabatan {{ $position }} Kementerian pada Kementerian Koordinator Bidang Perekonomian, aset tersebut wajib dikembalikan ke Sekretariat Kementerian Koordinator Bidang Perekonomian.</li>
				</ol>
			</p>
			<p>
				Demikian Berita Acara Serah Terima ini dibuat agar dapat dipergunakan sebagaimana mestinya.
			</p>
			<br /><br />
		</div>
		<div class="ttd">
			Jakarta, {{ date('d') }} {{ $month }} {{ date('Y') }}<br />
			Yang menerima,
			<br />
			<br />
			<br />
			<br />
			<br />
			{{ $user_name }}<br/>
			NIP. {{ $nip }}
		</div>
	</body>
</html>
