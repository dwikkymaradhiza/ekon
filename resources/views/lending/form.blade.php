@extends('layout.default')

@section('content')
<section class="content-header">
	<h1>Manajemen Peminjaman</h1>
	<h2>Detil Permintaan</h2>
	<!-- <ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Tables</a></li>
		<li class="active">Data tables</li>
	</ol> -->
</section>

@php
	$data = [];
@endphp

<section class="content">
	<div class="row">
		@if (count($data) > 0)
			<div class="col-sm-12">
				<h4>Form Persetujuan</h4>
			</div>

			<div class="col-sm-3">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Aset</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="ion-minus"></i>
							</button>
						</div>
					</div>

					<div class="box-body">
						<div class="form-group">
							<label>NUP</label>
							<div>{{ $Act->GetField('asset_item','id',$data->asset_item_id,'nup') }}</div>
						</div>

						<div class="form-group">
							<label>Nama Asset</label>
							<div>{{ $Act->GetField('asset_item','id',$data->asset_item_id,'name') }}</div>
						</div>

						<div class="form-group">
							<label>Kategori</label>
							<div>{{ $Act->GetField('asset_type','id',$Act->GetField('asset_item','id',$data->asset_item_id,'asset_type_id'),'name') }}</div>
						</div>

						<div class="form-group">
							<label>Waktu Pinjam</label>
							<div>{{ Carbon::parse($data->started_at)->format('d M Y').' - '.Carbon::parse($data->ended_at)->format('d M Y') }}</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Pengaju</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="ion-minus"></i>
							</button>
						</div>
					</div>

					<div class="box-body">
						<div class="form-group">
							<label>Nama</label>
							<div>{{ $Act->GetField('users','id',$data->users_id,'name') }}</div>
						</div>

						<div class="form-group">
							<label>NIK</label>
							<div>{{ $Act->GetField('users','id',$data->users_id,'nip') }}</div>
						</div>

						<div class="form-group">
							<label>Posisi</label>
							@php
								$posisi = $Act->GetField('users','id',$data->users_id,'position');
							@endphp
							<div>{{ $posisi != '' ? $posisi : '&#8211;' }}</div>
						</div>

						<div class="form-group">
							<label>Tanggal Pengajuan</label>
							<div>{{ Carbon::parse($data->created_at)->format('d M Y H:i') }}</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-6">
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Log</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="ion-minus"></i>
							</button>
						</div>
					</div>

					<div class="box-body">
						<table class="table table-striped" id="detail">
							<thead>
								<tr>
									<th width="10">No</th>
									<th>Tanggal</th>
									<th>Masalah</th>
									<th>Penyelesaian</th>
								</tr>
							</thead>
							<tbody><tr class="odd"><td colspan="4" class="dataTables_empty" valign="top"><div class="alert alert-info nomargin">Data belum ada!</div></td></tr></tbody>
						</table>
					</div>
				</div>
			</div>
		@else
			<div class="col-sm-12">
				<h4>Data Tidak Ditemukan!</h4>
			</div>
		@endif
	</div>

	@if (count($data) > 0)
		<div class="row">
			<div class="col-sm-12">
				<hr>
				@if (app('access')['update'] == '1')
					<button type="button" id="setuju" value="{{ $data->id }}" class="btn btn-primary">Setujui</button>
					<button type="button" id ="tolak" value="{{ $data->id }}" class="btn btn-danger">Tolak</button>
				@endif
				<button type="button" id ="batalkan" value="{{ $data->id }}" class="btn btn-default">Batal</button>

				<!-- 0 = Waiting Approval, 1 = Approved, 2 = Rejected, 3 = Returned -->
			</div>
		</div>
	@endif
</section>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#setuju').click('button', function (e) {
			e.preventDefault();
			var id = $(this).val();
			var data = 'id='+id+'&status=1';

			$.ajax({
				headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
				url: "{{ url('peminjaman/update')}}",
				data: data,
				type: 'post',
				dataType: 'json',
				success: function (response) {
					if (response.status === true) {
						Lobibox.notify("success", {
							icon: "ion ion-android-checkmark-circle",
							title: "Berhasil",
							msg: "Data telah berhasil diproses.",
							sound: false
						});

						setTimeout(function() {
							window.location="{{ url('peminjaman')}}";
						}, 5000);
					} else {
						Lobibox.notify("error", {
							icon: "ion ion-android-close",
							msg: "Pemrosesan data gagal!",
							sound: false
						});
					}
				}
			});
		});

		$('#batalkan').click('button', function (e) {
			e.preventDefault();
			var id = $(this).val();
			var data = 'id='+id+'&status=2';

			$.ajax({
				headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
				url: "{{ url('peminjaman/update')}}",
				data: data,
				type: 'post',
				dataType: 'json',
				success: function (response) {
					if (response.status === true) {
						Lobibox.notify("success", {
							icon: "ion ion-android-checkmark-circle",
							title: "Berhasil",
							msg: "Data telah berhasil diproses.",
							sound: false
						});

						setTimeout(function() {
							window.location="{{ url('peminjaman')}}";
						}, 5000);
					} else {
						Lobibox.notify("error", {
							icon: "ion ion-android-close",
							msg: "Pemrosesan data gagal!",
							sound: false
						});
					}
				}
			});
		});
	});
</script>
@endsection
