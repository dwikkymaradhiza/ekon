@extends('layout.default')

@section('styles-pre')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('plugins/datepicker/bootstrap-datepicker3.min.css') }}" rel="stylesheet"> -->
<link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
<style type="text/css">
	a.item-history {
		position: relative;
		z-index: 1;
	}
	a.item-history i {
		position: relative;
		z-index: -1;
	}
</style>
@endsection

@section('content')
<section class="content-header">
	<h1>Form Pengajuan Peminjaman{{ (count($asset_types) == 1 AND isset($asset_types[2])) ? ' Ruang Rapat' : '' }}</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('/') }}"><i class="ion-speedometer"></i> Dasbor</a></li>
		<li class="active">Pengajuan Peminjaman{{ (count($asset_types) == 1 AND isset($asset_types[2])) ? ' Ruang Rapat' : '' }}</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body">
					<form method="POST" action="{{ route('lending.store') }}">
						{{ csrf_field() }}
						@if (count($errors) > 0)
							<div class="col-md-12">
								<div class="alert alert-danger">
									@foreach ($errors->all() as $error)
										<p>{{ $error }}</p>
									@endforeach
								</div>
							</div>
						@endif

						<div class="col-md-4">
							<legend>Durasi Peminjaman</legend>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group req {{ $errors->first('date_from') != '' ? 'has-error' : '' }}">
										<label for="dateFrom">Mulai</label>
										<input type="text" id="dateFrom" class="datepicker form-control" name="date_from" value="{{ old('date_from') }}" />
										@if ($errors->first('date_from') != '')
											<span class="help-block">{{ $errors->first('date_from') }}</span>
										@endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group req {{ $errors->first('date_to') != '' ? 'has-error' : '' }}">
										<label for="dateTo">Hingga</label>
										<input type="text" id="dateTo" class="datepicker form-control" name="date_to" value="{{ old('date_to') }}" />
										@if ($errors->first('date_to') != '')
											<span class="help-block">{{ $errors->first('date_to') }}</span>
										@endif
									</div>
								</div>
							</div>

							@if (count($asset_types) == 1 AND isset($asset_types[2]))
								<legend>Ruang Rapat</legend>
								<input type="hidden" id="asset-type" name="asset_category" value="2">
								<div class="form-group">
									<label for="desc">Agenda Rapat</label>
									<input placeholder="Agenda Rapat" id="desc" name="desc" class="form-control desc" type="text" />
								</div>
							@else
								<legend>Aset</legend>
								<div class="form-group req {{ $errors->first('asset_category') != '' ? 'has-error' : '' }}">
									<label for="asset-type">Jenis Aset</label>
									<select id="asset-type" class="form-control select2 asset-type" name="asset_category">
										<option value="">&#8212; Pilih jenis &#8212;</option>
										@foreach($asset_types as $k => $v)
											<option value="{{ $k }}" {{ old('asset_category') == $k ? 'selected' : '' }}>{{ $v }}</option>
										@endforeach
									</select>
									@if ($errors->first('asset_category') != '')
										<span class="help-block">{{ $errors->first('asset_category') }}</span>
									@endif
								</div>
							@endif
							<div class="form-group info {{ $errors->first('nup') != '' ? 'has-error' : '' }}">
								<label>Nama {{ (count($asset_types) == 1 AND isset($asset_types[2])) ? 'Ruang Rapat' : 'Aset' }}</label>
								<input placeholder="Nama {{ (count($asset_types) == 1 AND isset($asset_types[2])) ? 'ruangan' : 'aset' }}" id="asset-name" class="form-control asset-name" readonly="readonly" type="text" />
								<input id="asset-nup" class="form-control asset-nuk" type="hidden" name="nup" />
								@if ($errors->first('nup') != '')
									<span class="help-block">{{ $errors->first('nup') }}</span>
								@endif
							</div>
						</div>

						<div class="col-md-8" id="load-asset">
							<legend>Daftar {{ (count($asset_types) == 1 AND isset($asset_types[2])) ? 'Ruang Rapat' : 'Aset' }}</legend>
							<div class="assets-info alert alert-info text-center" role="alert">
								Pilih jenis terlebih dahulu&#8230;
							</div>
							<div class="table-assets hidden" role="alert">
								<table id="table-assets" class="table table-striped table-hover" data-tables="true">
									<thead>
										<tr>
											<th width="10">No.</th>
											<th width="30">NUP</th>
											<th>Nama Aset</th>
											{!! (count($asset_types) == 1 AND isset($asset_types[2])) ? '' : '<th>Jenis</th>' !!}
											<th>Status</th>
											<th width="10">&nbsp;</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
							<br>
						</div>

						<div class="col-md-12">
							<div class="forminfo">
								<i class="ion ion-asterisk text-red"></i> Wajib diisi
							</div>
							<div class="forminfo">
								<i class="ion ion-information text-blue"></i> Wajib diisi dengan memilih salah satu aset dari daftar aset
							</div>
							<br>
							<div class="form-group">
								<input class="btn btn-primary" type="submit" name="process" value="Proses" />&nbsp;
								<input class="btn btn-default" type="reset" value="Batal / Ulang" />
							</div>
							<br>
						</div>
						<div class="clearfix"></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('modal')
<!-- ASSET ITEM HISTORY MODAL SECTION -->
<div class="modal fade stick-up" id="aset-detil" role="dialog" aria-labelledby="aset-detil" tabindex="-1" aria-hidden="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content-wrapper">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="modalAset">Detil Aset</h4>
				</div>

				<div id="detildata">
					<div class="modal-body box box-primary box-fulls">
						<div class="box-body">
							<div class="form-group inputan">
								<label>Nama Aset</label>
								<div id="data_asset_name"></div>
							</div>

							<div class="row">
								<div class="col-sm-5">
									<div class="form-group inputan">
										<label>NUP</label>
										<div id="data_asset_nup"></div>
									</div>
								</div>

								<div class="col-sm-7">
									<div class="form-group inputan">
										<label>Jenis Aset</label>
										<div id="data_asset_type"></div>
									</div>
								</div>
							</div>

							<div class="form-group inputan">
								<label>Lokasi Aset</label>
								<div id="data_asset_loc"></div>
							</div>

							<div class="form-group inputan" style="margin-bottom: 0;">
								<label>Deskripsi Aset</label>
								<div id="data_asset_desc"></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END OF THE ASSET ITEM HISTORY MODAL SECTION -->
@stop

@section('scripts')
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="{{ asset('js/id.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">
	// Get DataTable
	function getData() {
		$(".table-assets").removeClass("hidden");
		$(".assets-info").addClass("hidden");
		$("#table-assets tbody").unbind("click");

		var type = $("#asset-type").val();

		var asset_table = $("#table-assets").DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			ajax: {
				url: "{{ route('assets.available') }}/" + type,
				type: "POST",
				data: {
					"_token": "{{ csrf_token() }}",
					"from_data": $("#dateFrom").val(),
					"to_data": $("#dateTo").val()
				}
			},
			columns: [
				{ data: "rownum", name: "rownum", class: "text-right", searchable: false },
				{ data: "nuk", name: "a.nup"},
				{ data: "asset_name", name: "a.name", class: "name_of_asset" },
				@if (!(count($asset_types) == 1 AND isset($asset_types[2])))
				{ data: "category", name: "t.name" },
				@endif
				{ data: "status", name: "a.status", searchable: false },
				{ data: "action", orderable: false, searchable: false}
			],
			language: {
				url: "{{ asset('lang/Indonesian.json') }}"
			},
			responsive: true,
			initComplete: function() {
				$("#table-assets").wrap("<div class='table-responsive'></div>");
				$("select[name='table-assets_length']").select2({
					minimumResultsForSearch: Infinity
				});
			}
		});

		var selAsset = $(".asset-nuk").val();
		if (selAsset != "") {
			$("#table-assets tr.selected").removeClass("selected");
			$(".asset-nuk, .asset-name").val("");
		}

		$("#table-assets tbody").on("click", "tr", function () {
			var rowdata = asset_table.row(this).data();
			var restrict = rowdata.DT_RowAttr.class;
			$("#table-assets tr.selected").removeClass("selected");
			$(".asset-nuk, .asset-name").val("");
			if (restrict.indexOf("restrict") < 0) {
				$("#nup_" + rowdata.nuk + ".selectable").addClass("selected");
				$(".asset-nuk").val(rowdata.nuk);
				$(".asset-name").val(rowdata.nuk + " - " + rowdata.asset_name);
			}
		});
	}

	function asetDetail(event) {
		var target = $(event.target);
		$("#data_asset_name").html(target.data("asset"));
		$("#data_asset_type").html(target.data("category"));
		$("#data_asset_nup").html(target.data("nup"));
		$("#data_asset_desc").html(target.data("desc"));
		$("#data_asset_loc").html(target.data("room"));
	}

	$(document).ready(function () {
		@if (count($errors) > 0)
			setTimeout(
				function() {
					$(".alert").alert("close");
			}, 5000);
		@endif

		// Select2
		$(".select2").select2();

		// DatePicker
		$(".datepicker").datetimepicker({
			icons: {
				previous: "arrowLeft",
				next: "arrowRight"
			},
			tooltips: {
				today: 'Ke hari ini',
				clear: 'Hapus pilihan',
				close: 'Tutup jendela kalender',
				selectMonth: 'Pilih Bulan',
				prevMonth: 'Bulan Sebelumnya',
				nextMonth: 'Bulan Selanjutnya',
				selectYear: 'Pilih Tahun',
				prevYear: 'Tahun Sebelumnya',
				nextYear: 'Tahun Selanjutnya',
				selectDecade: 'Pilih Dekade',
				prevDecade: 'Dekade Sebelumnya',
				nextDecade: 'Dekade Selanjutnya',
				prevCentury: 'Abad Sebelumnya',
				nextCentury: 'Abad Selanjutnya',
				incrementHour: 'Tambah Jam',
				pickHour: 'Pilih Jam',
				decrementHour:'Kurangi Jam',
				incrementMinute: 'Tambah Menit',
				pickMinute: 'Pilih Menit',
				decrementMinute:'Kurangi Menit',
				incrementSecond: 'Tambah Detik',
				pickSecond: 'Pilih Detik',
				decrementSecond:'Kurangi Detik',
				selectTime: 'Pilih Waktu',
				selectDate: 'Pilih Tanggal'
			},
			// minDate: moment(),
			// maxDate: new Date(),
			format: "ddd, DD/MM/YYYY HH:mm"
		});

		// Default condition
		if ($("#asset-type").val() != "") {
			getData();
		}

		$("#asset-type").on("change", function() {
			getData();
			$("#asset-name, #asset-nup").val("");
		});
		$("#dateFrom").datetimepicker().on("dp.change", function(e) {
			$("#dateTo").data("DateTimePicker").minDate(e.date);
			if ($("#asset-type").val() != "") {
				getData();
			};
		});
		$("#dateTo").datetimepicker().on("dp.change", function(e) {
			$("#dateFrom").data("DateTimePicker").maxDate(e.date);
			if ($("#asset-type").val() != "") {
				getData();
			};
		});
	});
</script>
@endsection
