@extends('layout.default')

@section('styles-pre')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/datepicker/bootstrap-datepicker3.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<section class="content-header">
	<h1>Manajemen Aset</h1>
	<h2>Daftar Aset</h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('/') }}"><i class="ion-speedometer"></i> Dasbor</a></li>
		<li>Manajemen Aset</li>
		<li class="active">Daftar Aset</li>
	</ol>
	@if (app('access')['create'] == '1')
		<button type="button" class="btn btn-primary" id="indexAdd">Tambah Aset</button>
	@endif
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<table id="table" class="table table-striped table-responsive dataTable no-footer" data-tables="true" width="100%">
						<thead>
							<tr>
								<th width="10">No</th>
								<th class="text-nowrap">Nama Aset</th>
								<th>Jenis</th>
								<th class="text-nowrap">Nama Ruangan</th>
								<th>Status</th>
								<th>Pemegang Aset</th>
								<th width="10">&nbsp;</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('modal')
	<div class="modal fade stick-up" id="modalform" role="dialog" aria-labelledby="modalAset" tabindex="-1" aria-hidden="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title" id="modalAset"><span></span> Aset</h4>
					</div>

					<div id="detildata" style="display: none;">
						<div class="modal-body box box-primary box-fulls">
							<div class="box-body">
								<div class="form-group inputan">
									<label>Nama Aset</label>
									<div id="data_asset_name"></div>
								</div>

								<div class="row">
									<div class="col-sm-5">
										<div class="form-group inputan">
											<label>NUP</label>
											<div id="data_asset_nup"></div>
										</div>
									</div>

									<div class="col-sm-7">
										<div class="form-group inputan">
											<label>Jenis Aset</label>
											<div id="data_asset_type"></div>
										</div>
									</div>
								</div>

								<div class="form-group inputan">
									<label>Lokasi Aset</label>
									<div id="data_asset_loc"></div>
								</div>

								<div class="form-group inputan">
									<label>Deskripsi Aset</label>
									<div id="data_asset_desc"></div>
								</div>

								<legend>Perolehan, Perawatan dan Kepemilikan</legend>
								<div class="row">
									<div class="col-sm-5">
										<div class="form-group inputan">
											<label>Tanggal Perolehan</label>
											<div id="data_asset_date"></div>
										</div>
									</div>

									<div class="col-sm-7">
										<div class="form-group inputan">
											<label>Biaya Perawatan Rutin</label>
											<div id="data_asset_routine"></div>
										</div>
									</div>
								</div>

								<div class="form-group inputan" style="margin-bottom: 0;">
									<label>Status Kepemilikan</label>
									<div id="data_asset_owner"></div>
								</div>
							</div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						</div>
					</div>

					@if (app('access')['create'] == '1' OR app('access')['update'] == '1')
					<form method="POST" id="formupdate" action="" style="display: none;">
						{{ method_field('POST') }}
						{{ csrf_field() }}

						<div class="modal-body box box-primary box-full">
							<div class="box-body">
								<div class="form-group req {{ $errors->first('asset_name') != '' ? 'has-error' : '' }}">
									<label for="asset_name">Nama Aset</label>
									<input type="text" name="asset_name" id="asset_name" class="form-control" value="{{ old('asset_name') }}">
									@if ($errors->first('asset_name') != '')
										<span class="help-block">{{ $errors->first('asset_name') }}</span>
									@endif
								</div>

								<div class="row">
									<div class="col-sm-5">
										<div class="form-group req {{ $errors->first('asset_nup') != '' ? 'has-error' : '' }}">
											<label for="asset_nup">NUP</label>
											<input type="text" name="asset_nup" id="asset_nup" class="form-control" value="{{ old('asset_nup') }}">
											@if ($errors->first('asset_nup') != '')
												<span class="help-block">{{ $errors->first('asset_nup') }}</span>
											@endif
										</div>
									</div>

									<div class="col-sm-7">
										<div class="form-group req {{ $errors->first('asset_type') != '' ? 'has-error' : '' }}">
											<label for="asset_type">Jenis Aset</label>
											<select name="asset_type" id="asset_type" class="form-control select2" style="width: 100%;">
												<option value="">&#8212; Pilih jenis &#8212;</option>
												@foreach ($type as $k => $v)
													<option value="{{ $v['id'] }}" {{ old('asset_type') == $v['id'] ? 'selected' : '' }}>{{ ($v['short_name'] == '' ? $v['name'] : $v['short_name']).' ('.$v['code'].')' }}</option>
												@endforeach
											</select>
											@if ($errors->first('asset_type') != '')
												<span class="help-block">{{ $errors->first('asset_type') }}</span>
											@endif
										</div>
									</div>
								</div>

								<div class="form-group req {{ $errors->first('asset_loc') != '' ? 'has-error' : '' }}">
									<label for="asset_loc">Lokasi Aset</label>
									<select name="asset_loc" id="asset_loc" class="form-control select2" style="width: 100%;">
										<option value="">&#8212; Pilih lokasi &#8212;</option>
										@foreach ($location as $k => $v)
											<option value="{{ $v['id'] }}" {{ old('asset_loc') == $v['id'] ? 'selected' : '' }}>{{ $v['name'].' ('.$v['code'].')' }}</option>
										@endforeach
									</select>
									@if ($errors->first('asset_loc') != '')
										<span class="help-block">{{ $errors->first('asset_loc') }}</span>
									@endif
								</div>

								<div class="form-group">
									<label for="asset_desc">Deskripsi Aset</label>
									<textarea name="asset_desc" id="asset_desc" class="form-control" rows="4">{{ old('asset_desc') }}</textarea>
								</div>

								<legend>Perolehan, Perawatan dan Kepemilikan</legend>
								<div class="row">
									<div class="col-sm-5">
										<div class="form-group req {{ $errors->first('asset_date') != '' ? 'has-error' : '' }}">
											<label for="asset_date">Tanggal Perolehan</label>
											<input type="text" name="asset_date" id="asset_date" class="form-control" value="{{ old('asset_date') }}">
											@if ($errors->first('asset_date') != '')
												<span class="help-block">{{ $errors->first('asset_date') }}</span>
											@endif
										</div>
									</div>

									<div class="col-sm-7">
										<div class="form-group info {{ $errors->first('asset_routine') != '' ? 'has-error' : '' }}">
											<label for="asset_routine">Biaya Perawatan Rutin</label>
											<div class="input-group">
												<span class="input-group-addon" id="rupiah">Rp.</span>
												<input type="text" name="asset_routine" id="asset_routine" class="form-control" value="{{ old('asset_routine') == '' ? 0 : old('asset_routine') }}" aria-describedby="rupiah" data-mask>
											</div>
											@if ($errors->first('asset_routine') != '')
												<span class="help-block">{{ $errors->first('asset_routine') }}</span>
											@endif
										</div>
									</div>
								</div>

								<div id="ownership">
									<div class="form-group">
										<label>Status Kepemilikan</label>
									</div>
									<div class="radio" style="margin: -10px 0 15px 0;">
										<label style="padding-left: 0;">
											<input type="radio" name="is_public" id="is_public" value="1" {{ old('is_public') == '1' ? 'checked' : '' }}> &nbsp;Ruang Publik&#8230;
										</label>
										<label>
											<input type="radio" name="is_public" id="not_public" value="0" {{ (old('is_public') == '0' OR old('is_public') == '') ? 'checked' : '' }}> &nbsp;Aset Umum&#8230;
										</label>
										<label>
											<input type="radio" name="is_public" id="private" value="2" {{ old('is_public') == '2' ? 'checked' : '' }}> &nbsp;Kepemilikan&#8230;
										</label>
									</div>

									<div class="row" id="users_id_wrapper" style="{{ old('is_public') != '2' ? 'display: none;' : '' }}">
										<div class="col-sm-12">
											<div class="form-group req {{ $errors->first('users_id') != '' ? 'has-error' : '' }}">
												<label for="users_id">Kepemilikan - {{ old('is_public') }}</label>
												<select name="users_id" id="users_id" class="form-control select2" style="width: 100%;">
													<option value="">&#8212; Umum &#8212;</option>
													@foreach ($users as $k => $v)
														<option value="{{ $v['id'] }}" {{ old('users_id') == $v['id'] ? 'selected' : '' }}>{{ $v['name'].' ('.$v['nip'].')' }}</option>
													@endforeach
												</select>
												@if ($errors->first('users_id') != '')
													<span class="help-block">{{ $errors->first('users_id') }}</span>
												@endif
											</div>
										</div>
									</div>
								</div>
								<br>

								<div class="forminfo">
									<i class="ion ion-asterisk text-red"></i> Wajib diisi
								</div>
								<div class="forminfo">
									<i class="ion ion-information text-blue"></i> Nilai 0 mengindikasikan tidak ada perawatan rutin
								</div>
							</div>
						</div>

						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Simpan</button>
							<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Tutup</button>
						</div>
					</form>
					@endif

					@if (app('access')['delete'] == '1')
					<form method="POST" id="formhapus" action="" style="display: none;">
						{{ method_field('DELETE') }}
						{{ csrf_field() }}

						<div class="modal-body box box-danger">
							<div class="box-body">
								<div class="row">
									<div class="col-sm-12">
										Apakah Anda yakin ingin menghapus data <b class="asset-name"></b>?
									</div>
								</div>
							</div>
						</div>

						<div class="modal-footer">
							<button type="submit" class="btn btn-danger">Hapus</button>
							<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Tutup</button>
						</div>
					</form>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/datepicker/locales/bootstrap-datepicker.id.js') }}"></script>
<script src="{{ asset('plugins/inputmask/inputmask.js') }}"></script>
<script src="{{ asset('plugins/inputmask/inputmask.extensions.js') }}"></script>
<script src="{{ asset('plugins/inputmask/inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('plugins/inputmask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var table = $("#table").DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ url('aset/data-aset')}}",
			columns: [
				{ data: "rownum", name: "rownum", class: "text-right", searchable: false },
				{ data: "name", name: "asset_item.name", class: "cell_name" },
				{ data: "type", name: "t.name", class: "cell_type" },
				{ data: "room", name: "r.name", class: "cell_room" },
				{ data: "status", name: "status", render: function(data) {
					if (data == "0") {
						return "<span class='label label-danger'>Tidak Tersedia</span>";
					} else if (data == "1") {
						return "<span class='label label-success'>Tersedia</span>";
					} else if (data == "Publik") {
						return "<span class='label label-default'>Ruang Publik</span>";
					} else {
						return "<span class='label label-info'>Kepemilikan</span>";
					}
				}},
				{ data: "user_name", name: "u.name" },
				{ data: "action", class: "text-nowrap", orderable: false, searchable: false }
			],
			language: {
				url: "{{ asset('lang/Indonesian.json') }}"
			},
			responsive: true,
			initComplete: function() {
				$("#table").wrap("<div class='table-responsive'></div>");
				$("select[name='table_length']").select2({
					minimumResultsForSearch: Infinity
				});
			}
		});

		$(".select2").select2({ dropdownParent: $("#modalform") });

		$("#asset_date").datepicker({
			language: "id",
			autoclose: true,
			todayHighlight: true,
			format: "D, dd/mm/yyyy"
		});

		$("#asset_routine").inputmask("numeric", {
			radixPoint: ",",
			groupSeparator: ".",
			digits: 0,
			autoGroup: true,
			prefix: "",
			rightAlign: false,
		});
		$("#asset_nup").inputmask("numeric", {
			radixPoint: "",
			groupSeparator: "",
			digits: 0,
			autoGroup: true,
			prefix: "",
			rightAlign: false,
		});

		$(function () {
			$("input").iCheck({
				radioClass: "iradio_square-blue",
			});
		});

		$("input[name='is_public']").on("ifChanged", function() {
			if ($("input[name='is_public']:checked").val() == "2") {
				$("#users_id_wrapper").fadeIn("fast");
			} else {
				$("#users_id_wrapper").fadeOut("fast");
			}
		});
	});

	@if (session('success') != '')
		Lobibox.notify("success", {
			icon: "ion ion-android-checkmark-circle",
			title: "Berhasil",
			msg: "Data aset <b>{{ session('success') != 'hapus' ? '\''.session('success').'\'' : '' }}</b> telah berhasil {{ session('success') != 'hapus' ? 'disimpan' : 'dihapus' }}.",
			sound: false
		});
	@endif

	function detil(id) {
		$(".modal-title span").text("Detil Data");

		$("#data_asset_name").text($("#row_" + id + " .cell_name").text());
		$("#data_asset_nup").text($("#row_" + id).data("nup"));
		$("#data_asset_type").text($("#row_" + id).data("tipe"));
		$("#data_asset_loc").text($("#row_" + id).data("loc"));
		$("#data_asset_desc").text($("#row_" + id).data("desc"));
		$("#data_asset_date").text($("#row_" + id).data("tgl"));
		if ($("#row_" + id).data("cost") != "0") {
			$("#data_asset_routine").parent(".form-group").removeClass("hidden");
			$("#data_asset_routine").text("Rp. " + $("#row_" + id).data("cost").toLocaleString("id"));
		} else {
			$("#data_asset_routine").parents(".form-group").addClass("hidden");
		}

		var state = $("#row_" + id).data("pub");
		var owner = $("#row_" + id).data("own");
		var t = "Ruang Publik";
		if (state == "0") {
			if (owner == "0") {
				t = "Aset Umum";
			} else {
				t = owner;
			}
		}
		$("#data_asset_owner").text(t);

		$("#detildata").show();
		$("#modalform").modal("show");
	}

	@if (app('access')['create'] == '1')
		$("#indexAdd").on("click", function() {
			tambah();
		});

		function tambah() {
			$("#formupdate").attr("action", "{{ url('aset/aset') }}");
			$("#formupdate input[name='_method']").val("POST");
			$(".modal-title span").text("Tambah");
			$("#formupdate").show();
			$("#modalform").modal("show");
		}
	@endif

	@if (app('access')['update'] == '1')
		function sunting(id, old) {
			$("#formupdate").attr("action", "{{ url('aset/aset') }}/" + id);
			$("#formupdate input[name='_method']").val("PUT");
			if (old == "") {
				$("#asset_name").val($("#row_" + id + " .cell_name").text());
				$("#asset_nup").val($("#row_" + id).data("nup"));
				$("#asset_type").val($("#row_" + id).data("type"));
				$("#asset_routine").val($("#row_" + id).data("cost"));
				$("#asset_date").datepicker('setDate', new Date($("#row_" + id).data("date")));
				$("#asset_loc").val($("#row_" + id).data("room"));
				$("#asset_desc").val($("#row_" + id).data("desc"));
				$("#asset_type").trigger("change");
				$("#asset_loc").trigger("change");

				if ($("#row_" + id).data("stat") == "0") {
					$("#ownership").hide();
				} else {
					$("#ownership").show();
					if ($("#row_" + id).data("pub") == "0" && $("#row_" + id).data("user") != "0") {
						$("input[name='is_public'][value='2']").iCheck("check");
						$("#users_id_wrapper").show();
						$("#users_id").val($("#row_" + id).data("user"));
					} else {
						$("input[name='is_public'][value='" + $("#row_" + id).data("pub") + "']").iCheck("check");
						$("#users_id_wrapper").hide();
						$("#users_id").val("");
					}
					$("#users_id").trigger("change");
				}
			};
			$(".modal-title span").text("Sunting Data");
			$("#formupdate").show();
			$("#modalform").modal("show");
		}
	@endif

	@if (app('access')['delete'] == '1')
		function hapus(id, code) {
			$("#formhapus").attr("action", "{{ url('aset/aset') }}/" + id);
			$(".modal-title span").text("Hapus Data");
			$(".asset-name").text(code);
			$("#formhapus").show();
			$("#modalform").modal("show");
		}
	@endif

	$("#modalform").on("hidden.bs.modal", function() {
		$(".modal-title span, .asset-name").text("");
		$("#formupdate, #formhapus, #detildata").hide();

		$(".form-group").removeClass("has-error");
		$(".help-block").remove();
		$("#formupdate, #formhapus").attr("action", "");
		$("#asset_name, #asset_nup, #asset_type, #asset_loc, #asset_desc, #users_id").val("");
		$("#asset_routine").val("0");
		$("input[name='is_public'][value='0']").iCheck("check");
		$("#asset_date").datepicker('setDate', null);
		$("#asset_type").trigger("change");
		$("#asset_loc").trigger("change");
		$("#users_id").trigger("change");
		$("#users_id_wrapper").hide();
	});

	@if (session('error') != '')
		@if (session('error') == 'new')
			tambah();
		@else
			sunting({{ session('error') }}, "old");
		@endif
	@endif
</script>
@endsection
