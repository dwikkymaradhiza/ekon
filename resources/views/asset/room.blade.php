@extends('layout.default')

@section('styles-pre')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<section class="content-header">
	<h1>Manajemen Aset</h1>
	<h2>Daftar Ruangan</h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('/') }}"><i class="ion-speedometer"></i> Dasbor</a></li>
		<li>Manajemen Aset</li>
		<li class="active">Daftar Ruangan</li>
	</ol>
	@if (app('access')['create'] == '1')
		<button type="button" class="btn btn-primary" id="indexAdd">Tambah Ruangan</button>
	@endif
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<table id="table" class="table table-striped table-responsive dataTable no-footer" data-tables="true" width="100%">
						<thead>
							<tr>
								<th width="10">No</th>
								<th class="text-nowrap">Nama Ruangan</th>
								<th class="text-nowrap">Kode Ruangan</th>
								<th class="text-nowrap">Kode Lokasi</th>
								<th>Penangungjawab</th>
								<!-- <th>NIP</th> -->
								<th class="text-nowrap" width="10">Aset Tersedia</th>
								<th width="10">&nbsp;</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('modal')
	@if (app('access')['create'] == '1' OR app('access')['update'] == '1')
		<div class="modal fade stick-up" id="modalform" role="dialog" aria-labelledby="modalRuang" tabindex="-1" aria-hidden="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content-wrapper">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
							<h4 class="modal-title" id="modalRuang"><span></span> Ruangan</h4>
						</div>

						<form method="POST" id="formupdate" action="" style="display: none;">
							{{ method_field('POST') }}
							{{ csrf_field() }}

							<div class="modal-body box box-primary box-full">
								<div class="box-body">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group req {{ $errors->first('room_code') != '' ? 'has-error' : '' }}">
												<label for="room_code">Kode Ruangan</label>
												<input type="text" name="room_code" id="room_code" class="form-control" value="{{ old('room_code') }}">
												@if ($errors->first('room_code') != '')
													<span class="help-block">{{ $errors->first('room_code') }}</span>
												@endif
											</div>
										</div>
									</div>

									<div class="form-group req {{ $errors->first('room_name') != '' ? 'has-error' : '' }}">
										<label for="room_name">Nama Ruangan</label>
										<input type="text" name="room_name" id="room_name" class="form-control" value="{{ old('room_name') }}">
										@if ($errors->first('room_name') != '')
											<span class="help-block">{{ $errors->first('room_name') }}</span>
										@endif
									</div>

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group req {{ $errors->first('room_loc') != '' ? 'has-error' : '' }}">
												<label for="room_loc">Lokasi</label>
												<select name="room_loc" id="room_loc" class="form-control select2" style="width: 100%;">
													<option value="">&#8212; Pilih lokasi &#8212;</option>
													@foreach ($location as $k => $v)
														<option value="{{ $v['id'] }}" {{ old('room_loc') == $v['id'] ? 'selected' : '' }}>{{ $v['code'] }}</option>
													@endforeach
												</select>
												@if ($errors->first('room_loc') != '')
													<span class="help-block">{{ $errors->first('room_loc') }}</span>
												@endif
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group req {{ $errors->first('room_pic') != '' ? 'has-error' : '' }}">
												<label for="room_pic">Penanggungjawab</label>
												<select name="room_pic" id="room_pic" class="form-control select2" style="width: 100%;">
													<option value="">&#8212; Pilih penanggungjawab &#8212;</option>
													@foreach ($pic as $k => $v)
														<option value="{{ $v['id'] }}" {{ old('room_pic') == $v['id'] ? 'selected' : '' }}>{{ $v['name'] }}</option>
													@endforeach
												</select>
												@if ($errors->first('room_pic') != '')
													<span class="help-block">{{ $errors->first('room_pic') }}</span>
												@endif
											</div>
										</div>
									</div>

									<div class="forminfo">
										<i class="ion ion-asterisk text-red"></i> Wajib diisi
									</div>
								</div>
							</div>

							<div class="modal-footer">
								<button type="submit" class="btn btn-primary">Simpan</button>
								<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Tutup</button>
							</div>
						</form>

						<form method="POST" id="formhapus" action="" style="display: none;">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}

							<div class="modal-body box box-danger">
								<div class="box-body">
									<div class="row">
										<div class="col-sm-12">
											<p>Apakah Anda yakin ingin menghapus data <b class="room-name"></b>?</p>
										</div>
									</div>
								</div>
							</div>

							<div class="modal-footer">
								<button type="submit" class="btn btn-danger">Hapus</button>
								<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Tutup</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	@endif
@endsection

@section('scripts')
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var table = $("#table").DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ url('aset/data-ruangan')}}",
			columns: [
				{ data: "rownum", name: "rownum", class: "text-right", searchable: false },
				{ data: "name", name: "asset_room.name", class: "cell_name" },
				{ data: "code", name: "asset_room.code", class: "cell_code" },
				{ data: "location", name: "l.code" },
				{ data: "pic", name: "u.name" },
				// { data: "nip", name: "u.nip" },
				{ data: "assets", name: "assets", class: "text-right", searchable: false, render: function(data) {
					if (data == "0") {
						return "&#8212;";
					} else {
						return data + "&nbsp; <small class='text-muted'><i>item</i></small>";
					}
				}},
				{ data: "action", class: "text-nowrap", orderable: false, searchable: false }
			],
			language: {
				url: "{{ asset('lang/Indonesian.json') }}"
			},
			responsive: true,
			initComplete: function() {
				$("#table").wrap("<div class='table-responsive'></div>");
				$("select[name='table_length']").select2({
					minimumResultsForSearch: Infinity
				});
			}
		});

		$(".select2").select2({ dropdownParent: $("#modalform") });
	});

	@if (session('success') != '')
		Lobibox.notify("success", {
			icon: "ion ion-android-checkmark-circle",
			title: "Berhasil",
			msg: "Data ruangan <b>{{ session('success') != 'hapus' ? '\''.session('success').'\'' : '' }}</b> telah berhasil {{ session('success') != 'hapus' ? 'disimpan' : 'dihapus' }}.",
			sound: false
		});
	@endif

	@if (app('access')['create'] == '1')
		$("#indexAdd").on("click", function() {
			tambah();
		});

		function tambah() {
			$("#formupdate").attr("action", "{{ url('aset/ruangan') }}");
			$("#formupdate input[name='_method']").val("POST");
			$(".modal-title span").text("Tambah");
			$("#formupdate").show();
			$("#modalform").modal("show");
		}
	@endif

	@if (app('access')['update'] == '1')
		function sunting(id, old) {
			$("#formupdate").attr("action", "{{ url('aset/ruangan') }}/" + id);
			$("#formupdate input[name='_method']").val("PUT");
			if (old == "") {
				$("#room_code").val($("#row_" + id + " .cell_code").text());
				$("#room_name").val($("#row_" + id + " .cell_name").text());
				$("#room_loc").val($("#row_" + id).data("loc"));
				$("#room_pic").val($("#row_" + id).data("pic"));
				$("#room_loc").trigger("change");
				$("#room_pic").trigger("change");
			};
			$(".modal-title span").text("Sunting Data");
			$("#formupdate").show();
			$("#modalform").modal("show");
		}
	@endif

	@if (app('access')['delete'] == '1')
		function hapus(id, code) {
			$("#formhapus").attr("action", "{{ url('aset/ruangan') }}/" + id);
			$(".modal-title span").text("Hapus Data");
			$(".room-name").text(code);
			$("#formhapus").show();
			$("#modalform").modal("show");
		}
	@endif

	$("#modalform").on("hidden.bs.modal", function() {
		$(".modal-title span, .room-name").text("");
		$("#formupdate, #formhapus").hide();

		$(".form-group").removeClass("has-error");
		$(".help-block").remove();
		$("#formupdate, #formhapus").attr("action", "");
		$("#room_code, #room_name, #room_loc, #room_pic").val("");
	});

	@if (session('error') != '')
		@if (session('error') == 'new')
			tambah();
		@else
			sunting({{ session('error') }}, "old");
		@endif
	@endif
</script>
@endsection
