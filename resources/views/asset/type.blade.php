@extends('layout.default')

@section('content')
<section class="content-header">
	<h1>Manajemen Aset</h1>
	<h2>Daftar Jenis Aset</h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('/') }}"><i class="ion-speedometer"></i> Dasbor</a></li>
		<li>Manajemen Aset</li>
		<li class="active">Daftar Jenis Aset</li>
	</ol>
	@if (app('access')['create'] == '1')
		<button type="button" class="btn btn-primary" id="indexAdd">Tambah Jenis Aset</button>
	@endif
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<table id="table" class="table table-striped table-responsive dataTable no-footer" data-tables="true" width="100%">
						<thead>
							<tr>
								<th width="10">No</th>
								<th>Nama</th>
								<th class="text-nowrap">Nama Singkat</th>
								<th>Kode</th>
								<th class="text-nowrap" width="10">Jadwal Perawatan</th>
								<th class="text-nowrap" width="10">Aset Tersedia</th>
								<th width="10">&nbsp;</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('modal')
	@if (app('access')['create'] == '1' OR app('access')['update'] == '1')
		<div class="modal fade stick-up" id="modalform" role="dialog" aria-labelledby="modalTipe" aria-hidden="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content-wrapper">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
							<h4 class="modal-title" id="modalTipe"><span></span> Jenis Aset</h4>
						</div>

						<form method="POST" id="formupdate" action="" style="display: none;">
							{{ method_field('POST') }}
							{{ csrf_field() }}

							<div class="modal-body box box-primary box-full">
								<div class="box-body">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group req {{ $errors->first('type_code') != '' ? 'has-error' : '' }}">
												<label for="type_code">Kode Jenis</label>
												<input type="text" name="type_code" id="type_code" class="form-control" value="{{ old('type_code') }}">
												@if ($errors->first('type_code') != '')
													<span class="help-block">{{ $errors->first('type_code') }}</span>
												@endif
											</div>
										</div>
									</div>

									<div class="form-group req {{ $errors->first('type_name') != '' ? 'has-error' : '' }}">
										<label for="type_name">Nama Jenis</label>
										<input type="text" name="type_name" id="type_name" class="form-control" value="{{ old('type_name') }}">
										@if ($errors->first('type_name'))
											<span class="help-block">{{ $errors->first('type_name') }}</span>
										@endif
									</div>

									<div class="row">
										<div class="col-sm-8">
											<div class="form-group {{ $errors->first('type_short') != '' ? 'has-error' : '' }}">
												<label for="type_short">Nama Singkat</label>
												<input type="text" name="type_short" id="type_short" class="form-control" value="{{ old('type_short') }}">
												@if ($errors->first('type_short'))
													<span class="help-block">{{ $errors->first('type_short') }}</span>
												@endif
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group info {{ $errors->first('type_routine') != '' ? 'has-error' : '' }}">
												<label for="type_routine">Jadwal Perawatan</label>
												<div class="input-group">
													<input type="number" name="type_routine" id="type_routine" class="form-control" value="{{ old('type_routine') != '' ? old('type_routine') : 0 }}" aria-describedby="hari">
													<span class="input-group-addon" id="hari">hari</span>
												</div>
												@if ($errors->first('type_routine'))
													<span class="help-block">{{ $errors->first('type_routine') }}</span>
												@endif
											</div>
										</div>
									</div>

									<div class="forminfo">
										<i class="ion ion-asterisk text-red"></i> Wajib diisi
									</div>
									<div class="forminfo">
										<i class="ion ion-information text-blue"></i> Nilai 0 mengindikasikan tidak ada perawatan rutin
									</div>
								</div>
							</div>

							<div class="modal-footer">
								<button type="submit" class="btn btn-primary">Simpan</button>
								<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Tutup</button>
							</div>
						</form>

						<form method="POST" id="formhapus" action="" style="display: none;">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}

							<div class="modal-body box box-danger">
								<div class="box-body">
									<div class="row">
										<div class="col-sm-12">
											<p>Apakah Anda yakin ingin menghapus data <b class="type-name"></b>?</p>
										</div>
									</div>
								</div>
							</div>

							<div class="modal-footer">
								<button type="submit" class="btn btn-danger">Hapus</button>
								<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Tutup</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	@endif
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		var table = $("#table").DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ url('aset/data-jenis')}}",
			columns: [
				{ data: "rownum", name: "rownum", class: "text-right", searchable: false },
				{ data: "name", name: "name", class: "cell_name" },
				{ data: "short_name", name: "short_name", class: "cell_short" },
				{ data: "code", name: "code", class: "cell_code" },
				{ data: "routine_interval", name: "routine_interval", class: "text-right", render: function(data) {
					if (data == "0") {
						return "<i class='ion-android-close text-red'></i>";
					} else {
						return data + "&nbsp; <small class='text-muted'><i>hari</i></small>";
					}
				}},
				{ data: "assets", name: "assets", class: "text-right", searchable: false, render: function(data) {
					if (data == "0") {
						return "&#8212;";
					} else {
						return data + "&nbsp; <small class='text-muted'><i>item</i></small>";
					}
				}},
				{ data: "action", class: "text-nowrap", orderable: false, searchable: false }
			],
			language: {
				url: "{{ asset('lang/Indonesian.json') }}"
			},
			responsive: true,
			initComplete: function() {
				$("#table").wrap("<div class='table-responsive'></div>");
				$("select[name='table_length']").select2({
					minimumResultsForSearch: Infinity
				});
			}
		});
	});

	@if (session('success') != '')
		Lobibox.notify("success", {
			icon: "ion ion-android-checkmark-circle",
			title: "Berhasil",
			msg: "Data jenis aset <b>{{ session('success') != 'hapus' ? '\''.session('success').'\'' : '' }}</b> telah berhasil {{ session('success') != 'hapus' ? 'disimpan' : 'dihapus' }}.",
			sound: false
		});
	@endif

	@if (app('access')['create'] == '1')
		$("#indexAdd").on("click", function() {
			tambah();
		});

		function tambah() {
			$("#formupdate").attr("action", "{{ url('aset/jenis') }}");
			$("#formupdate input[name='_method']").val("POST");
			$(".modal-title span").text("Tambah");
			$("#formupdate").show();
			$("#modalform").modal("show");
		}
	@endif

	@if (app('access')['update'] == '1')
		function sunting(id, old) {
			$("#formupdate").attr("action", "{{ url('aset/jenis') }}/" + id);
			$("#formupdate input[name='_method']").val("PUT");
			if (old == "") {
				$("#type_code").val($("#row_" + id + " .cell_code").text());
				$("#type_name").val($("#row_" + id + " .cell_name").text());
				$("#type_short").val($("#row_" + id + " .cell_short").text());
				$("#type_routine").val($("#row_" + id + "").data("routine"));
			};
			$(".modal-title span").text("Sunting Data");
			$("#formupdate").show();
			$("#modalform").modal("show");
		}
	@endif

	@if (app('access')['delete'] == '1')
		function hapus(id, code) {
			$("#formhapus").attr("action", "{{ url('aset/jenis') }}/" + id);
			$(".modal-title span").text("Hapus Data");
			$(".type-name").text(code);
			$("#formhapus").show();
			$("#modalform").modal("show");
		}
	@endif

	$("#modalform").on("hidden.bs.modal", function() {
		$(".modal-title span, .type-name").text("");
		$("#formupdate, #formhapus").hide();

		$(".form-group").removeClass("has-error");
		$(".help-block").remove();
		$("#formupdate, #formhapus").attr("action", "");
		$("#type_code, #type_name, #type_short, #type_routine").val("");
	});

	@if (session('error') != '')
		@if (session('error') == 'new')
			tambah();
		@else
			sunting({{ session('error') }}, "old");
		@endif
	@endif
</script>
@endsection
