@extends('layout.display')

@section('styles-pre')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}" rel='stylesheet' />
<link href="{{ asset('plugins/fullcalendar/fullcalendar.print.min.css') }}" rel='stylesheet' media='print' />
@endsection

@section('content')
<section class="content-header">
	<h2>Daftar Ruang Meeting</h2>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<!-- <table id="table" class="table table-striped table-responsive dataTable no-footer" data-tables="true" width="100%">
						<thead>
							<tr>
								<th width="10">No</th>
								<th class="text-nowrap">Nama Ruangan</th>
								<th>Mulai Pinjam</th>
								<th>Selesai Pinjam</th>
								<th>Status</th>
							</tr>
						</thead>
					</table> -->
					{!! $calendar->calendar() !!}
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('scripts')
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/inputmask/inputmask.js') }}"></script>
<script src="{{ asset('plugins/inputmask/inputmask.extensions.js') }}"></script>
<script src="{{ asset('plugins/inputmask/inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('plugins/inputmask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/lib/moment.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>

{!! $calendar->script() !!}
<script type="text/javascript">
// $(document).ready(function() {
// 	var table = $("#table").DataTable({
// 				searching: false,
// 				paging: false,
// 				ordering: false,
// 				bInfo : false,
// 		processing: false,
// 		serverSide: true,
// 		ajax: "{{ url('aset/data-ruang-meeting')}}",
// 		columns: [
// 			{ data: "rownum", name: "rownum", class: "text-right", searchable: false },
// 						{ data: "name", name: "asset_item.name", class: "cell_name" },
// 						{ data: "started_at", name: "asset_item.started_at", class: "cell_name" },
// 						{ data: "ended_at", name: "asset_item.ended_at", class: "cell_name" },
// 						{ data: "status", name: "status", render: function(data) {
// 								if (data == "0") {
// 										return "<span class='label label-danger'>Tidak Tersedia</span>";
// 								} else if (data == "1") {
// 										return "<span class='label label-success'>Tersedia</span>";
// 								}
// 						}},
// 		],
// 		language: {
// 			url: "{{ asset('lang/Indonesian.json') }}"
// 		},
// 		responsive: true,
// 		initComplete: function() {
// 						$("#table").wrap("<div class='table-responsive'></div>");
// 						$("select[name='table_length']").select2({
// 								minimumResultsForSearch: Infinity
// 						});
// 				}
// 		// order: [ [3, "desc"] ]
// 	});
//
// 		setInterval(function () {
// 			table.ajax.reload();
// 		}, 3600000);
//  });
</script>
@endsection
