@extends('layout.default')

@section('content')
<section class="content-header">
	<h1>Manajemen Aset</h1>
	<h2>Daftar Lokasi</h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('/') }}"><i class="ion-speedometer"></i> Dasbor</a></li>
		<li>Manajemen Aset</li>
		<li class="active">Daftar Lokasi</li>
	</ol>
	@if (app('access')['create'] == '1')
		<button type="button" class="btn btn-primary" id="indexAdd">Tambah Lokasi</button>
	@endif
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<table id="table" class="table table-striped table-responsive dataTable no-footer" data-tables="true" width="100%">
						<thead>
							<tr>
								<th width="10">No</th>
								<th>Kode Lokasi</th>
								<th>Deskripsi</th>
								<th class="text-nowrap" width="10">Jumlah Ruang</th>
								<th class="text-nowrap" width="10">Aset Tersedia</th>
								<th width="10">&nbsp;</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('modal')
	@if (app('access')['create'] == '1' OR app('access')['update'] == '1')
		<div class="modal fade stick-up" id="modalform" role="dialog" aria-labelledby="modalLokasi" aria-hidden="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content-wrapper">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
							<h4 class="modal-title" id="modalLokasi"><span></span> Lokasi</h4>
						</div>

						<form method="POST" id="formupdate" action="" style="display: none;">
							{{ method_field('POST') }}
							{{ csrf_field() }}

							<div class="modal-body box box-primary box-full">
								<div class="box-body">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group req {{ $errors->first('loc_code') != '' ? 'has-error' : '' }}">
												<label for="loc_code">Kode Lokasi</label>
												<input type="text" name="loc_code" id="loc_code" class="form-control" value="{{ old('loc_code') }}">
												@if ($errors->first('loc_code') != '')
													<span class="help-block">{{ $errors->first('loc_code') }}</span>
												@endif
											</div>
										</div>
									</div>

									<div class="form-group req {{ $errors->first('loc_desc') != '' ? 'has-error' : '' }}">
										<label for="loc_desc">Deskripsi</label>
										<textarea name="loc_desc" id="loc_desc" class="form-control" rows="4">{{ old('loc_desc') }}</textarea>
										@if ($errors->first('loc_desc'))
											<span class="help-block">{{ $errors->first('loc_desc') }}</span>
										@endif
									</div>

									<div class="forminfo">
										<i class="ion ion-asterisk text-red"></i> Wajib diisi
									</div>
								</div>
							</div>

							<div class="modal-footer">
								<button type="submit" class="btn btn-primary">Simpan</button>
								<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Tutup</button>
							</div>
						</form>

						<form method="POST" id="formhapus" action="" style="display: none;">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}

							<div class="modal-body box box-danger">
								<div class="box-body">
									<div class="row">
										<div class="col-sm-12">
											<p>Apakah Anda yakin ingin menghapus data <b class="loc-name"></b>?</p>
										</div>
									</div>
								</div>
							</div>

							<div class="modal-footer">
								<button type="submit" class="btn btn-danger">Hapus</button>
								<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Tutup</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	@endif
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		var table = $("#table").DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ url('aset/data-lokasi')}}",
			columns: [
				{ data: "rownum", name: "rownum", class: "text-right", searchable: false },
				{ data: "code", name: "code", class: "cell_code" },
				{ data: "description", name: "description", class: "cell_desc" },
				{ data: "rooms", name: "rooms", class: "text-right", searchable: false, render: function(data) {
					if (data == "0") {
						return "&#8212;";
					} else {
						return data + "&nbsp; <small class='text-muted'><i>ruangan</i></small>";
					}
				}},
				{ data: "assets", name: "assets", class: "text-right", searchable: false, render: function(data) {
					if (data == "0") {
						return "&#8212;";
					} else {
						return data + "&nbsp; <small class='text-muted'><i>item</i></small>";
					}
				}},
				{ data: "action", class: "text-nowrap", orderable: false, searchable: false }
			],
			language: {
				url: "{{ asset('lang/Indonesian.json') }}"
			},
			responsive: true,
			initComplete: function() {
				$("#table").wrap("<div class='table-responsive'></div>");
				$("select[name='table_length']").select2({
					minimumResultsForSearch: Infinity
				});
			}
		});
	});

	@if (session('success') != '')
		Lobibox.notify("success", {
			icon: "ion ion-android-checkmark-circle",
			title: "Berhasil",
			msg: "Data lokasi <b>{{ session('success') != 'hapus' ? '\''.session('success').'\'' : '' }}</b> telah berhasil {{ session('success') != 'hapus' ? 'disimpan' : 'dihapus' }}.",
			sound: false
		});
	@endif

	@if (app('access')['create'] == '1')
		$("#indexAdd").on("click", function() {
			tambah();
		});

		function tambah() {
			$("#formupdate").attr("action", "{{ url('aset/lokasi') }}");
			$("#formupdate input[name='_method']").val("POST");
			$(".modal-title span").text("Tambah");
			$("#formupdate").show();
			$("#modalform").modal("show");
		}
	@endif

	@if (app('access')['update'] == '1')
		function sunting(id, old) {
			$("#formupdate").attr("action", "{{ url('aset/lokasi') }}/" + id);
			$("#formupdate input[name='_method']").val("PUT");
			if (old == "") {
				$("#loc_code").val($("#row_" + id + " .cell_code").text());
				$("#loc_desc").val($("#row_" + id + " .cell_desc").text());
			};
			$(".modal-title span").text("Sunting Data");
			$("#formupdate").show();
			$("#modalform").modal("show");
		}
	@endif

	@if (app('access')['delete'] == '1')
		function hapus(id, code) {
			$("#formhapus").attr("action", "{{ url('aset/lokasi') }}/" + id);
			$(".modal-title span").text("Hapus Data");
			$(".loc-name").text(code);
			$("#formhapus").show();
			$("#modalform").modal("show");
		}
	@endif

	$("#modalform").on("hidden.bs.modal", function() {
		$(".modal-title span, .loc-name").text("");
		$("#formupdate, #formhapus").hide();

		$(".form-group").removeClass("has-error");
		$(".help-block").remove();
		$("#formupdate, #formhapus").attr("action", "");
		$("#loc_code, #loc_desc").val("");
	});

	@if (session('error') != '')
		@if (session('error') == 'new')
			tambah();
		@else
			sunting({{ session('error') }}, "old");
		@endif
	@endif
</script>
@endsection
