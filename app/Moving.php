<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Moving extends Model
{
	use SoftDeletes;

	protected $table = 'moving';

	protected $fillable = [
		'ticket', 'asset_item_id', 'users_id', 'init_position', 'new_position', 'moved_at', 'status'
	];

	protected $dates = ['deleted_at'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
	public function item()
	{
		return $this->belongsTo('App\AssetItem');
	}
	public function initpos()
	{
		return $this->belongsTo('App\AssetRoom', 'init_position');
	}
	public function newpos()
	{
		return $this->belongsTo('App\AssetRoom', 'new_position');
	}

	public static function getMovingCompare()
	{
		return [
				Moving::where('status', '0')->whereYear('created_at', date('Y'))->count(),
				Moving::whereYear('created_at', date('Y'))->count()
			];
	}
}
