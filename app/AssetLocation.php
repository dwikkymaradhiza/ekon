<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssetLocation extends Model
{
	use SoftDeletes;

	protected $table = 'asset_location';

	protected $fillable = [
		'code', 'description'
	];

	protected $dates = ['deleted_at'];
}
