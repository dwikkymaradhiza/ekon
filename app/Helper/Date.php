<?php

namespace App\Helper;

class Date
{
	var $hari;
	var $tanggal;
	var $bulan;
	var $tahun;
	var $waktu_ori;

	function __construct()
	{
		date_default_timezone_set('Asia/Jakarta');
	}


	function Hari()
	{
		$this->hari = date('l');
		return $this->hari;
	}

	function Tanggal()
	{
		$this->tanggal = date('d');
		return $this->tanggal;
	}

	function Bulan()
	{
		$this->bulan = date('m');
		return $this->bulan;
	}

	function Tahun()
	{
		$this->tahun = date('Y');
		return $this->tahun;
	}

	function Tgl($waktu)
	{
		$tgl = substr($waktu,8,2);
		return $tgl;
	}

	function Bln($waktu)
	{
		$bln = substr($waktu,5,2);
		return $bln;
	}

	function Thn($waktu)
	{
		$thn = substr($waktu,0,4);
		return $thn;
	}

	function WaktuOri()
	{
		$this->waktu_ori = $this->tahun().'-'.$this->bulan().-$this->tanggal();
		return $this->waktu_ori;
	}

	function HariIndo($hr)
	{
		switch($hr) {
			case 'Sunday':
				$hari = ['Minggu', 'Min'];
				break;
			case 'Monday':
				$hari = ['Senin', 'Sen'];
				break;
			case 'Tuesday';
				$hari = ['Selasa', 'Sel'];
				break;
			case 'Wednesday';
				$hari = ['Rabu', 'Rab'];
				break;
			case 'Thursday';
				$hari = ['Kamis', 'Kam'];
				break;
			case 'Friday';
				$hari = ['Jumat', 'Jum'];
				break;
			case 'Saturday';
				$hari = ['Sabtu', 'Sab'];
				break;
			default:
				break;
		}
		return $hari;
	}

	function HariOri($hr)
	{
		switch($hr) {
			case 'Sunday':
				$hari = 'Sun';
				break;
			case 'Monday':
				$hari = 'Mon';
				break;
			case 'Tuesday';
				$hari = 'Tue';
				break;
			case 'Wednesday';
				$hari = 'Wed';
				break;
			case 'Thursday';
				$hari = 'Thu';
				break;
			case 'Friday';
				$hari = 'Fri';
				break;
			case 'Saturday';
				$hari = 'Sat';
				break;
			default:
				break;
		}
		return $hari;
	}

	function BulanIndo($bln)
	{
		switch($bln) {
			case '01':
				$bulan = ['Januari', 'Jan'];
				break;
			case '02':
				$bulan = ['Februari', 'Feb'];
				break;
			case '03':
				$bulan = ['Maret', 'Mar'];
				break;
			case '04':
				$bulan = ['April', 'Apr'];
				break;
			case '05':
				$bulan = ['Mei', 'Mei'];
				break;
			case '06':
				$bulan = ['Juni', 'Jun'];
				break;
			case '07':
				$bulan = ['Juli', 'Jul'];
				break;
			case '08':
				$bulan = ['Agustus', 'Agu'];
				break;
			case '09':
				$bulan = ['September', 'Sep'];
				break;
			case '10':
				$bulan = ['Oktober', 'Okt'];
				break;
			case '11':
				$bulan = ['November', 'Nov'];
				break;
			case '12':
				$bulan = ['Desember', 'Des'];
				break;
			default:
				break;
		}
		return $bulan;
	}

	function getWaktu($tanggal = null)
	{
		if ($tanggal == null):
			$hr			 = $this->Hari();
			$data['tgl'] = $this->tanggal();
			$bln		 = $this->Bulan();
			$data['thn'] = $this->tahun();
		else:
			$tanggal = strtotime($tanggal);
			$hr			 = strftime('%A', $tanggal);
			$data['tgl'] = strftime('%d', $tanggal);
			$bln		 = strftime('%m', $tanggal);
			$data['thn'] = strftime('%Y', $tanggal);
			$data['jam'] = strftime('%H', $tanggal);
		endif;

		$data['hari']	= $this->HariIndo($hr);
		$data['bulan']	= $this->BulanIndo($bln);

		return $data;
	}

	function WaktuIndo($tanggal = null)
	{
		$data = $this->getWaktu($tanggal);

		$waktu_indo = $data['hari'][0].', '.$data['tgl'].' '.$data['bulan'][0].' '.$data['thn'];
		return $waktu_indo;
	}

	function WaktuIndoLengkap($tanggal = null)
	{
		$data = $this->getWaktu($tanggal);

		$waktu_indo = $data['hari'][0].', '.$data['tgl'].' '.$data['bulan'][0].' '.$data['thn'] . ', Jam ' . $data['jam'];
		return $waktu_indo;
	}

	function WaktuIndoShort($tanggal = null)
	{
		$data = $this->getWaktu($tanggal);

		$waktu_indo = $data['hari'][1].', '.$data['tgl'].' '.$data['bulan'][1].' '.$data['thn'];
		return $waktu_indo;
	}
}

?>
