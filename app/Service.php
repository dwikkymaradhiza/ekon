<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Service extends Model
{
	use SoftDeletes;

	protected $table = 'service';

	protected $fillable = ['type', 
		'ticket', 'asset_item_id', 'users_id', 'complaint', 
		'technician', 'init_cost', 'init_info', 
		'cost', 'info', 
		'checked_at', 'done_at', 'status'
	];

	protected $dates = ['deleted_at'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
	public function item()
	{
		return $this->belongsTo('App\AssetItem');
	}
	public function technician()
	{
		return $this->belongsTo('App\User', 'technician');
	}

	public static function getServiceCompare($user_id = null)
	{
            $queries = [
                Service::whereNotIn('status', ['3', '4'])->whereYear('created_at', date('Y'))->count(),
                Service::whereYear('created_at', date('Y'))->count()
            ];
            
            if(!empty($user_id)) {
                $queries = [
                    //Total Pemeliharaan
                    Service::whereNotIn('status', ['4'])->where('technician', $user_id)->whereYear('created_at', date('Y'))->count(),
                    
                    //Pemeliharaan Tertangani
                    Service::whereIn('status', ['2', '3'])->where('technician', $user_id)->whereYear('created_at', date('Y'))->count(),
                    
                    //Pemeliharaan Belum Tertangani
                    Service::where('status', '1')->where('technician', $user_id)->whereYear('created_at', date('Y'))->count(),
                ];
            }
            
            return $queries;
	}

	public static function getReportRequest($month, $user_id = null)
	{
            $query = Service::whereMonth('created_at', $month)->whereYear('created_at', date('Y'));
            
            if(!empty($user_id)) {
                $query = $query->where('technician', $user_id);
            }
            
            return $query->count();
	}
	public static function getReportHandled($month, $user_id = null)
	{
            $query = Service::whereIn('status', ['3', '4'])->whereMonth('done_at', $month)->whereYear('done_at', date('Y'));
                
            if(!empty($user_id)) {
                $query = $query->where('technician', $user_id);
            }
            
            return $query->count();
	}
	public static function getReportCost($month, $user_id = null)
	{
            $query = Service::whereIn('status', ['3', '4'])->whereMonth('done_at', $month)->whereYear('done_at', date('Y'));
                
            if(!empty($user_id)) {
                $query = $query->where('technician', $user_id);
            }
            
            return $query->sum('cost');
	}
	public static function getReport($user_id = null)
	{
		$rep_service = [];
		for ($i = 1; $i <= date('m'); $i++):
			$rep_service['request'][] = self::getReportRequest($i, $user_id);
			$rep_service['handled'][] = self::getReportHandled($i, $user_id);
			$rep_service['cost'][]	  = self::getReportCost($i, $user_id);
		endfor;

		return $rep_service;
	}

	public static function getMostCount($user_id = null)
	{
                $request = Service::select(DB::raw('t.id, t.name, t.short_name, count(*) as total'))
					->distinct()
					->leftJoin('asset_item as i', 'service.asset_item_id', '=', 'i.id')
					->leftJoin('asset_type as t', 'i.asset_type_id', '=', 't.id')
					->whereYear('service.created_at', date('Y'));
                
                $handled = Service::select(DB::raw('t.id, t.name, t.short_name, count(*) as total'))
					->distinct()
					->leftJoin('asset_item as i', 'service.asset_item_id', '=', 'i.id')
					->leftJoin('asset_type as t', 'i.asset_type_id', '=', 't.id')
					->whereIn('service.status', ['3', '4'])
					->whereYear('done_at', date('Y'));
                
                if(!empty($user_id)) {
                    $request = $request->where('service.technician', $user_id);
                    $handled = $handled->where('service.technician', $user_id);
                }
                
		$rep_most['request'] = $request->groupBy('t.id')
					->limit(5)
					->orderBy('total', 'desc')
					->get();
                
		$rep_most['handled'] = $handled->groupBy('t.id')
					->limit(5)
					->orderBy('total', 'desc')
					->get();

		return $rep_most;
	}

	public static function getMostCost()
	{
		return Service::select(DB::raw('t.id, t.name, t.short_name, sum(cost) as total'))
					->distinct()
					->leftJoin('asset_item as i', 'service.asset_item_id', '=', 'i.id')
					->leftJoin('asset_type as t', 'i.asset_type_id', '=', 't.id')
					->whereIn('service.status', ['3', '4'])
					->whereYear('done_at', date('Y'))
					->groupBy('t.id')
					->limit(5)
					->orderBy('total', 'desc')
					->get();
	}
}
