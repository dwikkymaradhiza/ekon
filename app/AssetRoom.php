<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssetRoom extends Model
{
	use SoftDeletes;

	protected $table = 'asset_room';

	protected $fillable = [
		'code', 'name', 'description', 'asset_location_id', 'users_id'
	];

	protected $dates = ['deleted_at'];

	public function location()
	{
		return $this->belongsTo('App\AssetLocation');
	}
	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
