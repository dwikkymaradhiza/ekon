<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthPegawai extends Model
{
	protected $connection = 'mysql_pegawai';
	protected $table = 'pegawai';

	static function searchPeg($query)
	{
		return AuthPegawai::selectRaw('concat(NAMA, \' (\', NIP_BARU, \')\') as name, NIP_BARU as nip')
							->where('NAMA', 'like',  '%'.$query.'%')
							->orWhere('EMAIL', 'like', '%'.$query.'%')
							->orWhere('NIP_BARU', 'like',  '%'.$query.'%')
							->limit(10)
							->get();
	}

	static function validateAccount($username)
	{
		return AuthPegawai::where('NIP_BARU', $username)->first();
	}

	static function isValidPassword($password, $hashedPassword)
	{
		return md5($password) === $hashedPassword ;
	}
}
