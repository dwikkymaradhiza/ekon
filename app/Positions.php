<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Positions extends Model
{
    protected $table = 'positions';

	protected $fillable = ['code', 'position', 'position_short', 'unit', 'unit_short', 'asset_location_id'];

	public function location()
	{
		return $this->belongsTo('App\AssetLocation');
	}
}
