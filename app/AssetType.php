<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssetType extends Model
{
	use SoftDeletes;

	protected $table = 'asset_type';

	protected $fillable = [
		'code', 'name', 'short_name', 'routine_interval'
	];

	protected $dates = ['deleted_at'];
}
