<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\AuthPegawai;
use App\PegawaiJabatan;
use App\Positions;
use Auth;
use Validator;

class LoginController extends Controller
{
	const ROLE_RESEPSIONIS = 6;
	const ROLE_PENILIK = 5;
	const ROLE_TEKNISI = 3;
	const ROLE_SUPERADMIN = 1;

	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	* Where to redirect users after login.
	*
	* @var string
	*/
	protected $redirectTo = 'dasbor';

	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{
		$this->middleware('guest')->except('logout');
	}

	/**
	* Handle a login request to the application.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
	public function login_old(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email'		=> 'required',
			'nip'		=> 'required',
			'password'	=> 'required'
		]);

		if ($validator->fails()):
			return redirect()->back()->withErrors($validator)->withInput();
		else:
			// **
			// SEMENTARA PAKE INI DULU
			// ================================

			if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password'),'roles_id'=>3])):
				return redirect('pemeliharaan');
			endif;

			if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])):
				return redirect()->intended('/');
			endif;
			// ================================
			// SEMENTARA PAKE INI DULU
			// **

			return redirect()->back()->withErrors(['Username/password anda tidak cocok.']);

			try {
				$url	= env('AUTH_API');
				$ch		= curl_init($url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
						'username' => $request->get('nip'),
						'password' => $request->get('password')
					], true));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

				$output = curl_exec ($ch);
				curl_close ($ch);

				$response = json_decode($output);
			} catch (Exception $ex) {
				return redirect()->back()->withErrors(['Login Gagal. Silahkan Coba lagi.']);
			}

			if (empty($response)):
				return redirect()->back()->withErrors(['Login Gagal. Silahkan Coba lagi.']);
			endif;
			if (!$response->status):
				return redirect()->back()->withErrors([$response->message]);
			endif;

			// if user not exist in auth DB
			if (empty($response->message)):
				$user = User::where('email', $request->get('email'))->first();
				if (empty($user)):
					return redirect()->back()->withErrors(['Pengguna tidak ditemukan.']);
				endif;
				if ($user->roles_id !== self::ROLE_TEKNISI && $user->roles_id !== self::ROLE_SUPERADMIN):
					return redirect()->back()->withErrors(['Pengguna tidak ditemukan.']);
				endif;
				if (Auth::attempt(['nip' => $user->nip, 'password' => $request->input('password')])):
					return redirect()->intended('/');
				endif;
				if (Auth::attempt(['email' => $user->email, 'password' => $request->input('password')])):
					return redirect()->intended('/');
				endif;
			// check users apps db
			else:
				$user = User::where('simpeg_id', $response->message->ID)->first();
				if (empty($user)):
					$user = new User;
					$user->name = $response->message->NAMA;
					$user->email = $response->message->EMAIL;
					$user->simpeg_id = $response->message->ID;
					$user->nip = $response->message->NIP_BARU;
					$user->role_id = 4;
					$user->password = app('hash')->make($response->message->NIP_BARU);
					$user->position = '';
					$user->save();
				endif;
				// $request->merge(['nip' => $user->nip, 'password' => $user->nip]);
				if (Auth::attempt(['nip' => $user->nip, 'password' => $user->nip])):
					return redirect()->intended('/');
				endif;
			endif;

			return redirect()->back()->withErrors(['Username/password anda tidak cocok.']);
		endif;
	}

	/**
	* Handle a login request to the application.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
	public function login(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email'		=> 'required',
			'nip'		=> 'required',
			'password'	=> 'required'
		]);

		// if ($validator->fails()):
		// 	return redirect()->back()->withErrors($validator)->withInput();
		// else:
		// 	// **
		// 	// SEMENTARA PAKE INI DULU
		// 	// ================================
		//
		// 	if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password'),'roles_id'=>3])):
		// 		return redirect('pemeliharaan');
		// 	endif;
		//
		// 	if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])):
		// 		return redirect()->intended('/');
		// 	endif;
		// 	// ================================
		// 	// SEMENTARA PAKE INI DULU
		// 	// **
		// endif;


		if ($validator->fails()):
			return redirect()->back()->withErrors($validator)->withInput();
		else:
			try {
				$username = $request->get('nip');
				$password = $request->get('password');

				$response = AuthPegawai::validateAccount($username);
			} catch (Exception $ex) {
				return redirect()->back()->withErrors(['Login Gagal. Silahkan Coba lagi.']);
			}

			// if user not exist in auth DB
			if (empty($response)):
				$user = User::where('email', $request->get('email'))->first();
				// return $user;
				if (empty($user)):
					return redirect()->back()->withErrors(['Pengguna tidak ditemukan.']);
				endif;
				if ($user->roles_id !== self::ROLE_TEKNISI AND $user->roles_id !== self::ROLE_SUPERADMIN AND $user->roles_id !== self::ROLE_PENILIK AND $user->roles_id !== self::ROLE_RESEPSIONIS):
					return redirect()->back()->withErrors(['Pengguna tidak ditemukan.']);
				endif;
				// if (Auth::attempt(['nip' => $user->nip, 'password' => $request->input('password')])):
				// 	return redirect()->intended('/');
				// endif;
				if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])):
					return redirect()->intended('/');
				endif;
			// check users apps db
			elseif (!AuthPegawai::isValidPassword($password, $response->PASSWORD)):
				return redirect()->back()->withErrors(['Password yang anda masukan salah.']);
			else:
				$user = User::where('simpeg_id', $response->ID)->first();
								$pegawaiJabatan = PegawaiJabatan::where('ID', $response->ID_JABATAN_TERAKHIR)->first();
								$positions = 0;

				if (!empty($pegawaiJabatan)) {
					$position = Positions::where('code', $pegawaiJabatan->KD_JAB)->first();

					if (!empty($position)) {
						$positions = $position->id;
					}
				}

				if (empty($user)):
					$user = new User;
					$user->name = $response->NAMA;
					$user->email = $response->EMAIL;
					$user->simpeg_id = $response->ID;
					$user->nip = $response->NIP_BARU;
					$user->roles_id = 4;
					$user->password = app('hash')->make($response->NIP_BARU);
					$user->positions_id = $positions;
					$user->save();
				endif;
				// $request->merge(['nip' => $user->nip, 'password' => $user->nip]);
				if (Auth::attempt(['nip' => $user->nip, 'password' => $response->NIP_BARU])):
					return redirect()->intended('/');
				endif;
			endif;

			return redirect()->back()->withErrors(['Username/password anda tidak cocok.']);
		endif;
	}
}
