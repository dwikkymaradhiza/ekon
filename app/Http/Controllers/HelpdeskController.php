<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Action;
use App\User;
use App\Helper\Date;
use Response;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;


class HelpdeskController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function __construct()
	{
		// $this->middleware('auth');
		$this->Act = new Action;
		$this->Date = new Date;
	}

	public function index()
	{
		$this->users = User::select('id', 'name')->where('roles_id', 3)->get();
		return view('helpdesk.index', get_object_vars($this));
	}

	public function indexLog()
	{
		return view('helpdesk.indexlog');
	}

	// DATATABLES
	public function getList(Request $request)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$helpdesk = DB::table('service as s')
			->join('users as u', 's.users_id', '=', 'u.id')
			->join('asset_item as a', 's.asset_item_id', '=', 'a.id')
			->select([
				DB::raw('@rownum := @rownum + 1 AS rownum'),
					's.id',
					's.ticket',
					's.asset_item_id',
					's.complaint',
					's.created_at',
					's.status',
					's.users_id',
					's.technician',
					's.init_info',
					's.init_cost',
					'a.name as asset_name',
					'a.nup',
					'u.name as user_name',
					'u.nip',
					'p.position',
					'u.id as user_id'
				])
			->leftJoin('positions as p', 'u.positions_id', '=', 'p.id')
			->whereIn('s.status', ['0', '1', '2'])
			->whereNull('s.deleted_at')
			->whereNull('u.deleted_at')
			->whereNull('a.deleted_at');

			if (Auth::user()->roles_id == 3):
				$helpdesk = $helpdesk->where('technician',Auth::user()->id );
			endif;

		$datatables = Datatables::of($helpdesk)
			->addColumn('action', function ($helpdesk)
				{
					$idType = $this->Act->GetField('asset_item','id',$helpdesk->asset_item_id,'asset_type_id');
					$type = $this->Act->GetField('asset_type','id',$idType,'name');
					$type_short = $this->Act->GetField('asset_type','id',$idType,'short_name');
					$type_short != '' ? ($type = $type_short.' ('.$this->Act->GetField('asset_type','id',$idType,'code').')') : '';
					$tehName = $this->Act->GetField('users','id',$helpdesk->technician,'name');
					$idRoom = $this->Act->GetField('asset_item','id',$helpdesk->asset_item_id,'asset_room_id');
					$location = $this->Act->GetField('asset_room','id',$idRoom,'name').' ('.$this->Act->GetField('asset_room','id',$idRoom,'code').')';
					return '<a href="#" data-id="'.$helpdesk->id.
						'" data-ticket="'.$helpdesk->ticket.
						'" data-created_at="'.$this->Date->WaktuIndoShort($helpdesk->created_at).
						'" data-asset_name="'.$helpdesk->asset_name.
						'" data-tehname="'.$tehName.
						'" data-user_name="'.$helpdesk->user_name.
						'" data-nup="'.$helpdesk->nup.
						'" data-asset_item_id="'.$helpdesk->asset_item_id.
						'" data-complaint="'.$helpdesk->complaint.
						'" data-nip="'.$helpdesk->nip.
						'" data-init_info="'.$helpdesk->init_info.
						'" data-init_cost="Rp. '.number_format($helpdesk->init_cost, 0, ',', '.').
						'" data-position="'.$helpdesk->position.
						'" data-user_id="'.$helpdesk->user_id.
						'" data-technician="'.$helpdesk->technician.
						'" data-status="'.$helpdesk->status.
						'" data-type="'.$type.
						'" data-location="'.$location.
						'" data-toggle="modal" data-target="#modal_edit"><i class="ion ion-clipboard text-green"></i>
					</a>';
				})
			->editColumn('created_at', function ($helpdesk)
				{
					return $this->Date->WaktuIndoShort($helpdesk->created_at);
				})
			->removeColumn('id');

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}
	public function getLog(Request $request)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$helpdesk = DB::table('service as s')
			->join('users as u', 's.users_id', '=', 'u.id')
			->join('asset_item as a', 's.asset_item_id', '=', 'a.id')
			->select([
				DB::raw('@rownum := @rownum + 1 AS rownum'),
					's.id',
					's.ticket',
					's.asset_item_id',
					's.complaint',
					's.created_at',
					's.done_at',
					's.cost',
					's.info',
					's.status',
					's.users_id',
					's.technician',
					's.init_info',
					's.init_cost',
					'a.name as asset_name',
					'a.nup',
					'u.name as user_name',
					'u.nip',
					'p.position',
					'u.id as user_id'
				])
			->leftJoin('positions as p', 'u.positions_id', '=', 'p.id')
			->whereIn('s.status', ['3', '4'])
			->whereNull('s.deleted_at')
			->whereNull('u.deleted_at')
			->whereNull('a.deleted_at');

			if (Auth::user()->roles_id == 3):
			$helpdesk = $helpdesk->where('technician',Auth::user()->id );
			endif;

		$datatables = Datatables::of($helpdesk)
			->addColumn('action', function ($helpdesk)
				{
					$idType = $this->Act->GetField('asset_item','id',$helpdesk->asset_item_id,'asset_type_id');
					$type = $this->Act->GetField('asset_type','id',$idType,'name');
					$type_short = $this->Act->GetField('asset_type','id',$idType,'short_name');
					$type_short != '' ? ($type = $type_short.' ('.$this->Act->GetField('asset_type','id',$idType,'code').')') : '';
					$tehName = $this->Act->GetField('users','id',$helpdesk->technician,'name');
					$idRoom = $this->Act->GetField('asset_item','id',$helpdesk->asset_item_id,'asset_room_id');
					$location = $this->Act->GetField('asset_room','id',$idRoom,'name').' ('.$this->Act->GetField('asset_room','id',$idRoom,'code').')';
					return '<a href="#" data-id="'.$helpdesk->id.
						'" data-ticket="'.$helpdesk->ticket.
						'" data-created_at="'.$this->Date->WaktuIndoShort($helpdesk->created_at).
						'" data-asset_name="'.$helpdesk->asset_name.
						'" data-tehname="'.$tehName.
						'" data-user_name="'.$helpdesk->user_name.
						'" data-nup="'.$helpdesk->nup.
						'" data-asset_item_id="'.$helpdesk->asset_item_id.
						'" data-complaint="'.$helpdesk->complaint.
						'" data-nip="'.$helpdesk->nip.
						'" data-init_info="'.$helpdesk->init_info.
						'" data-init_cost="'.$helpdesk->init_cost.
						'" data-position="'.$helpdesk->position.
						'" data-user_id="'.$helpdesk->user_id.
						'" data-technician="'.$helpdesk->technician.
						'" data-status="'.$helpdesk->status.
						'" data-info="'.$helpdesk->info.
						'" data-type="'.$type.
						'" data-location="'.$location.
						'" data-toggle="modal" data-target="#modal_edit"><i class="ion ion-clipboard text-green"></i>
					</a>';
				})
			->editColumn('complaint', function ($helpdesk)
				{
					return str_limit($helpdesk->complaint, 256, '&#8230;');
				})
			->editColumn('created_at', function ($helpdesk)
				{
					return $this->Date->WaktuIndoShort($helpdesk->created_at);
				})
			->editColumn('done_at', function ($helpdesk)
				{
					return $this->Date->WaktuIndoShort($helpdesk->done_at);
				})
			->editColumn('cost', function ($helpdesk)
				{
					return '<span style="position: absolute;">Rp.</span><span class="pull-right" style="padding-left: 26px;">'.number_format($helpdesk->cost, 0, ',', '.').'</span>';
				})
			->escapeColumns([])
			->removeColumn('id');

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}
	public function getDetailHelpdesk(Request $request, $id)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$helpdesk = DB::table('service')
			->select([DB::raw('@rownum := @rownum + 1 AS rownum'),
				'id', 'asset_item_id', 'created_at',
				'users_id', 'complaint',
				'init_info', 'init_cost',
				'info', 'cost', 'done_at',
				'status'])
			->where('status', '=', '3')
			->where('asset_item_id', '=', $id)
			->whereNull('deleted_at');

		$datatables = Datatables::of($helpdesk)
			->editColumn('complaint', function ($helpdesk)
				{
					return str_limit($helpdesk->complaint, 256, '&#8230;');
				})
			->editColumn('created_at', function ($helpdesk)
				{
					return $this->Date->WaktuIndoShort($helpdesk->created_at);
				})
			->editColumn('done_at', function ($helpdesk)
				{
					return $this->Date->WaktuIndoShort($helpdesk->done_at);
				})
			->editColumn('cost', function ($helpdesk)
				{
					return '<span style="position: absolute;">Rp.</span><span class="pull-right" style="padding-left: 26px;">'.number_format($helpdesk->cost, 0, ',', '.').'</span>';
				})
			->escapeColumns([]);

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$this->data = DB::table('service')->where('id',$id)->first();
		return view('helpdesk.form',get_object_vars($this));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		if ($request->status == 1):
			$rules = [
				'status'	 => 'required',
				'technician' => 'required'
			];
			$this->validate($request, $rules);

			DB::table('service')->where('id', $request->id)
				->update([
					'status'	 => $request->status,
					'technician' => $request->technician
				]);
			return Response::json(['status' => true, 'message' => 'Data Berhasil Disimpan']);
		endif;

		if ($request->status == 2):
			$rules = [
				'information' => 'required',
				'cost'		  => 'required'
			];
			$this->validate($request, $rules);
			DB::table('service')->where('id', $request->id)
				->update([
					'status'	=> $request->status,
					'init_info'	=> $request->information,
					'init_cost'	=> str_replace('.', '', $request->cost)
				]);
			return Response::json(['status' => true, 'message' => 'Data Berhasil Disimpan']);
		endif;

		if ($request->status == 3):
			$rules = [
				'notes'		 => 'required',
				'cost_final' => 'required'
			];
			$this->validate($request, $rules);

			DB::table('service')->where('id', $request->id)
				->update([
					'status'	=> $request->status,
					'info'		=> $request->notes,
					'cost'		=> str_replace('.', '', $request->cost_final)
				]);
			return Response::json(['status' => true, 'message' => 'Data Berhasil Disimpan']);
		endif;

		if ($request->status == 4):
			if (!empty($request->notes)):
				$rules = [
				   'notes' => 'required'
				];
				$this->validate($request, $rules);
				DB::table('service')->where('id', $request->id)
					->update([
						'status' => $request->status,
						'info'	 => $request->notes
					]);
				return Response::json(['status' => true, 'message' => 'Data Berhasil Disimpan']);
			else:
				DB::table('service')->where('id',$request->id)
					->update([
						'status' => $request->status,
						'info'	 => $request->notes
					]);
				return Response::json(['status' => true, 'message' => 'Data Berhasil Disimpan']);
			endif;
		endif;
	}

	public function updateProses(Request $request)
	{
		DB::table('service')
			->where('id', $request->id)
			->update([
				'status' => $request->status,
				'info'	 =>$request->notes
			]);

		return Response::json(['status' => true, 'message' => 'Data Berhasil Disimpan']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Display helpdesk data refresh every 1 hour.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function display_view()
	{
		return view('helpdesk.display');
	}

	/**
	 * Display helpdesk data refresh every 1 hour.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function display_data(Request $request)
	{
			DB::statement(DB::raw('set @rownum=0'));
			$helpdesk = DB::table('service as s')
				->join('users as u', 's.users_id', '=', 'u.id')
				->join('asset_item as a', 's.asset_item_id', '=', 'a.id')
				->select([
					DB::raw('@rownum := @rownum + 1 AS rownum'),
						's.id',
						's.ticket',
						's.asset_item_id',
						's.complaint',
						's.created_at',
						's.status',
						's.users_id',
						's.technician',
						's.init_info',
						's.init_cost',
						'a.name as asset_name',
						'a.nup',
						'u.name as user_name',
						'u.nip',
						'p.position',
						'u.id as user_id'
					])
				->leftJoin('positions as p', 'u.positions_id', '=', 'p.id')
				->whereIn('s.status', ['0', '1', '2'])
				->whereNull('s.deleted_at')
				->whereNull('u.deleted_at')
				->whereNull('a.deleted_at');

			$datatables = Datatables::of($helpdesk)
				->editColumn('created_at', function ($helpdesk)
					{
						return $this->Date->WaktuIndoShort($helpdesk->created_at);
					})
				->removeColumn('id');

			if ($keyword = $request->get('search')['value']):
				$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
			endif;

			return $datatables->make(true);
	}
}
