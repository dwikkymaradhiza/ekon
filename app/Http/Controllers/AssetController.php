<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\AssetItem;
use App\AssetType;
use App\AssetRoom;
use App\AssetLocation;
use App\User;
use App\Lending;
use Response;
use DB;
use Validator;
use Calendar;
// use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Helper\Date;

class AssetController extends Controller {

	public function __construct()
	{
		$this->Date = new Date;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('asset.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function user()
	{
		//
	}

	// Asset Index
	public function asset()
	{
		$type = AssetType::all();
		$location = AssetRoom::all();
		$users = User::all();
		return view('asset.asset')->with('type', $type)->with('location', $location)->with('users', $users);
	}

	// Asset Index API
	public function assetList(Request $request)
	{
		$access = app('access');
		DB::statement(DB::raw('set @rownum=0'));

		$asset = AssetItem::select(DB::raw('@rownum := @rownum + 1 AS rownum'),
					'asset_item.name', 't.name as type_long', 't.short_name as type_short',
					't.id as type_id', 't.code as type_code', 'nup', 'asset_item.users_id',
					'r.id as room_id', 'r.code as room_code', 'r.name as room',
					'routine_cost', 'is_public', 'acquisition', 'asset_item.description',
					DB::raw('IF(asset_item.is_public = \'0\', IF(asset_item.users_id IS NOT NULL, \'Private\', status), \'Publik\') AS status'),
					'u.name AS user_name', 'asset_item.id')
				->leftJoin('asset_type as t', 'asset_item.asset_type_id', '=', 't.id')
				->leftJoin('asset_room as r', 'asset_item.asset_room_id', '=', 'r.id')
				->leftJoin('users as u', 'asset_item.users_id', '=', 'u.id');

		$datatables = Datatables::of($asset)
			->addColumn('type', function($asset)
				{
					return ($asset->type_short != '' ? $asset->type_short : $asset->type_long);
				})
			->editColumn('acquisition', function($asset)
				{
					return $this->Date->WaktuIndoShort($asset->acquisition);
				})
			->addColumn('action', function($asset) use($access)
			 	{
					return '<a href="#" onclick="detil('.$asset->id.'); return false;"><i class="ion ion-clipboard text-green"></i></a>'.
						($access['update'] == '1' ? '<a href="#" onclick="sunting('.$asset->id.', \'\'); return false;"><i class="ion ion-compose text-light-blue"></i></a>' : '').
						($access['delete'] == '1' ? '<a href="#" onclick="hapus('.$asset->id.', \''.$asset->name.'\'); return false;"><i class="ion ion-trash-a text-red"></i></a>' : '');
				})
			->addColumn('la', function($asset)
				{
					return $this->Date->WaktuIndoShort($asset->acquisition);
				})
			->setRowAttr([
					'data-type'	=> '{{ $type_id }}',
					'data-tipe'	=> '{{ $type_short != \'\' ? $type_short.\' (\'.$type_code.\')\' : $type_long }}',
					'data-room'	=> '{{ $room_id }}', // .\' (\'.$room_code.\')\'
					'data-loc'	=> '{{ $room.\' (\'.$room_code.\')\' }}',
					'data-cost'	=> '{{ $routine_cost == \'\' ? 0 : $routine_cost }}',
					'data-date'	=> '{{ Carbon::parse($acquisition)->format(\'m/d/Y\') }}',
					'data-tgl'	=> '{{ $la }}',
					'data-desc'	=> '{{ $description }}',
					'data-nup'	=> '{{ $nup }}',
					'data-user'	=> '{{ $users_id == \'\' ? 0 : $users_id }}',
					'data-own'	=> '{{ $users_id == \'\' ? 0 : $user_name }}',
					'data-pub'	=> '{{ $is_public }}',
					'data-stat'	=> '{{ $status }}'
				])
			->setRowId('row_{{ $id }}')
			->removeColumn('type_long')
			->removeColumn('type_short')
			->removeColumn('type_id')
			->removeColumn('room_id')
			->removeColumn('routine_cost')
			->removeColumn('description')
			->removeColumn('la')
			->removeColumn('id');

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
			$datatables->filterColumn('t.name', 'whereRaw', 't.name like ?', ["%{$keyword}%"]);
			$datatables->filterColumn('t.name', 'whereRaw', 't.short_name like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	// Asset Store
	public function assetStore(Request $request) {
		$req = $request->all();

		if ($req['asset_routine'] != ''):
			$req['asset_routine'] = str_replace('.', '', $req['asset_routine']);
		endif;
		if ($req['asset_date'] != ''):
			$date = explode(', ', $req['asset_date']);
			$date = explode('/', $date[1]);
			$req['asset_date'] = $date[2].'-'.$date[1].'-'.$date[0];
		endif;

		$validator = Validator::make($req,
		[
			'asset_nup' 	=> 'required|numeric|unique:asset_item,nup',
			'asset_name' 	=> 'required',
			'asset_type'	=> 'required',
			'asset_date'	=> 'required|date_format:"Y-m-d"',
			'asset_loc'		=> 'required',
			'asset_routine'	=> 'required|numeric|min:0',
			'is_public'		=> 'required',
			'users_id'		=> 'required_if:is_public,2'
		]);

		if ($validator->fails()):
			return redirect('aset/aset')
						->with('error', 'new')
						->withErrors($validator)
						->withInput();
		else:
			$asset = new AssetItem;
			$asset->nup = $req['asset_nup'];
			$asset->name = $req['asset_name'];
			$asset->asset_type_id = $req['asset_type'];
			$asset->acquisition = $req['asset_date'];
			$asset->asset_room_id = $req['asset_loc'];
			$asset->routine_cost = $req['asset_routine'];
			$desc = $req['asset_desc'];
			$asset->description = $desc == '' ? null : $desc;
			$pub = $req['is_public'];
			$asset->is_public = ($pub == '2' ? '0' : $pub);
			$asset->users_id = ($pub == '2' ? $req['users_id'] : null);
			$asset->save();

			return redirect('aset/aset')->with('success', $request->input('asset_name'));
		endif;
	}

	// Asset Store
	public function assetUpdate(Request $request, $id) {
		$req = $request->all();
		$req['asset_routine'] = str_replace('.', '', $req['asset_routine']);
		$date = explode(', ', $req['asset_date']);
		$date = explode('/', $date[1]);
		$req['asset_date'] = $date[2].'-'.$date[1].'-'.$date[0];

		$validator = Validator::make($req,
		[
			'asset_nup' 	=> 'required|numeric|unique:asset_item,nup,'.$id,
			'asset_name' 	=> 'required',
			'asset_type'	=> 'required',
			'asset_date'	=> 'required|date_format:"Y-m-d"',
			'asset_loc'		=> 'required',
			'asset_routine'	=> 'required|numeric|min:0',
			'is_public'		=> 'required',
			'users_id'		=> 'required_if:is_public,2'
		]);

		if ($validator->fails()):
			return redirect('aset/aset')
						->with('error', $id)
						->withErrors($validator)
						->withInput();
		else:
			$asset = AssetItem::find($id);
			$asset->nup = $req['asset_nup'];
			$asset->name = $req['asset_name'];
			$asset->asset_type_id = $req['asset_type'];
			$asset->acquisition = $req['asset_date'];
			$asset->asset_room_id = $req['asset_loc'];
			$asset->routine_cost = $req['asset_routine'];
			$desc = $req['asset_desc'];
			$asset->description = ($desc == '' ? null : $desc);
			$pub = $req['is_public'];
			$asset->is_public = ($pub == '2' ? '0' : $pub);
			$asset->users_id = ($pub == '2' ? $req['users_id'] : null);
			$asset->save();

			return redirect('aset/aset')->with('success', $request->input('asset_name'));
		endif;
	}

	// Asset Delete
	public function assetDestroy($id) {
		$asset = AssetItem::find($id);
		$asset->delete();

		return redirect('aset/aset')->with('success', 'hapus');
	}

	// Asset Type Index
	public function type()
	{
		return view('asset.type');
	}

	// Room Type API
	public function typeList(Request $request)
	{
		$access = app('access');
		DB::statement(DB::raw('set @rownum=0'));

		$type = AssetType::select(DB::raw('@rownum := @rownum + 1 AS rownum'),
						'name', 'short_name', 'code', 'routine_interval',
						DB::raw('(SELECT COUNT(*) FROM asset_item WHERE asset_type_id = asset_type.id AND status = \'1\') AS assets'), 'id');

		$datatables = Datatables::of($type)
			->addColumn('action', function($type) use($access)
			 	{
					return ($access['update'] == '1' ? '<a href="#" onclick="sunting('.$type->id.', \'\'); return false;"><i class="ion ion-compose text-light-blue"></i></a>' : '').
						($access['delete'] == '1' ? '<a href="#" onclick="hapus('.$type->id.', \''.$type->code.'\'); return false;"><i class="ion ion-trash-a text-red"></i></a>' : '');
				})
			->setRowId('row_{{ $id }}')
			->setRowAttr([
					'data-routine' => '{{ $routine_interval }}'
				])
			->removeColumn('id');

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	// Type Store
	public function typeStore(Request $request) {
		$validator = Validator::make($request->all(),
		[
			'type_code' 	=> 'required|numeric|unique:asset_type,code',
			'type_name' 	=> 'required',
			'type_routine'	=> 'required|integer'
		]);

		if ($validator->fails()):
			return redirect('aset/jenis')
						->with('error', 'new')
						->withErrors($validator)
						->withInput();
		else:
			$type = new AssetType;
			$type->code = $request->input('type_code');
			$type->name = $request->input('type_name');
			$short = $request->input('type_short');
			$type->short_name = $short == '' ? null : $short;
			$type->routine_interval = $request->input('type_routine');
			$type->save();

			return redirect('aset/jenis')->with('success', $request->input('type_code'));
		endif;
	}

	// Type Update
	public function typeUpdate(Request $request, $id) {
		$validator = Validator::make($request->all(),
		[
			'type_code' 	=> 'required|numeric|unique:asset_type,code,'.$id,
			'type_name' 	=> 'required',
			'type_routine'	=> 'required|integer'
		]);

		if ($validator->fails()):
			return redirect('aset/jenis')
						->with('error', $id)
						->withErrors($validator)
						->withInput();
		else:
			$type = AssetType::find($id);
			$type->code = $request->input('type_code');
			$type->name = $request->input('type_name');
			$short = $request->input('type_short');
			$type->short_name = $short == '' ? null : $short;
			$type->routine_interval = $request->input('type_routine');
			$type->save();

			return redirect('aset/jenis')->with('success', $request->input('type_code'));
		endif;
	}

	// Type Delete
	public function typeDestroy($id) {
		$type = AssetType::find($id);
		$type->delete();

		return redirect('aset/jenis')->with('success', 'hapus');
	}

	// Room Index
	public function room()
	{
		$location = AssetLocation::all();
		$pic = User::all();
		return view('asset.room')->with('location', $location)->with('pic', $pic);
	}

	// Room Index API
	public function roomList(Request $request)
	{
		$access = app('access');
		DB::statement(DB::raw('set @rownum=0'));

		$room = AssetRoom::select(DB::raw('@rownum := @rownum + 1 AS rownum'),
						'asset_room.name', 'asset_room.code',
						'l.code as location', 'l.id as locid', 'u.name as pic', 'u.nip', 'u.id as uid',
						DB::raw('(SELECT COUNT(*) FROM asset_item WHERE asset_room_id = asset_room.id AND asset_item.status = \'1\') AS assets'),
						'asset_room.id')
					->leftJoin('asset_location as l', 'asset_location_id', '=', 'l.id')
					->leftJoin('users as u', 'users_id', '=', 'u.id');

		$datatables = Datatables::of($room)
			->addColumn('action', function($room) use($access)
			 	{
					return ($access['update'] == '1' ? '<a href="#" onclick="sunting('.$room->id.', \'\'); return false;"><i class="ion ion-compose text-light-blue"></i></a>' : '').
						($access['delete'] == '1' ? '<a href="#" onclick="hapus('.$room->id.', \''.$room->code.'\'); return false;"><i class="ion ion-trash-a text-red"></i></a>' : '');
				})
			->setRowId('row_{{ $id }}')
			->setRowAttr([
					'data-loc' => '{{ $locid }}',
					'data-pic' => '{{ $uid }}'
				])
			->removeColumn('locid')
			->removeColumn('uid')
			->removeColumn('id');

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	// Room Store
	public function roomStore(Request $request) {
		$validator = Validator::make($request->all(),
		[
			'room_code' => 'required|unique:asset_room,code',
			'room_name' => 'required',
			'room_loc'	=> 'required',
			'room_pic'	=> 'required'
		]);

		if ($validator->fails()):
			return redirect('aset/ruangan')
						->with('error', 'new')
						->withErrors($validator)
						->withInput();
		else:
			$room = new AssetRoom;
			$room->code = $request->input('room_code');
			$room->name = $request->input('room_name');
			$room->asset_location_id = $request->input('room_loc');
			$room->users_id = $request->input('room_pic');
			$room->save();

			return redirect('aset/ruangan')->with('success', $request->input('room_code'));
		endif;
	}

	// Room Update
	public function roomUpdate(Request $request, $id) {
		$validator = Validator::make($request->all(),
		[
			'room_code' => 'required|unique:asset_room,code,'.$id,
			'room_name' => 'required',
			'room_loc'	=> 'required',
			'room_pic'	=> 'required'
		]);

		if ($validator->fails()):
			return redirect('aset/ruangan')
						->with('error', $id)
						->withErrors($validator)
						->withInput();
		else:
			$room = AssetRoom::find($id);
			$room->code = $request->input('room_code');
			$room->name = $request->input('room_name');
			$room->asset_location_id = $request->input('room_loc');
			$room->users_id = $request->input('room_pic');
			$room->save();

			return redirect('aset/ruangan')->with('success', $request->input('room_code'));
		endif;
	}

	// Room Delete
	public function roomDestroy($id) {
		$room = AssetRoom::find($id);
		$room->delete();

		return redirect('aset/ruangan')->with('success', 'hapus');
	}

	// Location Index
	public function location()
	{
		return view('asset.location');
	}

	// Location Index API
	public function locationList(Request $request)
	{
		$access = app('access');
		DB::statement(DB::raw('set @rownum=0'));

		$loc = AssetLocation::select(DB::raw('@rownum := @rownum + 1 AS rownum'), 'code', 'description',
					DB::raw('(SELECT COUNT(*) FROM asset_room WHERE asset_location_id = asset_location.id) AS rooms'),
					DB::raw('(SELECT COUNT(*) FROM asset_item WHERE asset_room_id IN (
								SELECT id FROM asset_room WHERE asset_location_id = asset_location.id
							) AND asset_item.status = \'1\') AS assets'),
					'id');

		$datatables = Datatables::of($loc)
			->addColumn('action', function($loc) use($access)
			 	{
					return ($access['update'] == '1' ? '<a href="#" onclick="sunting('.$loc->id.', \'\'); return false;"><i class="ion ion-compose text-light-blue"></i></a>' : '').
						($access['delete'] == '1' ? '<a href="#" onclick="hapus('.$loc->id.', \''.$loc->code.'\'); return false;"><i class="ion ion-trash-a text-red"></i></a>' : '');
				})
			->setRowId('row_{{ $id }}')
			->removeColumn('id');

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	// Location Store
	public function locationStore(Request $request) {
		$validator = Validator::make($request->all(),
		[
			'loc_code' => 'required|unique:asset_location,code',
			'loc_desc' => 'required'
		]);

		if ($validator->fails()):
			return redirect('aset/lokasi')
						->with('error', 'new')
						->withErrors($validator)
						->withInput();
		else:
			$loc = new AssetLocation;
			$loc->code = $request->input('loc_code');
			$loc->description = $request->input('loc_desc');
			$loc->save();

			return redirect('aset/lokasi')->with('success', $request->input('loc_code'));
		endif;
	}

	// Location Update
	public function locationUpdate(Request $request, $id) {
		$validator = Validator::make($request->all(),
		[
			'loc_code' => 'required|unique:asset_location,code,'.$id,
			'loc_desc' => 'required'
		]);

		if ($validator->fails()):
			return redirect('aset/lokasi')
						->with('error', $id)
						->withErrors($validator)
						->withInput();
		else:
			$loc = AssetLocation::find($id);
			$loc->code = $request->input('loc_code');
			$loc->description = $request->input('loc_desc');
			$loc->save();

			return redirect('aset/lokasi')->with('success', $request->input('loc_code'));
		endif;
	}

	// Location Delete
	public function locationDestroy($id) {
		$loc = AssetLocation::find($id);
		$loc->delete();

		return redirect('aset/lokasi')->with('success', 'hapus');
	}


	/**
	 * Display meeting room data refresh every 1 hour.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function display_view()
	{
		$bookings = Lending::select(
					'a.name', 't.name as type_long', 't.short_name as type_short',
					't.id as type_id', 't.code as type_code', 'nup', 'a.users_id',
					'r.id as room_id', 'r.code as room_code', 'r.name as room',
					'routine_cost', 'is_public', 'acquisition', 'a.description',
					'lending.desc', 'a.status', 'lending.lending_ticket',
					'lending.status as lending_status','lending.started_at', 'lending.ended_at',
					'u.name AS user_name', 'lending.id'
				)
				->join('asset_item as a', 'a.id', '=', 'lending.asset_item_id')
				->join('asset_type as t', 'a.asset_type_id', '=', 't.id')
				->leftJoin('asset_room as r', 'a.asset_room_id', '=', 'r.id')
				->leftJoin('users as u', 'a.users_id', '=', 'u.id')
				->where('lending.status', '1')
        ->where('t.id', 2)
				->whereRaw('DATE(lending.ended_at) >= "' . Carbon::now()->format("Y-m-d") . '"')
				->get();

		$events = [];
		foreach ($bookings as $key => $value) {
			$events[] = Calendar::event(
	      empty($value->desc) ? $value->name . ' - No Name' : $value->name . ' - ' . $value->desc,
	      false,
	      new \DateTime($value->started_at),
	      new \DateTime($value->ended_at),
	      $value->lending_ticket
	    );
		}

    $calendar = Calendar::addEvents($events)->setOptions([
			'defaultView' => 'agendaWeek',
			'minTime' => '08:00:00',
			'maxTime' => '18:00:00',
			'height' => 'auto'
		]);
		return view('asset.display', compact('calendar'));
	}

	/**
	 * Display meeting room data refresh every 1 hour.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function display_data(Request $request)
	{
                DB::statement(DB::raw('set @rownum=0'));

		$asset = AssetItem::select(DB::raw('@rownum := @rownum + 1 AS rownum'),
					'asset_item.name', 't.name as type_long', 't.short_name as type_short',
					't.id as type_id', 't.code as type_code', 'nup', 'asset_item.users_id',
					'r.id as room_id', 'r.code as room_code', 'r.name as room',
					'routine_cost', 'is_public', 'acquisition', 'asset_item.description',
					'asset_item.status', 'l.lending_ticket', 'l.status as lending_status','l.started_at', 'l.ended_at',
					'u.name AS user_name', 'asset_item.id')
				->leftJoin('asset_type as t', 'asset_item.asset_type_id', '=', 't.id')
				->leftJoin('asset_room as r', 'asset_item.asset_room_id', '=', 'r.id')
				->leftJoin('users as u', 'asset_item.users_id', '=', 'u.id')
                                ->leftJoin('lending as l', function($q) {
                                    $q->on('l.asset_item_id', '=', 'asset_item.id')
                                        ->where('l.status', '=', '1');
                                })
                                ->where('t.id', 2);

		$datatables = Datatables::of($asset)
                            ->addColumn('status', function ($asset)
				{
                                    if(!empty($asset->lending_ticket) && $asset->lending_status == 1) {
                                        return 0;
                                    }

                                    return 1;
				})
                            ->editColumn('started_at', function ($asset)
                                {
                                        return (!empty($asset->started_at)) ? $this->Date->WaktuIndoShort($asset->started_at) : "-";
                                })
                            ->editColumn('ended_at', function ($asset)
				{
					return (!empty($asset->ended_at)) ? $this->Date->WaktuIndoShort($asset->ended_at) : "-";
				});

		return $datatables->make(true);
	}
}
