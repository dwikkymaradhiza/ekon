<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lending;
use App\Action;
use App\Helper\Date;
use App\AssetItem;
use App\AssetType;
use Response;
use DB;
use Yajra\Datatables\Datatables;
use Auth;
use Validator;
use QrCode;
use Carbon\Carbon;
use PDF;
use Milon\Barcode\DNS1D;

class LendingController extends Controller
{
	const PENDING = '0';
	const APPROVED = '1';
	const REJECTED = '2';
	const RETURNED = '3';

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function __construct()
	{
		// $this->middleware('auth');
		$this->Act = new Action;
		$this->Date = new Date;
	}

	public function index()
	{
		return view('lending.index');
	}

	public function indexLog()
	{
		return view('lending.indexlog');
	}

	// SCAN
	public function scan()
	{
		return view('lending.scan');
	}

	// DATATABLES
	public function getList(Request $request)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$lending = DB::table('lending as l')
			->join('users as u', 'l.users_id', '=', 'u.id')
			->join('asset_item as a', 'l.asset_item_id', '=', 'a.id')
			->select([
				DB::raw('@rownum := @rownum + 1 AS rownum'),
					'l.lending_ticket',
					'l.started_at', 'l.ended_at', 'l.created_at', 'l.status',
					'l.id', 'l.desc',
					'l.asset_item_id', 'a.name as asset_name', 'a.nup',
					'u.name as user_name', 'u.nip', 'u.id as users_id'
				])
			->whereIn('l.status', [self::PENDING, self::APPROVED])
			->whereNull('u.deleted_at')
			->whereNull('a.deleted_at')
			->whereNull('l.deleted_at')
			->whereNotNull('l.lending_ticket')
			->whereNull('taken_at');

			if (Auth::user()->roles_id == 6):
				$lending = $lending->where('a.asset_type_id','=',2 );
			endif;

		$datatables = Datatables::of($lending)
			->addColumn('action', function ($lending)
				{
					$barcode = htmlentities(QrCode::size(218)->generate($lending->lending_ticket));
					$idType = $this->Act->GetField('asset_item','id',$lending->asset_item_id,'asset_type_id');
					$idRoom = $this->Act->GetField('asset_item','id',$lending->asset_item_id,'asset_room_id');
					$room = $this->Act->GetField('asset_room','id',$idRoom,'name');
					$codeRoom = $this->Act->GetField('asset_room','id',$idRoom,'code');
					$type = $this->Act->GetField('asset_type','id',$idType,'name');
					$type_short = $this->Act->GetField('asset_type','id',$idType,'short_name');
					$typeCode = $this->Act->GetField('asset_type','id',$idType,'code');
					if ($lending->status == 0):
						return '<a href="#" data-barcode="'.$barcode.
							'" data-id="'.$lending->id.
							'" data-lending_ticket="'.$lending->lending_ticket.
							'" data-created_at="'.$this->Date->WaktuIndo($lending->created_at).
							'" data-started_at="'.$this->Date->WaktuIndo($lending->started_at).
							'" data-ended_at="'.$this->Date->WaktuIndo($lending->ended_at).
							'" data-asset_name="'.$lending->asset_name.
							'" data-user_name="'.$lending->user_name.
							'" data-nup="'.$lending->nup.
							'" data-nip="'.$lending->nip.
							'" data-user_id="'.$lending->users_id.
							'" data-type="'.($type_short != '' ? $type_short : $type).
							'" data-room="'.$room.
							'" data-type_code="'.$typeCode.
							'" data-code_room="'.$codeRoom.
							'" data-toggle="modal" data-target="#modal_edit"><i class="ion ion-clipboard text-green"></i>
						</a>';
					else:
						return '<a href="#" data-barcode="'.$barcode.
							'" data-id="'.$lending->id.
							'" data-lending_ticket="'.$lending->lending_ticket.
							'" data-created_at="'.$this->Date->WaktuIndo($lending->created_at).
							'" data-started_at="'.$this->Date->WaktuIndo($lending->started_at).
							'" data-ended_at="'.$this->Date->WaktuIndo($lending->ended_at).
							'" data-asset_name="'.$lending->asset_name.
							'" data-user_name="'.$lending->user_name.
							'" data-nup="'.$lending->nup.
							'" data-nip="'.$lending->nip.
							'" data-user_id="'.$lending->users_id.
							'" data-type="'.($type_short != '' ? $type_short : $type).
							'" data-room="'.$room.
							'" data-type_code="'.$typeCode.
							'" data-code_room="'.$codeRoom.
							'"  data-toggle="modal" data-target="#modal_peminjaman"><i class="ion ion-clipboard text-green"></i>
						</a>';
					endif;
				})
			->editColumn('started_at', function ($lending)
				{
					if (Auth::user()->roles_id == 6):
						return $this->Date->WaktuIndoLengkap($lending->started_at);
					else:
						return $this->Date->WaktuIndoShort($lending->started_at);
					endif;

				})
			->editColumn('ended_at', function ($lending)
				{
					if (Auth::user()->roles_id == 6):
						return $this->Date->WaktuIndoLengkap($lending->ended_at);
					else:
						return $this->Date->WaktuIndoShort($lending->ended_at);
					endif;
				})
			->editColumn('created_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->created_at);
				})
			->editColumn('status', function ($lending)
				{
					if (Auth::user()->roles_id == 6):
						return (empty($lending->desc)) ? '-' : $lending->desc;
					else:
						switch ($lending->status):
							case self::APPROVED:
							 if(Auth::user()->roles_id === 6 ):
								return '<span class="label label-warning">Sudah Dipesan</span>';
							 else:
								return '<span class="label label-success">Menunggu Pengambilan</span>';
							 endif;

								break;
							case self::PENDING:
								return '<span class="label label-info">Menunggu Persetujuan</span>';
								break;
							default:
								# code...
								break;
						endswitch;
					endif;
				})
			->removeColumn('nup')
			->removeColumn('id')
			->escapeColumns([]);

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	public function getLendingDetail(Request $request, $id)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$lending = DB::table('lending as l')->join('users as u', 'l.users_id', '=', 'u.id')
			->join('asset_item as a', 'l.asset_item_id', '=', 'a.id')
			->select([
				DB::raw('@rownum := @rownum + 1 AS rownum'),
					'l.lending_ticket',
					'l.started_at',
					'l.ended_at',
					'l.asset_item_id',
					'a.name as asset_name',
					'u.id'
				])
			->where('u.id', '=', $id)
			->where('l.status', '=', '1')
			->whereNull('u.deleted_at')
			->whereNull('a.deleted_at')
			->whereNull('l.deleted_at')
			->whereNotNull('taken_at');

		$datatables = Datatables::of($lending)

			->editColumn('started_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->started_at);
				})
			->editColumn('ended_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->ended_at);
				})
			->removeColumn('id');

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	public function getLog(Request $request)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$lending = DB::table('lending as l')
			->join('users as u', 'l.users_id', '=', 'u.id')
			->join('asset_item as a', 'l.asset_item_id', '=', 'a.id')
			->select([
				DB::raw('@rownum := @rownum + 1 AS rownum'),
					'l.lending_ticket',
					'l.started_at', 'l.ended_at', 'l.created_at', 'l.status',
					'l.id', 'l.asset_item_id', 'a.name as asset_name', 'a.nup',
					'u.name as user_name', 'u.nip', 'u.id as users_id'
				])
			->whereNull('u.deleted_at')
			->whereNull('a.deleted_at')
			->whereNull('l.deleted_at');

			if (Auth::user()->roles_id == 6):
				$lending->whereIn('l.status', ['1','2','3']);
				$lending->where('a.asset_type_id','=','2' );
			else:
				$lending->whereIn('l.status', ['1','2','3']);
			endif;

		$datatables = Datatables::of($lending)
			->addColumn('action', function ($lending)
				{
					$idType = $this->Act->GetField('asset_item','id',$lending->asset_item_id,'asset_type_id');
					$idRoom = $this->Act->GetField('asset_item','id',$lending->asset_item_id,'asset_room_id');
					$room = $this->Act->GetField('asset_room','id',$idRoom,'name');
					$codeRoom = $this->Act->GetField('asset_room','id',$idRoom,'code');
					$type = $this->Act->GetField('asset_type','id',$idType,'name');
					$typeCode = $this->Act->GetField('asset_type','id',$idType,'code');
					$type_short = $this->Act->GetField('asset_type','id',$idType,'short_name');
					$type_short != '' ? ($type = $type_short.' ('.$this->Act->GetField('asset_type','id',$idType,'code').')') : '';
					return '<a href="#" data-id="'.$lending->id.
						'" data-lending_ticket="'.$lending->lending_ticket.
						'" data-created_at="'.$this->Date->WaktuIndo($lending->created_at).
						'" data-started_at="'.$this->Date->WaktuIndo($lending->started_at).
						'" data-ended_at="'.$this->Date->WaktuIndo($lending->ended_at).
						'" data-asset_name="'.$lending->asset_name.
						'" data-user_name="'.$lending->user_name.
						'" data-nup="'.$lending->nup.
						'" data-nip="'.$lending->nip.
						'" data-user_id="'.$lending->users_id.
						'" data-type="'.$type.
						'" data-type_code="'.$typeCode.
						'" data-room="'.$room.
						'" data-code_room="'.$codeRoom.
						'" data-toggle="modal" data-target="#modal_edit"><i class="ion ion-clipboard text-green"></i>
					</a>';
				})
			->editColumn('started_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->started_at);
				})
			->editColumn('ended_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->ended_at);
				})
			->editColumn('created_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->created_at);
				})
			->editColumn('status', function ($lending)
				{
					switch ($lending->status):
						case self::APPROVED:
							return '<span class="label label-success">Disetujui</span>';
							break;
						case self::REJECTED:
							return '<span class="label label-danger">Ditolak</span>';
							break;
						case self::RETURNED:
							return '<span class="label label-info">Telah Dikembalikan</span>';
							break;
						default:
							# code...
							break;
					endswitch;
				})
			->removeColumn('nup')
			->removeColumn('id')
			->escapeColumns([]);

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	/**
	* Show the form for creating a new resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function create_user(Request $request)
	{
		$asset_types = ($request->segment(2) == 'form-pengajuan-rapat' ? [2 => 'Meeting Room'] : AssetType::all()->pluck('name', 'id'));
		return view('lending.formuser', compact('asset_types'));
	}

	/**
	* Show the form for creating a new resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function create()
	{
		return view('lending.form');
	}

	/**
	* Display the specified resource.
	*
	* @param int $id
	* @return \Illuminate\Http\Response
	*/
	public function show(Request $request)
	{
		$data = DB::table('lending as l')
				->join('users as u', 'l.users_id', '=', 'u.id')
				->join('asset_item as a', 'l.asset_item_id', '=', 'a.id')
				->leftJoin('positions as p', 'u.positions_id', '=', 'p.id')
				->select([
					'l.lending_ticket',
					'l.return_ticket',
					'l.started_at',
					'l.ended_at',
					'l.created_at',
					'l.updated_at',
					'l.status',
					'l.id',
					'l.asset_item_id',
					'a.name as asset_name',
					'a.nup',
					'u.name as user_name',
					'u.nip',
					'u.id as users_id'
				])
				->where('lending_ticket',$request->tiket)
				->where('l.status','=','1')
				->first();
		if ($data):
			$idType = $this->Act->GetField('asset_item','id',$data->asset_item_id,'asset_type_id');
			$type = $this->Act->GetField('asset_type','id',$idType,'name');
			$type_short = $this->Act->GetField('asset_type','id',$idType,'short_name');
			$type_short != '' ? ($type = $type_short.' ('.$this->Act->GetField('asset_type','id',$idType,'code').')') : '';
			$idRoom = $this->Act->GetField('asset_item','id',$data->asset_item_id,'asset_room_id');
			$room = $this->Act->GetField('asset_room','id',$idRoom,'name');
			$codeRoom = $this->Act->GetField('asset_room','id',$idRoom,'code');

			$date = $this->Date->WaktuIndo($data->created_at);
			return Response::json(['status'	=> true, 'message' => $data,
									'type'	=> $type, 'created' => $date,
									'room'	=> $room, 'codeRoom' => $codeRoom]);
		else:
			return Response::json(['status' => false, 'message' => 'Tiket Tidak Ditemukan']);
		endif;
	}

	/**
	* Show the form for editing the specified resource.
	*
	* @param int $id
	* @return \Illuminate\Http\Response
	*/
	public function edit($id)
	{
		$this->data = DB::table('lending')->where('id',$id)->first();
		return view('lending.form',get_object_vars($this));
	}

	/**
	* Update the specified resource in storage.
	*
	* @param \Illuminate\Http\Request $request
	* @param int $id
	* @return \Illuminate\Http\Response
	*/
	public function update(Request $request)
	{
		if ($request->status == self::APPROVED):
			if (empty($request->taken_at)):
				$getThis = DB::table('lending')
								->where('id', $request->id)
								->selectRaw('asset_item_id, started_at, ended_at')
								->first();
				DB::table('lending')->where('id', $request->id)->update(['status' => $request->status]);
				DB::table('asset_item')->where('id', $getThis->asset_item_id)->update(['status' => '0']);
				$others = DB::table('lending')
								->whereRaw('id != '.$request->id.' AND asset_item_id = '.$getThis->asset_item_id.' AND `status` = \'0\' AND
									((started_at <= \''.$getThis->started_at.'\' && \''.$getThis->started_at.'\' <= ended_at) || (started_at <= \''.$getThis->ended_at.'\' && \''.$getThis->ended_at.'\' <= ended_at) || (\''.$getThis->started_at.'\' <= started_at && started_at <= \''.$getThis->ended_at.'\') || (\''.$getThis->started_at.'\' <= ended_at && ended_at <= \''.$getThis->ended_at.'\'))')
								->pluck('id');
				DB::table('lending')
						->whereIn('id', $others)
						->update(['status' => '2']);
				return Response::json(['status' => true, 'message' => 'Data Berhasil Disimpan '.implode(', ', $others->toArray())]);
			else:
				DB::table('lending')->where('id', $request->id)->update(['taken_at' => Date('Y-m-d')]);
				return Response::json(['status' => true, 'message' => 'Data Berhasil Disimpan']);
			endif;
		elseif ($request->status == self::REJECTED):
			$update = DB::table('lending')->where('id', $request->id)->update(['status' => $request->status]);
			return Response::json(['status' => true, 'message' => 'Data Berhasil Disimpan']);
		endif;
	}

	public function verify(Request $request)
	{
		DB::table('lending')->where('id', $request->id)->update(['taken_at' => Date('Y-m-d')]);
		return Response::json(['status' => true, 'message' => 'Data Berhasil Disimpan']);
	}


	/**
	* Remove the specified resource from storage.
	*
	* @param int $id
	* @return \Illuminate\Http\Response
	*/
	public function destroy($id)
	{
		//
	}

	/**
	 * Download pdf.
	 *
	 * @param int $id
	 * @return PDF
	 */
	public function download($id) {
		$lending = Lending::where('lending.id', $id)
					->join('users', 'users.id', '=', 'lending.users_id')
					->join('positions', 'users.positions_id', '=', 'positions.id')
					->join('asset_item', 'lending.asset_item_id', '=', 'asset_item.id')
					->join('asset_type', 'asset_item.asset_type_id', '=', 'asset_type.id')
					->join('asset_room', 'asset_item.asset_room_id', '=', 'asset_room.id')
					->select(
							'asset_item.*',
							'asset_type.name AS category',
							'asset_room.name AS room_name',
							'lending.lending_ticket',
							'users.nip',
							'users.name as user_name',
							'positions.position'
						)
					->firstOrFail()
					->toArray();

				$nowDate = date("Y-m-d");

				$lending['day'] = $this->convertDayMonth("D", $nowDate);
				$lending['month'] = $this->convertDayMonth("M", $nowDate);
				$lending['date'] = $this->terbilang(date('d', strtotime($nowDate)));
				$lending['year'] = $this->terbilang(date('Y', strtotime($nowDate)));

		$pdf = PDF::loadView('pdf.lending', $lending);
		return $pdf->stream('receipt-' . date('dmY') . '.pdf');
	}

	public function terbilang($bilangan) {

		$angka = array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
			'0', '0', '0', '0', '0', '0');
		$kata = array('', 'satu', 'dua', 'tiga', 'empat', 'lima',
			'enam', 'tujuh', 'delapan', 'sembilan');
		$tingkat = array('', 'ribu', 'juta', 'milyar', 'triliun');

		$panjang_bilangan = strlen($bilangan);

		/* pengujian panjang bilangan */
		if ($panjang_bilangan > 15) {
			$kalimat = "Diluar Batas";
			return $kalimat;
		}

		/* mengambil angka-angka yang ada dalam bilangan,
		  dimasukkan ke dalam array */
		for ($i = 1; $i <= $panjang_bilangan; $i++) {
			$angka[$i] = substr($bilangan, -($i), 1);
		}

		$i = 1;
		$j = 0;
		$kalimat = "";


		/* mulai proses iterasi terhadap array angka */
		while ($i <= $panjang_bilangan) {

			$subkalimat = "";
			$kata1 = "";
			$kata2 = "";
			$kata3 = "";

			/* untuk ratusan */
			if ($angka[$i + 2] != "0") {
				if ($angka[$i + 2] == "1") {
					$kata1 = "seratus";
				} else {
					$kata1 = $kata[$angka[$i + 2]] . " ratus";
				}
			}

			/* untuk puluhan atau belasan */
			if ($angka[$i + 1] != "0") {
				if ($angka[$i + 1] == "1") {
					if ($angka[$i] == "0") {
						$kata2 = "sepuluh";
					} elseif ($angka[$i] == "1") {
						$kata2 = "sebelas";
					} else {
						$kata2 = $kata[$angka[$i]] . " belas";
					}
				} else {
					$kata2 = $kata[$angka[$i + 1]] . " puluh";
				}
			}

			/* untuk satuan */
			if ($angka[$i] != "0") {
				if ($angka[$i + 1] != "1") {
					$kata3 = $kata[$angka[$i]];
				}
			}

			/* pengujian angka apakah tidak nol semua,
			  lalu ditambahkan tingkat */
			if (($angka[$i] != "0") OR ( $angka[$i + 1] != "0") OR ( $angka[$i + 2] != "0")) {
				$subkalimat = "$kata1 $kata2 $kata3 " . $tingkat[$j] . " ";
			}

			/* gabungkan variabe sub kalimat (untuk satu blok 3 angka)
			  ke variabel kalimat */
			$kalimat = $subkalimat . $kalimat;
			$i = $i + 3;
			$j = $j + 1;
		}

		/* mengganti satu ribu jadi seribu jika diperlukan */
		if (($angka[5] == "0") AND ( $angka[6] == "0")) {
			$kalimat = str_replace("satu ribu", "seribu", $kalimat);
		}

		return trim($kalimat);
	}

	public function convertDayMonth($format, $tanggal = "now", $bahasa = "id") {
		$en = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Jan", "Feb",
			"Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

		$id = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",
			"Januari", "Pebruari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September",
			"Oktober", "Nopember", "Desember");

		// mengganti kata yang berada pada array en dengan array id, fr (default id)
		$converted = str_replace($en, $$bahasa, date($format, strtotime($tanggal)));

		return $converted;
	}

}
