<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Lending;
use App\Action;
use App\AssetItem;
use App\AssetRoom;
use App\Service;
use App\AuthPegawai;
use App\Moving;
use Response;
use DB;
use Yajra\Datatables\Datatables;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Helper\Date;
use QrCode;
use Log;

class UserController extends Controller
{
	public function __construct()
	{
		$this->Date = new Date;
	}

	const PENDING  = '0';
	const APPROVED = '1';
	const REJECTED = '2';
	const RETURNED = '3';

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('user.index', ['roles' => Role::all()]);
	}

	// Index API
	public function indexList(Request $request)
	{
		DB::statement(DB::raw('set @rownum=0'));

		$users = User::select(DB::raw('@rownum := @rownum + 1 AS rownum'), 'name', 'email', 'nip', 'p.position', 'r.role', 'users.id', 'users.roles_id', 'users.simpeg_id')
					->leftJoin('roles as r', 'users.roles_id', '=', 'r.id')
					->leftJoin('positions as p', 'positions_id', '=', 'p.id');

		$datatables = Datatables::of($users)
			->addColumn('action', function($users)
				{
					$act = '<a href="#" onclick="detil('.$users->id.', \'\'); return false;"><i class="ion ion-clipboard text-green"></i></a>';
					if ($users->simpeg_id == 0 AND Auth::user()->roles_id < $users->roles_id):
						if (app('access')['update'] == '1'):
							$act = $act.'<a href="#" onclick="update('.$users->id.', \'\'); return false;"><i class="ion ion-compose text-light-blue"></i></a>';
						endif;
						if (app('access')['delete'] == '1'):
							$act = $act.'<a href="#" onclick="hapus('.$users->id.', \''.$users->name.'\'); return false;"><i class="ion ion-trash-a text-red"></i></a>';
						endif;
					endif;
					return $act;
				})
			->removeColumn('id');

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	// Detail API
	public function indexShow(Request $request, $id)
	{
		$data = User::select('users.*', 'p.*', 'r.role')
					->leftJoin('roles as r', 'users.roles_id', '=', 'r.id')
					->leftJoin('positions as p', 'users.positions_id', '=', 'p.id')
					->where('users.id', $id)
					->first();
		if ($data):
			return response($data->toJson(), 200)->header('Content-Type', 'application/json');
		else:
			return response([], 404)->header('Content-Type', 'application/json');
		endif;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$req = $request->all();
		$validator = Validator::make($req,
		[
			'name' 		=> 'required',
			'email' 	=> 'required|email|unique:users,email',
			'password'	=> 'required|min:6',
			'roles_id'	=> 'required'
		]);

		if ($validator->fails()):
			return redirect('pengguna')
						->with('errornew', 'new')
						->withErrors($validator)
						->withInput();
		else:
			$user = new User;
			$user->name = $req['name'];
			$user->email = $req['email'];
			$user->password = bcrypt($req['password']);
			$user->simpeg_id = $user->nip = '0';
			$user->roles_id = $req['roles_id'];
			$user->save();

			return redirect('pengguna')->with('success', $req['name']);
		endif;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Request $request, $id)
	{
		$req = $request->all();
		$valid = [
				'name' 		=> 'required',
				'email' 	=> 'required|email|unique:users,email,'.$id,
				'roles_id'	=> 'required'
			];
		if ($req['password'] != ''):
			$valid['password'] = 'min:6';
		endif;
		$validator = Validator::make($req, $valid);

		if ($validator->fails()):
			return redirect('pengguna')
						->with('errorupdate', $id)
						->withErrors($validator)
						->withInput();
		else:
			$user = User::find($id);
			$user->name = $req['name'];
			$user->email = $req['email'];
			if ($user->password != ''):
				$user->password = bcrypt($req['password']);
			endif;
			$user->roles_id = $req['roles_id'];
			$user->save();

			return redirect('pengguna')->with('success', $req['name']);
		endif;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(),
		[
			'role_name' => 'required'
		]);

		if ($validator->fails()):
			return redirect('pengguna')
				->with('userid', $id)
				->withErrors($validator)
				->withInput();
		else:
			$user = User::find($id);
			$user->roles_id = $request->input('role_name');
			$user->save();

			return redirect('pengguna')->with('success', User::find($id)->name);
		endif;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);
		$name = $user->name;
		$user->delete();

		return redirect('pengguna')->with('success', $name)->with('erase', '1');
	}

	public function get_user_assets(Request $request)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$lending = DB::table('lending')
			->join('users', 'lending.users_id', '=', 'users.id')
			->join('asset_item as a', 'lending.asset_item_id', '=', 'a.id')
			->join('asset_type', 'a.asset_type_id', '=', 'asset_type.id')
			->leftJoin('asset_room', 'a.asset_room_id', '=', 'asset_room.id')
			->leftJoin('positions as p', 'users.positions_id', '=', 'p.id')
			->select([
				DB::raw('@rownum := @rownum + 1 AS rownum'),
					'lending.id',
					'lending.asset_item_id',
					'a.nup as nuk',
					'a.name as asset_name',
					'asset_type.name as category',
					'lending.return_ticket',
					'lending.lending_ticket',
					'lending.taken_at',
					'lending.started_at',
					'lending.created_at',
					'lending.ended_at',
					'lending.status',
					'users.name as user_name',
					'users.nip',
					'p.position',
					'users.id as users_id',
					'asset_room.name as room_name',
					'asset_room.description as room_desc',
					'asset_type.short_name as cat_short',
					'asset_type.code as cat_code',
					'asset_room.code as room_code',
					'asset_room.id as room_id'
				])
			->whereIn('lending.status', [self::PENDING, self::APPROVED])
			->whereNull('a.users_id')
			->where('users.id', '=', Auth::user()->id);

		$datatables = Datatables::of($lending)
			->addColumn('action', function ($lending)
				{
					$service = Service::where('users_id', Auth::user()->id)
										->where('asset_item_id', $lending->asset_item_id)
										->whereIn('status', ['0', '1', '2'])
										->first();

					$moving = Moving::where('users_id', Auth::user()->id)
										->where('asset_item_id', $lending->asset_item_id)
										->where('status', '0')
										->first();

					if (!empty($lending->return_ticket)):
						$QRCode = htmlentities(QrCode::size(218)->generate($lending->return_ticket));
						return '<a href="#" '
							. 'data-id="'.$lending->id.'" '
							. 'data-position="'. $lending->position .'" '
							. 'data-lending_ticket="'.$lending->lending_ticket.'" '
							. 'data-return_ticket="'.$lending->return_ticket.'" '
							. 'data-created_at="'.$this->Date->WaktuIndo($lending->created_at).'" '
							. 'data-started_at="'.$this->Date->WaktuIndo($lending->started_at).'" '
							. 'data-ended_at="'.$this->Date->WaktuIndo($lending->ended_at).'" '
							. 'data-asset_name="'.$lending->asset_name.'" '
							. 'data-user_name="'.$lending->user_name.'" '
							. 'data-room_desc="'.$lending->room_desc.'" '
							. 'data-room_name="'.$lending->room_name.' ('.$lending->room_code.')" '
							. 'data-nup="'.$lending->nuk.'" '
							. 'data-nip="'.$lending->nip.'" '
							. 'data-user_id="'.$lending->users_id.'" '
							. 'data-type="'.($lending->cat_short ? $lending->cat_short.' ('.$lending->cat_code.')' : $lending->category).'" '
							. 'data-qrcode="'. $QRCode .'" '
							. 'data-toggle="modal" '
							. 'data-target="#taken-modal" '
							. 'class="label label-info">Proses Pengembalian</a>';
					endif;

					$LendQRCode = htmlentities(QrCode::size(218)->generate($lending->lending_ticket));

					if (!empty($service)):
						switch ($service->status):
							case '1':
								return '<span class="label label-warning">Pengecekan Keluhan</span>';
								break;
							case '2':
								return '<span class="label label-success">Laporan Awal</span>';
								break;
							case '3':
								return '<span class="label label-info">Tertangani</span>';
								break;
							case '4':
								return '<span class="label label-red">Ditolak</span>';
								break;
							default:
								return '<span class="label label-info">Proses Helpdesk</span>';
								break;
						endswitch;
					endif;

					if (!empty($moving)):
						return '<span class="label label-info">Proses Pemindahan</span>';
					endif;

					if ($lending->status == self::PENDING):
						return '&nbsp;';
					endif;

					return '<div class="btn-group">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Tiket
								</button>
								<ul class="ticket-event dropdown-menu pull-right">'.
									(($lending->status == self::APPROVED AND $lending->taken_at == '') ? '
									<li><a data-id="'.$lending->id.
										'" data-position="'.$lending->position.
										'" data-lending_ticket="'.$lending->lending_ticket.
										'" data-created_at="'.$this->Date->WaktuIndo($lending->created_at).
										'" data-started_at="'.$this->Date->WaktuIndo($lending->started_at).
										'" data-ended_at="'.$this->Date->WaktuIndo($lending->ended_at).
										'" data-asset_name="'.$lending->asset_name.
										'" data-user_name="'.$lending->user_name.
										'" data-room_desc="'.$lending->room_desc.
										'" data-room_name="'.$lending->room_name.' ('.$lending->room_code.')'.
										'" data-nup="'.$lending->nuk.
										'" data-nip="'.$lending->nip.
										'" data-user_id="'.$lending->users_id.
										'" data-type="'.($lending->cat_short ? $lending->cat_short.' ('.$lending->cat_code.')' : $lending->category).'" '.
										'" data-lendqrcode="'.$LendQRCode.
										'" data-toggle="modal" data-target="#taken-modal" href="#" class="taken-ticket">
										<i class="ion ion-qr-scanner"></i> Pengambilan</a>
									</li>' : '
									<li><a data-toggle="modal" data-asset_name="'.$lending->asset_name.
										'" lending="'.$lending->id.
										'" nuk="'.$lending->nuk.
										'" data-target="#helpdesk-modal" href="#" class="helpdesk-ticket"><i class="ion ion-settings"></i> Helpdesk</a>
									</li>
									<li><a data-toggle="modal" data-asset_name="'.$lending->asset_name.
										'" lending="'.$lending->id.
										'" nuk="'.$lending->nuk.
										'" data-room="'.$lending->room_id.
										'" data-target="#move-modal" href="#" class="moving-ticket"><i class="ion ion-arrow-move"></i> Pemindahan</a>
									</li>
									<li><a data-toggle="modal" data-asset_name="'.$lending->asset_name.
										'" lending="'.$lending->id.
										'" nuk="'.$lending->nuk.
										'" data-target="#return-modal" href="#" class="return-ticket"><i class="ion ion-android-sync"></i> Pengembalian</a>
									</li>').
								'</ul>
						</div>';
				})
			->addColumn('started_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->started_at);
				})
			->addColumn('status', function ($lending)
				{
					if ($lending->status == self::PENDING):
						return '<span class="label label-info">Menunggu Persetujuan</span>';
					endif;

					if ($lending->status == self::APPROVED && !empty($lending->taken_at)):
						return '<span class="label label-success">Telah Diterima</span>';
					endif;

					if ($lending->status == self::APPROVED):
						return '<span class="label label-success">Telah Disetujui</span>';
					endif;
				})
			 ->addColumn('ended_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->ended_at);
				})
			->addColumn('taken_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->taken_at);
				})
			->escapeColumns([]);

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	public function get_user_fixed_assets(Request $request)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$lending = DB::table('asset_item')
					->join('asset_type', 'asset_item.asset_type_id', '=', 'asset_type.id')
					->leftJoin('asset_room', 'asset_item.asset_room_id', '=', 'asset_room.id')
					->leftJoin('users', 'users.id', '=', 'asset_item.users_id')
					->leftJoin('positions', 'users.positions_id', '=', 'positions.id')
					->leftJoin('lending', 'asset_item.id', '=', 'lending.asset_item_id')
					->select([
						DB::raw('@rownum := @rownum + 1 AS rownum'),
							'asset_item.id as id',
							'asset_item.nup as nuk',
							'asset_item.name as asset_name',
							'asset_type.name as category',
							'asset_room.name as room_name',
							'asset_room.description as room_desc',
							'lending.return_ticket',
							'lending.lending_ticket',
							'lending.taken_at',
							'lending.started_at',
							'lending.created_at',
							'lending.ended_at',
							'lending.status',
							'users.name as user_name',
							'users.nip',
							'users.id as users_id',
							'positions.position',
							'lending.return_ticket'
						])
					->whereNull('lending.lending_ticket')
					->where('asset_item.users_id', '=', Auth::user()->id);

		$datatables = Datatables::of($lending)
			->addColumn('action', function ($lending)
				{
					// If in return progress
					if (!empty($lending->return_ticket)):
						$QRCode = htmlentities(QrCode::size(218)->generate($lending->return_ticket));

						return '<a href="#" '
							. 'data-id="'.$lending->id.'" '
							. 'data-position="'. $lending->position .'" '
							. 'data-lending_ticket="'.$lending->lending_ticket.'" '
							. 'data-return_ticket="'.$lending->return_ticket.'" '
							. 'data-created_at="'.$this->Date->WaktuIndo($lending->created_at).'" '
							. 'data-started_at="'.$this->Date->WaktuIndo($lending->started_at).'" '
							. 'data-ended_at="'.$this->Date->WaktuIndo($lending->ended_at).'" '
							. 'data-asset_name="'.$lending->asset_name.'" '
							. 'data-user_name="'.$lending->user_name.'" '
							. 'data-room_desc="'.$lending->room_desc.'" '
							. 'data-room_name="'.$lending->room_name.'" '
							. 'data-nup="'.$lending->nuk.'" '
							. 'data-nip="'.$lending->nip.'" '
							. 'data-user_id="'.$lending->users_id.'" '
							. 'data-type="'.$lending->category.'" '
							. 'data-qrcode="'. $QRCode .'" '
							. 'data-toggle="modal" '
							. 'data-target="#taken-modal" '
							. 'class="label label-info">Proses Pengembalian</a>';
						endif;

						//If in helpdesk progress
						$service = Service::where('users_id', Auth::user()->id)
										->where('asset_item_id', $lending->id)
										->whereIn('status', ['0', '1', '2'])
										->first();

						if (!empty($service)):
							switch ($service->status):
								case '1':
									return '<span class="label label-warning">Pengecekan Keluhan</span>';
									break;
								case '2':
									return '<span class="label label-success">Laporan Awal</span>';
									break;
								case '3':
									return '<span class="label label-info">Tertangani</span>';
									break;
								case '4':
									return '<span class="label label-red">Ditolak</span>';
									break;
								default:
									return '<span class="label label-info">Proses Helpdesk</span>';
									break;
							endswitch;
						endif;

						return '<div class="btn-group">
									<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											Tiket
									</button>
									<ul class="ticket-event dropdown-menu pull-right">
											<li><a data-toggle="modal" data-asset_name="'.$lending->asset_name.
													'" lending="'.$lending->id.
													'" nuk="'.$lending->nuk.
													'" data-target="#helpdesk-modal"  href="#" class="helpdesk-ticket"><i class="ion ion-settings"></i> Helpdesk</a>
											</li>
											<li><a data-toggle="modal" data-asset_name="'.$lending->asset_name.
													'" lending="'.$lending->id.
													'" nuk="'.$lending->nuk.
													'" data-target="#return-modal" href="#" class="return-ticket"><i class="ion ion-android-sync"></i> Pengembalian</a>
											</li>
									</ul>
								</div>';
				})
			->escapeColumns([]);

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	public function get_available_assets(Request $request, $type = null)
	{
		$from = $to = Carbon::now();
		if ($request->get('from_data') != ''):
			$from = $request->get('from_data');
			$from = explode(' ', $from);
			$date = explode('/', $from[1]);
			$date = $date[2].'-'.$date[1].'-'.$date[0];
			$from = $date.' '.$from[2].':00';
		endif;
		if ($request->get('to_data') != ''):
			$to = $request->get('to_data');
			$to = explode(' ', $to);
			$date = explode('/', $to[1]);
			$date = $date[2].'-'.$date[1].'-'.$date[0];
			$to = $date.' '.$to[2].':00';
		endif;
		DB::statement(DB::raw('set @rownum=0'));
		$assets = DB::table('asset_item as a')
				->select([
						DB::raw('@rownum := @rownum + 1 AS rownum'),
						'a.nup as nuk',
						'a.id',
						'a.users_id',
						'a.acquisition',
						'a.name as asset_name',
						DB::raw('IF((l.started_at <= \''.$from.'\' && \''.$from.'\' <= l.ended_at) || (l.started_at <= \''.$to.'\' && \''.$to.'\' <= l.ended_at) || (\''.$from.'\' <= l.started_at && l.started_at <= \''.$to.'\') || (\''.$from.'\' <= l.ended_at && l.ended_at <= \''.$to.'\'), \'0\', \'1\') as status'),
						'a.description',
						't.code as cat_code',
						't.name as category',
						'r.name as asset_room_name',
						'r.code as room_code',
						't.short_name',
						// 'l.started_at as lend_start',
						// 'l.ended_at as lend_end'
					])
				->leftJoin('asset_type as t', 'asset_type_id', '=', 't.id')
				->join('asset_room as r', 'a.asset_room_id', '=', 'r.id')
				->leftJoin(DB::raw('(SELECT * FROM lending WHERE id IN (SELECT MAX(id) AS id FROM lending WHERE status = \'1\' AND ((started_at <= \''.$from.'\' && \''.$from.'\' <= ended_at) || (started_at <= \''.$to.'\' && \''.$to.'\' <= ended_at) || (\''.$from.'\' <= started_at && started_at <= \''.$to.'\') || (\''.$from.'\' <= ended_at && ended_at <= \''.$to.'\')) GROUP BY asset_item_id ORDER BY MAX(created_at), MAX(id))) as l'), 'l.asset_item_id', '=', 'a.id')
				->where('a.is_public', '0')
				->whereNull('a.users_id')
				->whereNull('a.deleted_at');

		if (!empty($type)):
			$assets = $assets->where('asset_type_id', $type);
		endif;

		$datatables = Datatables::of($assets)
				->addColumn('action', function ($assets)
					{
						$detail =  '<a class="item-history" data-toggle="modal" data-target="#aset-detil" ';
						$detail .= 'data-category="'.($assets->short_name != '' ? ($assets->short_name.' ('.$assets->cat_code.')') : $asset->category).'" ';
						$detail .= 'data-asset="'.$assets->asset_name.'" ';
						$detail .= 'data-room="'.$assets->asset_room_name.' ('.$assets->room_code.')" ';
						$detail .= 'data-acquisition="'.$this->Date->WaktuIndo($assets->acquisition).'" ';
						$detail .= 'data-desc="'.($assets->description != '' ? $assets->description : '&#8211;').'" ';
						$detail .= 'data-nup="'.$assets->nuk.'" ';
						$detail .= 'data-id="'.$assets->id.'" ';
						$detail .= 'href="#" onClick="asetDetail(event)"><i class="glyphicon glyphicon-search"></i></a>';
						return  $detail;
					})
				->editColumn('status', function ($assets)
					{
						if ($assets->status == 0 || !empty($assets->users_id)):
							return '<span class="label label-danger">Tidak Tersedia</span>';
						endif;

						if ($assets->status == 1):
							return '<span class="label label-success">Tersedia</span>';
						endif;
					})
				->setRowAttr(['id' => 'nup_{{ $nuk }}', 'class' => '{{ $status == 0 ? \'restrict\' : \'selectable\' }}'])
				->escapeColumns([]);

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	public function store_lending(Request $request)
	{
		$req = $request->all();
		if ($req['date_from'] != ''):
			$date = explode(', ', $req['date_from']);
			$date = explode(' ', $date[1]);
			$time_start = $date[1].':00';
			$date = $date[0];
			$date = explode('/', $date);
			$req['date_from'] = $date[2].'-'.$date[1].'-'.$date[0];
		endif;
		if ($req['date_to'] != ''):
			$date = explode(', ', $req['date_to']);
			$date = explode(' ', $date[1]);
			$time_end = $date[1].':00';
			$date = $date[0];
			$date = explode('/', $date);
			$req['date_to'] = $date[2].'-'.$date[1].'-'.$date[0];
		endif;

		$validator = Validator::make($req, [
			'date_from'		 => 'required|date|before_or_equal:date_to',
			'date_to'		 => 'required|date|after_or_equal:date_from',
			'asset_category' => 'required',
			'nup'			 => 'required'
		]);

		if ($validator->fails()):
			return redirect()->back()->withInput()->withErrors($validator);
		endif;

		$asset = AssetItem::join('asset_room', 'asset_item.asset_room_id', '=', 'asset_room.id')
									->where('asset_item.nup', $request->get('nup'))
									// ->where('asset_item.status', '1')
									->select('asset_item.*', 'asset_room.description AS room_desc', 'asset_room.name AS room_name')
									->first();

				if (empty($asset)):
					return redirect()->back()->withInput(['date_from', 'date_to'])->withErrors(['Pengajuan peminjaman gagal. Aset ini tidak tersedia atau tidak ada ruangan.']);
				endif;

		$lending = Lending::where('users_id', Auth::user()->id)->where('asset_item_id', $asset->id)->where('status', '0')->first();
		if (!empty($lending)):
			return redirect()->back()->withInput(['date_from', 'date_to'])->withErrors(['Pengajuan peminjaman anda untuk aset ini sedang dalam proses.']);
		endif;

		$lending = new Lending;
		$lending->lending_ticket = "LND-".date('Ymd').rand(0,999);
		$lending->asset_item_id = $asset->id;
		$lending->users_id = Auth::user()->id;
		$lending->started_at = $req['date_from'].' '.$time_start;
		$lending->ended_at = $req['date_to'].' '.$time_end;
		$lending->status = '0';

		if (isset($req['desc'])) {
			$lending->desc = $req['desc'];
		}

		$lending->save();

		$roomName = !empty($asset->room_name) ? $asset->room_name : $asset->room_desc;
		return redirect()->route('user.dashboard')->with('stored', '<p>Pengajuan pinjaman untuk aset <strong>'.$asset->name.'</strong> telah kami terima.</p><p>Setelah proses disetujui Anda dapat mengambil aset di lokasi <strong>'.$roomName.'</strong>.</p>');
	}

	public function store_moving(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'nuk'   => 'required',
			'room_id' => 'required'
		]);

		if($validator->fails()) {
				return redirect()->back()->withInput(['date_from', 'date_to'])->withErrors($validator);
		}

		$asset = AssetItem::where('nup', $request->get('nuk'))->firstOrFail();

		$moving = new Moving;
		$moving->ticket = "MOV-".date('Ymd').rand(0,999);
		$moving->users_id = Auth::user()->id;
		$moving->asset_item_id = $asset->id;
		$moving->init_position = $asset->asset_room_id;
		$moving->new_position = $request->get('room_id');
		$moving->save();

				return redirect()->route('user.dashboard')->with('stored', 'Pengajuan Pemindahan untuk aset <strong>'.$asset->name.'</strong> telah kami terima.');
	}

	public function store_return(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'lending_id' => 'required',
			'nuk'		 => 'required'
		]);

		if($validator->fails()) {
				return redirect()->back()->withInput(['date_from', 'date_to'])->withErrors($validator);
		}

		$asset = AssetItem::where('nup', $request->get('nuk'))->firstOrFail();

		// Jika asset tetap, Create new lending
		if(!empty($asset->users_id)) {
			$lending = new Lending;
			$lending->return_ticket = "RET-".date('Ymd').rand(0,999);
			$lending->asset_item_id = $asset->id;
			$lending->users_id = Auth::user()->id;
			$lending->started_at = date('Y-m-d');
			$lending->ended_at = date('Y-m-d');
			$lending->status = '0';
			$lending->save();
		} else {
			$lending = Lending::find($request->get('lending_id'));
			$lending->return_ticket = 'RET-'.date('Ymd').rand(0,999);
			$lending->save();
		}

		return redirect()->route('user.dashboard')->with('stored', 'Pengajuan Pengembalian untuk aset <strong>'.$asset->name.'</strong> telah kami terima.');
	}

	public function store_service(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'lending_id' => 'required',
			'nuk'		 => 'required',
			'message'	 => 'required'
		]);

		if ($validator->fails()):
			// return Response::json($request->all());
			return redirect()->back()->withInput()->with('error', 'error')->withErrors($validator);
		endif;

		$asset = AssetItem::where('nup', $request->get('nuk'))
				// ->where('status', '1')
				->firstOrFail();

		$service = new Service;
		$service->ticket = "SRV-".date('Ymd').rand(0,999);
		$service->users_id = Auth::user()->id;
		$service->asset_item_id = $asset->id;
		$service->cost = 0;
		$service->complaint = $request->get('message');
		$service->save();

		return redirect()->route('user.dashboard')->with('stored', 'Pengajuan Helpdesk untuk aset <strong>'.$asset->name.'</strong> telah kami terima.');
	}

	public function user_log()
	{
		return view('user.log');
	}

	public function get_user_log(Request $request)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$lending = DB::table('lending')
						->join('users', 'lending.users_id', '=', 'users.id')
						->join('asset_item', 'lending.asset_item_id', '=', 'asset_item.id')
						->join('asset_type', 'asset_item.asset_type_id', '=', 'asset_type.id')
						->select([
							DB::raw('@rownum := @rownum + 1 AS rownum'),
								'lending.id',
								'asset_item.nup as nuk',
								'asset_item.name as asset_name',
								'asset_type.name as category',
								'lending.started_at',
								'lending.ended_at',
								'lending.taken_at',
								'lending.status'
							])
						->whereIn('lending.status', [self::REJECTED, self::RETURNED])
						->where('lending.users_id', '=', Auth::user()->id);

		$datatables = Datatables::of($lending)
						->addColumn('started_at', function ($lending)
							{
								return $this->Date->WaktuIndoShort($lending->started_at);
							})
						->addColumn('ended_at', function ($lending)
							{
								return $this->Date->WaktuIndoShort($lending->ended_at);
							})
						->addColumn('status', function ($lending)
							{
								if ($lending->status == self::REJECTED):
									return '<span class="text-danger">Ditolak</span>';
								endif;

								if ($lending->status == self::RETURNED):
									return '<span class="text-success">Telah Dikembalikan</span>';
								endif;
							})
						->escapeColumns([]);

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	public function get_asset_history($id)
	{
		$asset = AssetItem::findOrFail($id);

		DB::statement(DB::raw('set @rownum=0'));
		$service_history = DB::table('service')
							->selectRaw(
								'@rownum := @rownum + 1 AS rownum,
								info,
								DATE_FORMAT(done_at, "%e %b %Y") as done_at'
							)
							->whereNotNull('info')
							->whereNotNull('done_at')
							->where('status', '3')
							->where('asset_item_id', $id)
							->get()
							->toArray();

		$lending = Lending::leftJoin('users', 'lending.users_id', '=', 'users.id')
							->where('lending.asset_item_id', $id)
							->where('lending.status', '1')
							->select('users.name AS username')
							->first();

		$asset->history = $service_history;
		$asset->state = ($asset->status == '1') ? "Status <span class='text-success'>Tersedia</span>" : "Status <span class='text-danger'>Tidak tersedia</span>";

		if(!empty($lending)) {
			$asset->state = "Status <span class='text-danger'>Terpakai oleh {$lending->username}</span>";
		}

		return response()->json($asset);
	}

	public function autocomplete(Request $request)
	{
		try {
			$pegawai = AuthPegawai::searchPeg($request->get('query'))->toArray();
			array_push($pegawai, ['name' => $request->get('query'), 'nip' => '']);
			return response()->json($pegawai);
		} catch (\Exception $ex) {
			Log::error($ex->getMessage());
		}
	}
}
