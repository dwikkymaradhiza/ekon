<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Users;
use App\Address;
use App\Skills;
use App\Educations;
use App\Works;
use App\Orgs;
use App\Applicants;
use Barryvdh\DomPDF\Facade as PDF;
// use Spipu\Html2Pdf\Html2Pdf;

class CVController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$cv = DB::table('cv')->selectRaw('id, name, CONCAT(\''.url('images/cv').'/\', file, \'.jpg\') AS thumbnail')->get();
		return response()->json(compact('cv'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function html($id)
	{
		$user	= Auth::user()->id;
		$lang	= Auth::user()->langs;
		$theme	= DB::table('cv')->where('id', $id)->pluck('file');
		$datas	= [
			'user'		=> Auth::user(),
			'address'	=> Address::getAddress($user),
			'skills'	=> Skills::getSkills($user, $lang),
			'eduset'	=> Educations::getEduSet($user, $lang),
			'workset'	=> Works::getWorkSet($user, $lang),
			'orgset'	=> Orgs::getOrgSet($user),
			'employee'	=> Applicants::where('users_id', $user)->first()
		];
		return view('cv.'.$theme[0], $datas);
	}
	public function show(Request $request, $id)
	{
		$html = $this->html($id);
		$name = '/cv_temp/cv_'.$request->userid.'_'.$id.'.pdf';
		$file = PDF::loadHTML($html)->save(public_path().$name);
		if ($file):
			return response()->json(['cv' => url().$name]);
		endif;
		return response()->json(['error' => 400]);
	}
	public function download($id)
	{
		$html = $this->html($id);
		return PDF::loadHTML($html)->stream('cv.pdf');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
