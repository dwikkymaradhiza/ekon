<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lending;
use App\Action;
use App\Helper\Date;
use Response;
use DB;
use Auth;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use Milon\Barcode\DNS1D;

class ReturnController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function __construct()
	{
		// $this->middleware('auth');
		$this->Act = new Action;
		$this->Date = new Date;
	}

	public function index()
	{
		return view('return.index');
	}

	public function indexLog()
	{
		return view('return.indexlog');
	}

	// SCAN
	public function scan()
	{
		return view('return.scan');
	}

	// DATATABLES
	public function getList(Request $request)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$lending = DB::table('lending as l')->join('users as u', 'l.users_id', '=', 'u.id')
			->join('asset_item as a', 'l.asset_item_id', '=', 'a.id')
			->select([
				DB::raw('@rownum := @rownum + 1 AS rownum'),
					'l.lending_ticket',
					'l.return_ticket',
					'l.started_at',
					'l.ended_at',
					'l.created_at',
					'l.updated_at',
					'l.id',
					'l.asset_item_id',
					'a.name as asset_name',
					'a.nup',
					'u.name as user_name',
					'u.nip',
					'u.id as users_id'
				])
				->where('l.status', '=', '1')
				->whereNotNull('return_ticket');

		$datatables = Datatables::of($lending)
			->addColumn('action', function ($lending)
				{
					$d = new DNS1D();
					//$d->setStorPath(__DIR__."/cache/");
					$barcode = htmlentities($d->getBarcodeHTML($lending->return_ticket, "C128"));
					$idType = $this->Act->GetField('asset_item','id',$lending->asset_item_id,'asset_type_id');
					$type = $this->Act->GetField('asset_type','id',$idType,'name');
					$typeCode = $this->Act->GetField('asset_type','id',$idType,'code');
					$idRoom = $this->Act->GetField('asset_item','id',$lending->asset_item_id,'asset_room_id');
					$room = $this->Act->GetField('asset_room','id',$idRoom,'name');
					$codeRoom = $this->Act->GetField('asset_room','id',$idRoom,'code');
					return '<a href="#" data-barcode="'.$barcode.'" data-id="'.$lending->id.'" data-lending_ticket="'.$lending->lending_ticket.'" data-created_at="'.$this->Date->WaktuIndo($lending->created_at).'" data-started_at="'.$this->Date->WaktuIndo($lending->started_at).'" data-ended_at="'.$this->Date->WaktuIndo($lending->ended_at).'" data-asset_name="'.$lending->asset_name.'" data-user_name="'.$lending->user_name.'"
					data-nup="'.$lending->nup.'" data-nip="'.$lending->nip.'" data-users_id="'.$lending->users_id.'" data-type="'.$type.'"
					data-type_code="'.$typeCode.'" data-room="'.$room.'" data-room_code="'.$codeRoom.'" data-toggle="modal" data-target="#modal_edit"><i class="ion ion-clipboard text-green"></i></a>';
				})
			->editColumn('started_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->started_at);
				})
			->editColumn('ended_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->ended_at);
				})
			->editColumn('created_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->created_at);
				})
			->removeColumn('id');

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}
	public function getLog(Request $request)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$lending = DB::table('lending as l')->join('users as u', 'l.users_id', '=', 'u.id')
			->join('asset_item as a', 'l.asset_item_id', '=', 'a.id')
			->select([
				DB::raw('@rownum := @rownum + 1 AS rownum'),
					'l.lending_ticket',
					'l.return_ticket',
					'l.started_at',
					'l.ended_at',
					'l.created_at',
					'l.updated_at',
					'l.id',
					'l.asset_item_id',
					'a.name as asset_name',
					'a.nup',
					'u.name as user_name',
					'u.nip',
					'u.id as users_id'
				])
			->where('l.status', '=', '3')
			->whereNotNull('return_ticket')
			->whereNotNull('returned_at');

				if (Auth::user()->roles_id == 6):
                
                $lending->where('a.asset_type_id','=',2 );

			   endif;

		$datatables = Datatables::of($lending)
			->addColumn('action', function ($lending)
				{
					$idType = $this->Act->GetField('asset_item','id',$lending->asset_item_id,'asset_type_id');
					$type = $this->Act->GetField('asset_type','id',$idType,'name');
					$typeCode = $this->Act->GetField('asset_type','id',$idType,'code');
					$idRoom = $this->Act->GetField('asset_item','id',$lending->asset_item_id,'asset_room_id');
					$room = $this->Act->GetField('asset_room','id',$idRoom,'name');
					$codeRoom = $this->Act->GetField('asset_room','id',$idRoom,'code');
					return '<a href="#" data-id="'.$lending->id.'" data-lending_ticket="'.$lending->lending_ticket.'" data-created_at="'.$this->Date->WaktuIndo($lending->created_at).'" data-started_at="'.$this->Date->WaktuIndo($lending->started_at).'" data-ended_at="'.$this->Date->WaktuIndo($lending->ended_at).'" data-asset_name="'.$lending->asset_name.'" data-user_name="'.$lending->user_name.'" data-nup="'.$lending->nup.
					'" data-nip="'.$lending->nip.'" data-users_id="'.$lending->users_id.'" data-type="'.$type.'" '.
					'data-type_code="'.$typeCode.'" data-room="'.$room.'" data-room_code="'.$codeRoom.'" data-toggle="modal" data-target="#modal_edit"><i class="ion ion-clipboard text-green"></i></a>';
				})
			->editColumn('started_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->started_at);
				})
			->editColumn('ended_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->ended_at);
				})
			->editColumn('updated_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->updated_at);
				})
			->removeColumn('id');

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	/**
	 * Return Detail
	 *
	 * @return Datatable
	 */

	public function getReturnDetail(Request $request, $id)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$lending = DB::table('lending as l')->join('users as u', 'l.users_id', '=', 'u.id')
			->join('asset_item as a', 'l.asset_item_id', '=', 'a.id')
			->select([
				DB::raw('@rownum := @rownum + 1 AS rownum'),
					'l.lending_ticket',
					'l.returned_at',
					'l.return_ticket',
					'l.started_at',
					'l.ended_at',
					'l.asset_item_id',
					'a.name as asset_name',
					'u.id'
				])
			->where('u.id', '=', $id)
			->where('l.status', '=', '3')
			->whereNotNull('l.return_ticket')
			->whereNotNull('l.returned_at');

		$datatables = Datatables::of($lending)

			->editColumn('started_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->started_at);
				})
			->editColumn('ended_at', function ($lending)
				{
					return $this->Date->WaktuIndoShort($lending->ended_at);
				})
			->removeColumn('id');

		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request)
	{
		$data = DB::table('lending as l')->join('users as u', 'l.users_id', '=', 'u.id')
				->join('asset_item as a', 'l.asset_item_id', '=', 'a.id')
				->select([
					'l.lending_ticket',
					'l.return_ticket',
					'l.started_at',
					'l.ended_at',
					'l.created_at',
					'l.updated_at',
					'l.id',
					'l.asset_item_id',
					'a.name as asset_name',
					'a.nup',
					'u.name as user_name',
					'u.nip',
					'p.position',
					'u.id as users_id'
				])
				->leftJoin('positions as p', 'u.positions_id', '=', 'p.id')
				->where('return_ticket',$request->tiket)
				->first();
		if ($data) {
			$idType = $this->Act->GetField('asset_item','id',$data->asset_item_id,'asset_type_id');
			$type = $this->Act->GetField('asset_type','id',$idType,'name');
			$typeCode = $this->Act->GetField('asset_type','id',$idType,'code');
			$idRoom = $this->Act->GetField('asset_item','id',$data->asset_item_id,'asset_room_id');
			$room = $this->Act->GetField('asset_room','id',$idRoom,'name');
			$codeRoom = $this->Act->GetField('asset_room','id',$idRoom,'code');
			$date = $this->Date->WaktuIndo($data->created_at);
			return Response::json(['status' => true, 'message' => $data, 'type' => $type, 'type_code' => $typeCode, 'room' => $room, 'room_code' => $codeRoom, 'created' => $date]);
		} else {
			return Response::json(['status' => false, 'message' => 'Tiket Tidak Ditemukan']);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		$update = DB::table('lending')->where('id',$request->id)
		->update(['status'=>$request->status,'returned_at'=>Date('Y-m-d h:i:s')]);
		$assetId = $this->Act->GetField('lending','id',$request->id,'asset_item_id');
		if ($update) {
			DB::table('asset_item')->where('id',$assetId)->update(['status'=>'1','users_id'=>null]);
		}
		return Response::json(['status' => true, 'message' => 'Data Berhasil Disimpan']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
