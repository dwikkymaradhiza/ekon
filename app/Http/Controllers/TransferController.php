<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Action;
use App\Helper\Date;
use Response;
use DB;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;

class TransferController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function __construct()
	{
		// $this->middleware('auth');
		$this->Act = new Action;
		$this->Date = new Date;
	}

	public function index()
	{
		return view('transfer.index');
	}

	public function getLog(Request $request)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$move = DB::table('moving as m')
			->join('users as u', 'm.users_id', '=', 'u.id')
			->join('asset_item as a', 'm.asset_item_id', '=', 'a.id')
			->select([
				DB::raw('@rownum := @rownum + 1 AS rownum'),
					'm.id',
					'm.ticket',
					'm.init_position',
					'm.new_position',
					'm.status',
					'm.created_at',
					'm.asset_item_id',
					'a.name as asset_name',
					'a.nup',
					'u.name as user_name',
					'u.nip',
					'p.position',
					'u.id as user_id'
				])
			->leftJoin('positions as p', 'u.positions_id', '=', 'p.id')
			->whereIn('m.status', ['1','2']);

		$datatables = Datatables::of($move)
		 	->addColumn('action', function ($move)
				{
					$idType = $this->Act->GetField('asset_item','id',$move->asset_item_id,'asset_type_id');
					$type = $this->Act->GetField('asset_type','id',$idType,'name');
					$typeCode = $this->Act->GetField('asset_type','id',$idType,'code');
					$idRoom = $this->Act->GetField('asset_item','id',$move->asset_item_id,'asset_room_id');
					$room = $this->Act->GetField('asset_room','id',$idRoom,'name');
					$codeRoom = $this->Act->GetField('asset_room','id',$idRoom,'code');
					return '<a href="#" data-id="'.$move->id.'" data-ticket="'.$move->ticket.'" data-created_at="'.$this->Date->WaktuIndo($move->created_at).'" data-asset_name="'.$move->asset_name.'" data-user_name="'.$move->user_name.'"  data-nip="'.$move->nip.'" data-position="'.$move->position.'" data-user_id="'.$move->user_id.'" data-nup="'.$move->nup.'" data-type="'.$type.'"'.
					' data-status="'.$move->status.'" data-type_code="'.$typeCode.'" data-room="'.$room.'" data-room_code="'.$codeRoom.'" data-toggle="modal" data-target="#modal_edit"><i class="ion ion-clipboard text-green"></i></a>';
				})
			->addColumn('init_position', function ($move)
				{
					$init_loc = $this->Act->GetField('asset_room','asset_location_id',$move->init_position,'name');
					return $init_loc;
				})
			->addColumn('new_position', function ($move)
				{
					$init_loc = $this->Act->GetField('asset_room','asset_location_id',$move->new_position,'name');
					return $init_loc;
				})
			->editColumn('created_at', function ($move)
				{
					return $this->Date->WaktuIndoShort($move->created_at);
				})
			->removeColumn('id');
		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	public function getList(Request $request)
	{
		DB::statement(DB::raw('set @rownum=0'));
		$move = DB::table('moving as m')
			->join('users as u', 'm.users_id', '=', 'u.id')
			->join('asset_item as a', 'm.asset_item_id', '=', 'a.id')
			->select([
				DB::raw('@rownum := @rownum + 1 AS rownum'),
					'm.id',
					'm.ticket',
					'm.init_position',
					'm.new_position',
					'm.status',
					'm.created_at',
					'm.asset_item_id',
					'a.name as asset_name',
					'a.nup',
					'u.name as user_name',
					'u.nip',
					'p.position',
					'u.id as user_id'
				])
			->leftJoin('positions as p', 'u.positions_id', '=', 'p.id')
			->whereIn('m.status', ['0']);

		$datatables = Datatables::of($move)

		 	->addColumn('action', function ($move)
				{
					$idType = $this->Act->GetField('asset_item','id',$move->asset_item_id,'asset_type_id');
					$type = $this->Act->GetField('asset_type','id',$idType,'name');
					$typeCode = $this->Act->GetField('asset_type','id',$idType,'code');
					$idRoom = $this->Act->GetField('asset_item','id',$move->asset_item_id,'asset_room_id');
					$room = $this->Act->GetField('asset_room','id',$idRoom,'name');
					$codeRoom = $this->Act->GetField('asset_room','id',$idRoom,'code');
					return '<a href="#" data-id="'.$move->id.'" data-ticket="'.$move->ticket.'" data-created_at="'.$this->Date->WaktuIndo($move->created_at).'" data-asset_name="'.$move->asset_name.'" data-user_name="'.$move->user_name.'"  data-nip="'.$move->nip.'" data-position="'.$move->position.'" data-user_id="'.$move->user_id.'" data-nup="'.$move->nup.'" data-type="'.$type.
					'" data-type_code="'.$typeCode.'" data-room="'.$room.'" data-room_code="'.$codeRoom.'" data-status="'.$move->status.'"  data-toggle="modal" data-target="#modal_edit"><i class="ion ion-clipboard text-green"></i></a>';
				})


			->addColumn('init_position', function ($move)
				{
					 $init_loc = $this->Act->GetField('asset_room','id',$move->init_position,'name');
					 return $init_loc;
				})
			->addColumn('new_position', function ($move)
				{
					 $init_loc = $this->Act->GetField('asset_room','id',$move->new_position,'name');
					 return $init_loc;
				})
			->editColumn('created_at', function ($move)
				{
					return $this->Date->WaktuIndoShort($move->created_at);
				})
			->removeColumn('id');
		if ($keyword = $request->get('search')['value']):
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum + 1 like ?', ["%{$keyword}%"]);
		endif;

		return $datatables->make(true);
	}

	public function indexLog()
	{
		return view('transfer.indexlog');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		$update = DB::table('moving')->where('id',$request->id)
		->update(['status'=>$request->status,'moved_at'=>Date('Y-m-d')]);
		return Response::json(['status' => true,'message'=>'successfully update data']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
