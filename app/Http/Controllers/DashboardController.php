<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\AssetType;
use App\AssetRoom;
use App\Service;
use App\Lending;
use App\Moving;

class DashboardController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$role = Auth::user()->roles_id;
		switch ($role) {
			case 1:
			case 2:
			case 5:
				return $this->admin();
				break;
			case 3:
				return $this->technician();
				break;
			case 6:
				return $this->receptionist();
			default:
				return $this->user();
				break;
		}
	}

	public function admin()
	{
		$asset_types = AssetType::all()->pluck('name', 'id');

		$service = Service::getServiceCompare();
		$lending = Lending::getLendingCompare();
		$return	 = Lending::getReturnCompare();
		$moving	 = Moving::getMovingCompare();

		$rep_service = Service::getReport();

		$most_service = Service::getMostCount();
		$most_cost = Service::getMostCost();

		return view('dashboard.admin', compact('asset_types', 'service', 'lending', 'return', 'moving', 'rep_service', 'most_service', 'most_cost'));
	}
	public function technician()
	{
		$asset_types = AssetType::all()->pluck('name', 'id');
		$service = Service::getServiceCompare(Auth::user()->id);
		$rep_service = Service::getReport(Auth::user()->id);
		$most_service = Service::getMostCount(Auth::user()->id);

		return view('dashboard.technician', compact('asset_types', 'service', 'rep_service', 'most_service'));
	}
	public function user()
	{
		$rooms = AssetRoom::all();
		return view('dashboard.user', compact('rooms'));
	}
	public function receptionist()
	{
		return redirect('peminjaman/form-pengajuan-rapat');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$data = $request->all();
		session(['role' => $data['username']]);
		return redirect('dasbor');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

}
