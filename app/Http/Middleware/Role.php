<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next, $role)
	{
		if (Auth::user()->roles_id != 1 AND (is_null(app('access')) OR app('access')[$role] == '0')):
			return response('Unauthorized', 401)->header('Content-Type', 'text/plain');
		endif;

		return $next($request);
	}
}
