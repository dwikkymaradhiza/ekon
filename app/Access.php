<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Access extends Model
{
	protected $table = 'access';

	protected $fillable = [
		'modules_id', 'roles_id', 'read', 'create', 'update', 'delete'
	];

	protected $dates = ['deleted_at'];

	public function module()
	{
		return $this->belongsTo('App\Module');
	}
	public function role()
	{
		return $this->belongsTo('App\Role');
	}

	public static function get_access()
	{
		$uri = str_replace(url('/').'/', '', url()->current());
		if (Module::where('uri', $uri)->count() == 0):
			$part = strrpos($uri, '/');
			$last = substr($uri, ($part + 1));
			if (is_numeric($last)):
				$uri = substr($uri, 0, $part);
			endif;
		endif;

		$role = Auth::user()->roles_id;
		if ($role == 1):
			return ['read' => 1, 'create' => 1, 'update' => 1, 'delete' => 1, 'roles_id' => $role, 'role' => Role::find($role)->role];
		else:
			$access = Access::select('access.read', 'access.create', 'access.update', 'access.delete', 'access.roles_id', 'r.role')
							->leftJoin('modules as m', 'access.modules_id', '=', 'm.id')
							->leftJoin('roles as r', 'access.roles_id', '=', 'r.id')
							->where('roles_id', ($role != 1 ? '=' : '!='), $role)
							->where('m.uri', $uri)
							->first();
			if ($access):
				return $access->toArray();
			else:
				return ['read' => 0, 'create' => 0, 'update' => 0, 'delete' => 0, 'roles_id' => $role, 'role' => Role::find($role)->role];
			endif;
		endif;
	}

	public static function get_menu($is_public)
	{
		$role = Auth::user()->roles_id;
		if ($role == 1):
			$menu = Module::where('on_menu', '1')
							->where('is_public', $is_public)
							->whereNull('parent')
							->orderBy('menu_order', 'asc')
							->distinct()
							->get()
							->toArray();
			foreach ($menu as $k => $v):
				$menu[$k]['children'] = Module::where('on_menu', '1')
											->where('is_public', $is_public)
											->where('parent', $v['id'])
											->orderBy('menu_order', 'asc')
											->distinct()
											->get()
											->toArray();
			endforeach;
		else:
			$menu = Access::select('m.*')
							->leftJoin('modules as m', 'modules_id', '=', 'm.id')
							->where('roles_id', $role)
							->where('on_menu', '1')
							->where('is_public', $is_public)
							->whereNull('parent')
							->where('read', '1')
							->orderBy('menu_order', 'asc')
							->distinct()
							->get()
							->toArray();
			foreach ($menu as $k => $v):
				$menu[$k]['children'] = Access::select('m.*')
											->leftJoin('modules as m', 'modules_id', '=', 'm.id')
											->where('roles_id', $role)
											->where('on_menu', '1')
											->where('is_public', $is_public)
											->where('parent', $v['id'])
											->where('read', '1')
											->orderBy('menu_order', 'asc')
											->distinct()
											->get()
											->toArray();
			endforeach;
		endif;

		return $menu;
	}
}
