<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class Action extends Model
{
    function GetField($table,$field_name,$where,$get_field)
	{
      $user=DB::table($table)
		  ->where($field_name,$where)
		  ->select($get_field)
		  ->first();
	   if($user){

		  return $user->$get_field;
	   }	
	   return "";  
	}
	function TglIndoToMysql($time)
	{
      $value=strtotime("$time");
      return date("Y-m-d H:i", $value);
	}
	
}