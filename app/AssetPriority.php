<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssetPriority extends Model
{
	use SoftDeletes;

	protected $table = 'asset_priority';

	protected $fillable = [
		'asset_item_id', 'users_id', 'priority'
	];

	protected $dates = ['deleted_at'];

	public function item()
	{
		return $this->belongsTo('App\AssetItem');
	}
	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
