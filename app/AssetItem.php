<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssetItem extends Model
{
	use SoftDeletes;

	protected $table = 'asset_item';

	protected $fillable = [
		'asset_type_id', 'users_id', 'nup', 'name', 'acquisition', 'routine_cost', 'next_routine', 'asset_room_id', 'room_description', 'description', 'status', 'is_public'
	];

	protected $dates = ['deleted_at'];

	public function type()
	{
		return $this->belongsTo('App\AssetType');
	}
	public function room()
	{
		return $this->belongsTo('App\AssetRoom');
	}
}
