<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PegawaiJabatan extends Model
{
    protected $connection = 'mysql_pegawai';
    protected $table = 'pegawai_jabatan';
}
