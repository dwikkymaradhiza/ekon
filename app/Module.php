<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
	protected $table = 'modules';

	protected $fillable = [
		'name', 'uri', 'on_menu', 'parent', 'is_public', 'css_class', 'menu_order'
	];

	protected $dates = ['deleted_at'];

}
