<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lending extends Model
{
	use SoftDeletes;

	protected $table = 'lending';

	protected $fillable = [
		'lending_ticket', 'return_ticket', 'asset_item_id', 'users_id', 'started_at', 'ended_at', 'taken_at', 'returned_at', 'status'
	];

	protected $dates = ['deleted_at', 'started_at', 'ended_at'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
	public function item()
	{
		return $this->belongsTo('App\AssetItem');
	}

	public static function getLendingCompare()
	{
		return [
				lending::where('status', '0')->whereYear('created_at', date('Y'))->count(),
				lending::whereYear('created_at', date('Y'))->count()
			];
	}

	public static function getReturnCompare()
	{
		return [
				Lending::where('status', '1')->whereNotNull('return_ticket')->whereYear('created_at', date('Y'))->count(),
				Lending::whereNotNull('return_ticket')->whereYear('created_at', date('Y'))->count()
			];
	}
}
