<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('user/search', ['as' => 'autocomplete', 'uses'=> 'UserController@autocomplete']);

Auth::routes();

App::singleton('menu', function () {
	return App\Access::get_menu('0');
});
App::singleton('menu_public', function () {
	return App\Access::get_menu('1');
});
App::singleton('access', function () {
	return App\Access::get_access();
});

Route::get('pemeliharaan/tampilan', 'HelpdeskController@display_view');
Route::get('pemeliharaan/data-daftar-tampilan', 'HelpdeskController@display_data');
Route::get('aset/ruang-meeting', 'AssetController@display_view');
Route::get('aset/data-ruang-meeting', 'AssetController@display_data');

Route::group(['middleware' => 'auth'], function () {

	Route::get('/', function () {
		return redirect('dasbor');
	});

	Route::group(['middleware' => 'role:read'], function () {

		// **
		// Home / Dashboard
		Route::get('dasbor', 'DashboardController@index');
		// **

		// **
		// User
		Route::get('aset-saya', ['as' => 'user.dashboard', 'uses' => 'DashboardController@user']);
		Route::get('log-saya', ['as' => 'user.log', 'uses' => 'UserController@user_log']);
		Route::get('data-aset-saya', ['as' => 'user.asset', 'uses' => 'UserController@get_user_assets']);
		Route::get('data-aset-tetap-saya', ['as' => 'user.asset.fixed', 'uses' => 'UserController@get_user_fixed_assets']);
		Route::get('data-log-saya', ['as' => 'user.logdata', 'uses' => 'UserController@get_user_log']);
		Route::post('aset/tipe/{type?}', ['as' => 'assets.available', 'uses' => 'UserController@get_available_assets']);
		Route::get('peminjaman/cetak/{id?}', ['as' => 'lending.print', 'uses' => 'LendingController@download']);
		// **

		// **
		// User & Role Management
		Route::group(['prefix' => 'pengguna'], function () {
			// **
			// Index
			Route::get('/', 'UserController@index');
			// **
			// API
			Route::get('data-pengguna', 'UserController@indexList');
			Route::get('data-detil/{id}', 'UserController@indexShow');
			// **
		});
		// **

		// **
		// Asset Management
		Route::group(['prefix' => 'aset'], function () {
			// **
			Route::get('/', 'AssetController@index');
			// **
			// Asset
			Route::get('aset', 'AssetController@asset');
			Route::get('data-aset', 'AssetController@assetList');
			// **
			// Type
			Route::get('jenis', 'AssetController@type');
			Route::get('data-jenis', 'AssetController@typeList');
			// **
			// Room
			Route::get('ruangan', 'AssetController@room');
			Route::get('data-ruangan', 'AssetController@roomList');
			// **
			// Location
			Route::get('lokasi', 'AssetController@location');
			Route::get('data-lokasi', 'AssetController@locationList');
			// **
		});
		// **

		// **
		// Lending Management
		Route::group(['prefix' => 'peminjaman'], function () {
			Route::get('/', 'LendingController@index');
			Route::get('/form-pengajuan', 'LendingController@create_user');
			Route::get('/form-pengajuan-rapat', 'LendingController@create_user');
			Route::get('log', 'LendingController@indexLog');
			Route::get('data-daftar', 'LendingController@getList');
			Route::get('data-log', 'LendingController@getLog');
			Route::get('data-tiket', 'LendingController@show');
			Route::get('data-detil/{id}', 'LendingController@getLendingDetail');
		});
		// **

		// **
		// Returning
		Route::group(['prefix' => 'pengembalian'], function () {
			Route::get('/', 'ReturnController@index');
			Route::get('log', 'ReturnController@indexLog');
			Route::get('data-daftar', 'ReturnController@getList');
			Route::get('data-detil/{id}', 'ReturnController@getReturnDetail');
			Route::get('data-log', 'ReturnController@getLog');
			Route::get('data-tiket', 'ReturnController@show');
		});
		// **

		// **
		// Service Management
		Route::group(['prefix' => 'pemeliharaan'], function () {
			Route::get('/', 'HelpdeskController@index');
			Route::get('log', 'HelpdeskController@indexLog');
			Route::get('data-daftar', 'HelpdeskController@getList');
			Route::get('data-log', 'HelpdeskController@getLog');
			Route::get('data-detil/{id}', 'HelpdeskController@getDetailHelpdesk');
		});
		// **

		// **
		// Asset Transfer Management
		Route::group(['prefix' => 'pemindahan'], function () {
			Route::get('/', 'TransferController@index');
			Route::get('/update', 'TransferController@update');
			Route::get('log', 'TransferController@indexLog');
		    Route::get('data-daftar', 'TransferController@getList');
			Route::get('data-log', 'TransferController@getLog');
		});
		// **

		// **
		// Scan
		Route::group(['prefix' => 'pindai'], function () {
			Route::get('pengambilan', 'LendingController@scan');
			Route::get('pengembalian', 'ReturnController@scan');
		});
		// **

		Route::get('aset/histori/{id?}', ['as' => 'assets.history', 'uses' => 'UserController@get_asset_history']);
	});

	Route::group(['middleware' => 'role:create'], function () {

		// Service
		Route::post('helpdesk', ['as' => 'helpdesk.store', 'uses' => 'UserController@store_service']);

		// Lending
		Route::post('peminjaman', ['as' => 'lending.store', 'uses' => 'UserController@store_lending']);

		// Return
		Route::post('pengembalian', ['as' => 'return.store', 'uses' => 'UserController@store_return']);

		// Moving
		Route::post('pemindahan', ['as' => 'moving.store', 'uses' => 'UserController@store_moving']);

		// **
		// Asset Management
		// Location

		// **
		// Pengguna
		Route::post('pengguna/tambah-pengguna', 'UserController@store');
		// **

		// **
		// Asset Management
		Route::group(['prefix' => 'aset'], function () {
			// **
			//
			// **
			// Asset
			Route::post('aset', 'AssetController@assetStore');
			// **
			// Type
			Route::post('jenis', 'AssetController@typeStore');
			// **
			// Room
			Route::post('ruangan', 'AssetController@roomStore');
			// **
			// Location
			Route::post('lokasi', 'AssetController@locationStore');
			// **
		});
		// **

	});

	Route::group(['middleware' => 'role:update'], function () {

		// **
		// User & Role Management
		Route::put('pengguna/{id}', 'UserController@update');
		Route::put('pengguna/sunting-pengguna/{id}', 'UserController@edit');
		// **

		// **
		// Asset Management
		Route::group(['prefix' => 'aset'], function () {
			// **
			//
			// **
			// Asset
			Route::put('aset/{id}', 'AssetController@assetUpdate');
			// **
			// Type
			Route::put('jenis/{id}', 'AssetController@typeUpdate');
			// **
			// Room
			Route::put('ruangan/{id}', 'AssetController@roomUpdate');
			// **
			// Location
			Route::put('lokasi/{id}', 'AssetController@locationUpdate');
			// **
		});
		// **

		// **
		// Lending
		Route::post('peminjaman/update', 'LendingController@update');
		Route::post('peminjaman/verify', 'LendingController@verify');

		// **
		// Helpdesk
		Route::post('pemeliharaan/update', 'HelpdeskController@update');

		// **
		// Return
		Route::post('pengembalian/update', 'ReturnController@update');

		// **
		// moving
		Route::post('pemindahan/update', 'TransferController@update');
	});

	Route::group(['middleware' => 'role:delete'], function () {

		// **
		// Asset Management
		Route::group(['prefix' => 'aset'], function () {
			// **
			//
			// **
			// Asset
			Route::delete('aset/{id}', 'AssetController@assetDestroy');
			// **
			// Type
			Route::delete('jenis/{id}', 'AssetController@typeDestroy');
			// **
			// Room
			Route::delete('ruangan/{id}', 'AssetController@roomDestroy');
			// **
			// Location
			Route::delete('lokasi/{id}', 'AssetController@locationDestroy');
			// **
		});
		// **

		// **
		// Pengguna
		Route::delete('pengguna/hapus-pengguna/{id}', 'UserController@destroy');
		// **
	});
});
