<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service', function (Blueprint $table) {
			$table->increments('id');
			$table->enum('type', ['0', '1'])->default('0')->comment('0 = Helpdesk, 1 = Routine');
			$table->string('ticket', 15);
			$table->integer('asset_item_id')->unsigned();
			$table->integer('users_id')->unsigned();
			$table->text('complaint');
			$table->integer('technician')->unsigned()->nullable();
			$table->integer('init_cost')->unsigned();
			$table->text('init_info')->nullable();
			$table->integer('cost')->unsigned();
			$table->text('info')->nullable();
			$table->date('checked_at')->nullable();
			$table->date('done_at')->nullable();
			$table->enum('status', ['0', '1', '2', '3', '4'])->default('0')->comment('0 = Waiting Approval, 1 = Field Checking, 2 = Initial Report, 3 = Done, 4 = Rejected');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('asset_item_id')
				->references('id')->on('asset_item')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('users_id')
				->references('id')->on('users')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('technician')
				->references('id')->on('users')
				->onDelete('cascade')
				->onUpdate('cascade');
		}); 
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::dropIfExists('service');
	}
}
