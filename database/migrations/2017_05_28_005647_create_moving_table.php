<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovingTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('moving', function (Blueprint $table) {
			$table->increments('id');
			$table->string('ticket', 15);
			$table->integer('asset_item_id')->unsigned();
			$table->integer('users_id')->unsigned();
			$table->integer('init_position')->unsigned();
			$table->integer('new_position')->unsigned();
			$table->date('moved_at')->nullable();
			$table->enum('status', ['0', '1', '2'])->default('0')->comment('0 = Waiting Approval, 1 = Approved, 2 = Rejected');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('asset_item_id')
				->references('id')->on('asset_item')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('users_id')
				->references('id')->on('users')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('init_position')
				->references('id')->on('asset_room')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('new_position')
				->references('id')->on('asset_room')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::dropIfExists('moving');
	}
}
