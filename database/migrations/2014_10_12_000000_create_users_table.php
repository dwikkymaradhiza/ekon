<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// ROLES
		Schema::create('roles', function (Blueprint $table) {
			$table->increments('id');
			$table->string('role', 24);
			$table->timestamps();
		});

		// POSITION
		Schema::create('positions', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code', 24)->unique();
			$table->string('position');
			$table->string('position_short')->nullable();
			$table->string('unit');
			$table->string('unit_short')->nullable();
			$table->integer('asset_location_id')->unsigned()->default(1);
			$table->timestamps();
		});

		// SELECT ID AS `id`, KODE AS `code`, 
		// 	NAMA_JABATAN AS `position`, NAMA_JABATAN_SINGKAT AS `position_short`, 
		// 	NAMA_UNIT AS `unit`, NAMA_UNIT_SINGKAT AS `unit_short`, 1 AS `asset_location_id` 
		// FROM ref_jabatan;
		
		// USERS
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('email')->unique();
			$table->string('password');
			$table->bigInteger('simpeg_id')->unsigned()->unique();
			$table->bigInteger('nip')->unsigned()->unique();
			$table->integer('positions_id')->unsigned()->default(1284);
			$table->integer('roles_id')->unsigned()->default(4);
			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('roles_id')
				->references('id')->on('roles')
				->onDelete('cascade')
				->onUpdate('cascade');

			$table->foreign('positions_id')
				->references('id')->on('positions')
				->onDelete('cascade')
				->onUpdate('cascade');
		});

		// SELECT IF(p.NAMA_GELAR IS NULL, p.NAMA, p.NAMA_GELAR) AS `name`, 
		// 	IF(p.EMAIL = '', p.NIP_BARU, p.EMAIL) AS `email`, 
		// 	p.PASSWORD AS `password`, p.ID AS `simpeg_id`, 
		// 	p.NIP_BARU AS `nip`, IF(r.ID IS NULL, 1284, r.ID) AS `positions_id` 
		// FROM simpeg.pegawai AS p 
		// LEFT JOIN simpeg.pegawai_jabatan AS j ON p.ID_JABATAN_TERAKHIR = j.ID 
		// LEFT JOIN simpeg.ref_jabatan AS r ON j.ID_REF_JABATAN = r.ID 
		// WHERE p.ID NOT IN (149, 156, 317, 324, 419, 449, 255, 368);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::dropIfExists('users');
		Schema::dropIfExists('roles');
	}
}
