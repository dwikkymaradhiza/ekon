<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('asset_location', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code', 24)->unique();
			$table->string('description')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('asset_room', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code', 16)->unique();
			$table->string('name');
			$table->string('description')->nullable();
			$table->integer('asset_location_id')->unsigned();
			$table->integer('users_id')->unsigned()->comment('PIC');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('asset_location_id')
				->references('id')->on('asset_location')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('users_id')
				->references('id')->on('users')
				->onDelete('cascade')
				->onUpdate('cascade');
		});

		Schema::create('asset_type', function (Blueprint $table) {
			$table->increments('id');
			$table->bigInteger('code')->unsigned()->unique();
			$table->string('name');
			$table->string('short_name')->nullable();
			$table->tinyInteger('routine_interval')->default(0)->comment('0 value means no routine service');
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('asset_item', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('asset_type_id')->unsigned();
			$table->integer('users_id')->unsigned()->nullable();
			$table->integer('nup')->unsigned()->unique();
			$table->string('name');
			$table->date('acquisition');
			$table->integer('routine_cost')->unsigned()->nullable();
			$table->date('next_routine')->nullable()->comment('1st initial obtained from acquisition + routine_interval, then obtained from next_routine + routine_interval');
			$table->integer('asset_room_id')->unsigned();
			$table->string('room_description')->nullable();
			$table->string('description')->nullable();
			$table->enum('status', ['0', '1'])->default('1')->comment('0 = Unavailable, 1 = Available');
			$table->enum('is_public', ['0', '1'])->default('0')->comment('0 = Non Public, 1 = Public');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('asset_room_id')
				->references('id')->on('asset_room')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('asset_type_id')
				->references('id')->on('asset_type')
				->onDelete('cascade')
				->onUpdate('cascade');
		});

		Schema::create('asset_priority', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('asset_item_id')->unsigned();
			$table->integer('users_id')->unsigned();
			$table->tinyInteger('priority')->unsigned();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('asset_item_id')
				->references('id')->on('asset_item')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('users_id')
				->references('id')->on('users')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::dropIfExists('asset_priority');
		Schema::dropIfExists('asset_item');
		Schema::dropIfExists('asset_type');
		Schema::dropIfExists('asset_room');
		Schema::dropIfExists('asset_location');
	}
}
