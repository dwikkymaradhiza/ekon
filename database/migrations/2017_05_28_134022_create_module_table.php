<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('modules', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('uri');
			$table->enum('on_menu', ['0', '1'])->default('0')->comment('0 = No, 1 = Yes');
			$table->enum('is_public', ['0', '1'])->default('0')->comment('0 = No, 1 = Yes');
			$table->integer('parent')->unsigned()->nullable();
			$table->string('css_class')->nullable();
			$table->tinyInteger('menu_order')->unsigned()->nullable();
			$table->timestamps();
		});

		Schema::create('access', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('modules_id')->unsigned();
			$table->integer('roles_id')->unsigned();
			$table->enum('read', ['0', '1'])->default('1');
			$table->enum('create', ['0', '1'])->default('0');
			$table->enum('update', ['0', '1'])->default('0');
			$table->enum('delete', ['0', '1'])->default('0');
			$table->timestamps();

			$table->foreign('modules_id')
				->references('id')->on('modules')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('roles_id')
				->references('id')->on('roles')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::dropIfExists('access');
		Schema::dropIfExists('modules');
	}
}
