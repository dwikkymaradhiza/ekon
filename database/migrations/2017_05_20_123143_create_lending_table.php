<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLendingTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lending', function (Blueprint $table) {
			$table->increments('id');
			$table->string('lending_ticket', 15);
			$table->string('return_ticket', 15)->nullable();
			$table->integer('asset_item_id')->unsigned();
			$table->integer('users_id')->unsigned();
			$table->date('started_at');
			$table->date('ended_at');
			$table->date('taken_at')->nullable();
			$table->date('returned_at')->nullable();
			$table->text('desc')->nullable();
			$table->enum('status', ['0', '1', '2', '3'])->default('0')->comment('0 = Waiting Approval, 1 = Approved, 2 = Rejected, 3 = Returned');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('asset_item_id')
				->references('id')->on('asset_item')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('users_id')
				->references('id')->on('users')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::dropIfExists('lending');
	}
}
