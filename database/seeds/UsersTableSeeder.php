<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// ROLES
		DB::table('roles')->insert([
			'id'	=> 1,
			'role'	=> 'Superadmin'
		]);
		DB::table('roles')->insert([
			'id'	=> 2,
			'role'	=> 'Admin'
		]);
		DB::table('roles')->insert([
			'id'	=> 3,
			'role'	=> 'Teknisi'
		]);
		DB::table('roles')->insert([
			'id'	=> 4,
			'role'	=> 'Pengguna'
		]);
		DB::table('roles')->insert([
			'id'	=> 5,
			'role'	=> 'Penilik'
		]);
		DB::table('roles')->insert([
			'id'	=> 6,
			'role'	=> 'Receptionist'
		]);
		DB::table('roles')->insert([
			'id'	=> 7,
			'role'	=> 'Admin BMN'
		]);

		// USERS
		DB::table('users')->insert([
			'id'			=> 1,
			'name'			=> 'Superadmin',
			'email'			=> 'rayendra.rajasa@gmail.com',
			'password'		=> bcrypt('5uP3r'),
			'simpeg_id'		=> 0,
			'nip'			=> 0,
			'positions_id'	=> 1284,
			'roles_id'		=> 1
		]);
		DB::table('users')->insert([
			'id'			=> 2,
			'name'			=> 'Rayani Marlinang, S.Sos., M.M.',
			'email'			=> 'rayani_marlinang@yahoo.co',
			'password'		=> bcrypt('196302261983102001'),
			'simpeg_id'		=> 149,
			'nip'			=> 196302261983102001,
			'positions_id'	=> 412,
			'roles_id'		=> 7
		]);
		DB::table('users')->insert([
			'id'			=> 3,
			'name'			=> 'Agam Embun Sunarpati, S.Sos., M.Hum.',
			'email'			=> 'agam.embun.sunarpati@mail.me',
			'password'		=> bcrypt('196309141983101001'),
			'simpeg_id'		=> 156,
			'nip'			=> 196309141983101001,
			'positions_id'	=> 37,
			'roles_id'		=> 2
		]);
		DB::table('users')->insert([
			'id'			=> 4,
			'name'			=> 'Dodi Wahyugi ST,MMSI',
			'email'			=> 'dodiwahyugi@ekon.go.id',
			'password'		=> bcrypt('198106222003121003'),
			'simpeg_id'		=> 317,
			'nip'			=> 198106222003121003,
			'positions_id'	=> 34,
			'roles_id'		=> 3
		]);
		DB::table('users')->insert([
			'id'			=> 5,
			'name'			=> 'Renggo Wasongko',
			'email'			=> 'renggo_wasongko@yahoo.com',
			'password'		=> bcrypt('198206262007011001'),
			'simpeg_id'		=> 324,
			'nip'			=> 198206262007011001,
			'positions_id'	=> 110,
		]);
		DB::table('users')->insert([
			'id'			=> 6,
			'name'			=> 'Kharisma Sisbiono',
			'email'			=> 'kharisma.sisbiono@mail.me',
			'password'		=> bcrypt('198901302014021002'),
			'simpeg_id'		=> 419,
			'nip'			=> 198901302014021002,
			'positions_id'	=> 1284,
			'roles_id'		=> 3
		]);
		DB::table('users')->insert([
			'id'			=> 7,
			'name'			=> 'Yuli Nurhayati',
			'email'			=> 'yuli.nurhayati@mail.me',
			'password'		=> bcrypt('198807222014022002'),
			'simpeg_id'		=> 449,
			'nip'			=> 198807222014022002,
			'positions_id'	=> 1284,
		]);
		DB::table('users')->insert([
			'id'			=> 8,
			'name'			=> 'Dodi Rohimat Sopiana, S.Sos, M.Si',
			'email'			=> 'dodi.rohimat.sopiana@mail.me',
			'password'		=> bcrypt('197312171994031001'),
			'simpeg_id'		=> 255,
			'nip'			=> 197312171994031001,
			'positions_id'	=> 19,
		]);
		DB::table('users')->insert([
			'id'			=> 9,
			'name'			=> 'Hergy Cahyono',
			'email'			=> 'hergycah@gmail.com',
			'password'		=> bcrypt('198606292008121001'),
			'simpeg_id'		=> 368,
			'nip'			=> 198606292008121001,
			'positions_id'	=> 342,
		]);
	}
}
