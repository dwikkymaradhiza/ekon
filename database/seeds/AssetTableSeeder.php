<?php

use Illuminate\Database\Seeder;

class AssetTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// ASSET_LOCATION
		DB::table('asset_location')->insert([
			'id'		  => 1,
			'code'		  => '0',
			'description' => 'Unlocated'
		]);
		DB::table('asset_location')->insert([
			'id'		  => 2,
			'code'		  => '035.01.01.427752.000',
			'description' => ''
		]);

		// ASSET_ROOM
		DB::table('asset_room')->insert([
			'id'				=> 1,
			'code'				=> 'R.XXXXX',
			'description'		=> 'Unlocated',
			'asset_location_id'	=> 1,
			'users_id'			=> 1
		]);
		DB::table('asset_room')->insert([
			'id'				=> 2,
			'code'				=> 'U 06 018',
			'description'		=> 'Sekretariat Biro Perencanaan',
			'asset_location_id'	=> 2,
			'users_id'			=> 8
		]);
		DB::table('asset_room')->insert([
			'id'				=> 3,
			'code'				=> 'I 03 017',
			'description'		=> 'Ruang Kerja Sekretariat 01 Kedeputian V',
			'asset_location_id'	=> 2,
			'users_id'			=> 9
		]);

		// ASSET_TYPE
		DB::table('asset_type')->insert([
			'id'		 => 1,
			'code'		 => 3100203003,
			'name'		 => 'Printer (Peralatan Personal Komputer)',
			'short_name' => 'Printer'
		]);
		DB::table('asset_type')->insert([
			'id'		 => 2,
			'code'		 => 3100203004,
			'name'		 => 'Scanner (Peralatan Personal Komputer)',
			'short_name' => 'Scanner'
		]);

		// ASSET_ITEM
		DB::table('asset_item')->insert([
			'asset_type_id'		=> 1,
			'nup'				=> 62,
			'name'				=> 'HP LASER JET 1300',
			'acquisition'		=> '2003-01-01',
			'asset_room_id'		=> 2,
			'room_description'	=> '',
			'description'		=> 'Kondisi Rusak',
			'status'			=> '0'
		]);
		DB::table('asset_item')->insert([
			'asset_type_id'		=> 1,
			'nup'				=> 68,
			'name'				=> 'HP LASER JET 1000 S',
			'acquisition'		=> '2003-01-01',
			'asset_room_id'		=> 3,
			'room_description'	=> '',
			'description'		=> '',
			'status'			=> '1'
		]);
		DB::table('asset_item')->insert([
			'asset_type_id'		=> 1,
			'nup'				=> 94,
			'name'				=> 'HP LASER JET 1010',
			'acquisition'		=> '2004-01-01',
			'asset_room_id'		=> 3,
			'room_description'	=> '',
			'description'		=> '',
			'status'			=> '1'
		]);
		DB::table('asset_item')->insert([
			'asset_type_id'		=> 2,
			'nup'				=> 23,
			'name'				=> 'HP Scanjet G4010',
			'acquisition'		=> '2009-07-17',
			'asset_room_id'		=> 3,
			'room_description'	=> '',
			'description'		=> 'PPK-4766 D.V',
			'status'			=> '1'
		]);
	}
}
